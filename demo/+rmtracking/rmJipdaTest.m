% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
%

function [gt, tracks, detections, evals] = rmJipdaTest(varargin)
  
  parser = inputParser();
  
  % simulation parameters
  parser.addParameter('num_steps',    100);
  parser.addParameter('num_trials',   20);
  parser.addParameter('num_objects',  15);
  parser.addParameter('gt',           []);      % ground truth
  parser.addParameter('detections',   []);
  parser.addParameter('r',            1);       % radius of a sphere
  parser.addParameter('dim',          2);       % dimension of a sphere
  parser.addParameter('sigma_q',      0.05^2);    % process noise std dev
  parser.addParameter('sigma_r',      0.002^2);   % measurement noise std dev
  parser.addParameter('dt',           0.05);    % sampling time
  parser.addParameter('fa_rate',      8);     % false alarms per step
  parser.addParameter('pd',           0.98);    % detection probability
  parser.addParameter('unique_gt',    false);
  
  % jipda parameters
  parser.addParameter('jipda_lambda',           []);
  parser.addParameter('jipda_confirm_thres',    0.85);
  parser.addParameter('jipda_delete_thres',     0.003);
  parser.addParameter('jipda_pd',               0.98);
  parser.addParameter('jipda_pg',               0.95);
  parser.addParameter('jipda_ps',               0.99);
  parser.addParameter('jipda_init_existence',   0.65);
  parser.addParameter('jipda_init_thres',       0.5);
  
  % Riemannian manifold EKF parameters
  parser.addParameter('rm_sigma_q', 0.05^2);
  parser.addParameter('rm_sigma_r', 0.002^2);
  parser.addParameter('rm_sigma_p', 0.004^2);
  
  % Von Mises-Fisher filter parameters
  parser.addParameter('vmf_kappa_d',    20);
  parser.addParameter('vmf_kappa_o',    8000);
  parser.addParameter('vmf_kappa_init', 1000);
  
  % Projected Kalman filter parameters
  parser.addParameter('kf_sigma_q',     0.05^2);
  parser.addParameter('kf_sigma_r',     0.002^2);
  parser.addParameter('kf_sigma_init',  0.004^2);

  % Const velocity RM-EKF params
  parser.addParameter('cv_rm_sigma_q', 0.05^2);
  parser.addParameter('cv_rm_sigma_r', 0.002^2);
  parser.addParameter('cv_rm_sigma_p', [0.002^2 0.002^2 0.5 0.5]);
  
  parser.parse(varargin{:});
  
  % extract simulation parameters
  sim_params = struct(...
    'num_steps',       parser.Results.num_steps, ...
    'num_trials',      parser.Results.num_trials, ...
    'num_objects',     parser.Results.num_objects, ...
    'gt',              parser.Results.gt, ...
    'detections',      parser.Results.detections, ...
    'r',               parser.Results.r, ...
    'sigma_q',         parser.Results.sigma_q, ...
    'sigma_r',         parser.Results.sigma_r, ...
    'dim',             int32(parser.Results.dim), ...
    'dt',              parser.Results.dt, ...
    'fa_rate',         parser.Results.fa_rate, ...
    'pd',              parser.Results.pd, ...
    'unique_gt',       parser.Results.unique_gt);
  
  dim = sim_params.dim;
  r = sim_params.r;
  ddim = double(dim);
  
  sphere = m3t.geometry.rm.Sphere(dim, r);
  sphere.setKarcherMeanSettings(...
    'maxIter',    int32(10), ...
    'absTol',     1e-3, ...
    'relTol',     1e-4, ...
    'gradTol',    1e-4, ...
    'warnings',   false);
  sphere_surface = (2 * pi^((ddim + 1) / 2) * r^(ddim) / gamma((ddim + 1) / 2));
  
  sim_params.manifold = sphere;
  
  % extract jipda parameters
  if isempty(parser.Results.jipda_lambda)
    lambda = sim_params.fa_rate / sphere_surface;
  else
    lambda = parser.Results.jipda_lambda;
  end
  if isempty(parser.Results.jipda_pd)
    pd = sim_params.pd;
  else
    pd = parser.Results.jipda_pd;
  end
  jipda_params = struct( ...
    'confirm_thres',    parser.Results.jipda_confirm_thres, ...
    'delete_thres',     parser.Results.jipda_delete_thres, ...
    'pg',               parser.Results.jipda_pg, ...
    'pd',               pd, ...
    'ps',               parser.Results.jipda_ps, ...
    'init_existence',   parser.Results.jipda_init_existence, ...
    'init_thres',       parser.Results.jipda_init_thres, ...
    'lambda',           lambda);
  
  % extract Riemannian manifold UKF parameters
  rm_sigma_q = parser.Results.rm_sigma_q;
  rm_sigma_r = parser.Results.rm_sigma_r;
  rm_sigma_p = parser.Results.rm_sigma_p;
  
  rm_params = struct(...
    'manifold',      sphere, ...
    'sigma_q', rm_sigma_q, ...
    'sigma_r', rm_sigma_r, ...
    'P0',     @(x)(rmtracking.utils.createRmCovarianceMatrix(x, rm_sigma_p)), ...
    'x0',     @(x)(x));
  
  % extract Von Mises-Fisher filter parameters
  vmf_params = struct(...
    'dim',        dim, ...
    'kappa_d',    parser.Results.vmf_kappa_d, ...
    'kappa_o',    parser.Results.vmf_kappa_o, ...
    'kappa_init', parser.Results.vmf_kappa_init);
  
  % extract projected Kalman filter parameters
  proj_kf_params = struct(...
    'dim',            dim + 1, ...
    'projection_fcn', @(x) (x/norm(x)*r), ...
    'F',              eye(dim + 1), ...
    'H',              eye(dim + 1), ...
    'Q',              eye(dim + 1) * parser.Results.kf_sigma_q, ...
    'R',              eye(dim + 1) * parser.Results.kf_sigma_r, ...
    'P',              eye(dim + 1) * parser.Results.kf_sigma_init);
  
  % extract const velocity RM-EKF parameters
  cv_rm_sigma_q = parser.Results.cv_rm_sigma_q;
  cv_rm_sigma_r = parser.Results.cv_rm_sigma_r;
  cv_rm_sigma_p = parser.Results.cv_rm_sigma_p;

  cv_rm_params = struct(...
    'manifold', sphere, ...
    'sigma_q', cv_rm_sigma_q, ...
    'sigma_r', cv_rm_sigma_r, ...
    'P0', @(x)(rmtracking.utils.createRmCovarianceMatrix(x, cv_rm_sigma_p)), ...
    'x0', @(x)(x));

  params = struct(...
    'sim_params',       sim_params, ...
    'jipda_params',     jipda_params, ...
    'rm_params',        rm_params, ...
    'vmf_params',       vmf_params, ...
    'proj_kf_params',   proj_kf_params, ...
    'cv_rm_params',     cv_rm_params);
  
  
  % tracking metrics
  ospa = m3t.eval.OptimalSubPatternAssignment(r, 2);
  ospa.setDistanceFcn(@(x,y)(sphere.distance(x.value, y)));
  
  [gt, tracks, detections, evals] = rmtracking.utils.monteCarloSimulations(params, ospa);
  
  tvec = ((1:sim_params.num_steps) - 1) * sim_params.dt;
  
  figure;
  axes;
  hold on;
  grid on;
  plot(tvec, evals(:,1), 'b', 'LineWidth', 2);
  plot(tvec, evals(:,2), 'r--', 'LineWidth', 2);
  plot(tvec, evals(:,3), 'g-.', 'LineWidth', 2);
  plot(tvec, evals(:,4), 'm-.', 'LineWidth', 2);
  legend('RM-JIPDA', 'vMF-JIPDA', 'Projected KF', 'CV-RM-JIPDA');
  title('OSPA error');
  xlabel('t [s]');
  ylabel('error [m]');
  
  f2 = figure;

  hh1 = subplot(3,2,1);
  grid on;
  hold on;
  xlabel('$t$ [s]', 'Interpreter', 'latex');
  ylabel('$x$ [m]', 'Interpreter', 'latex');

  hh2 = subplot(3,2,3);
  grid on;
  hold on;
  xlabel('$t$ [s]', 'Interpreter', 'latex');
  ylabel('$y$ [m]', 'Interpreter', 'latex');

  hh3 = subplot(3,2,5);
  grid on;
  hold on;
  xlabel('$t$ [s]', 'Interpreter', 'latex');
  ylabel('$z$ [m]', 'Interpreter', 'latex');

  hh4 = subplot(3,2,2);
  grid on;
  hold on;
  xlabel('$t$ [s]', 'Interpreter', 'latex');
  ylabel('$\dot{x}$ [m/s]', 'Interpreter', 'latex');

  hh5 = subplot(3,2,4);
  grid on;
  hold on;
  xlabel('$t$ [s]', 'Interpreter', 'latex');
  ylabel('$\dot{y}$ [m/s]', 'Interpreter', 'latex');

  hh6 = subplot(3,2,6);
  grid on;
  hold on;
  xlabel('$t$ [s]', 'Interpreter', 'latex');
  ylabel('$\dot{z}$ [m/s]', 'Interpreter', 'latex');


  for i = 1:numel(detections)
    x = detections(i).value.euclideanValue();
    t = detections(i).timestamp;

    subplot(hh1);
    plot(t, x(1), '.', 'Color', [0.5 0.5 0.5 0.5], 'MarkerSize', 8);
    subplot(hh2);
    plot(t, x(2), '.', 'Color', [0.5 0.5 0.5 0.5], 'MarkerSize', 8);
    subplot(hh3);
    plot(t, x(3), '.', 'Color', [0.5 0.5 0.5 0.5], 'MarkerSize', 8);
  end

  ids = unique([gt.id]);
  
  for i = ids
    temp_gt = gt([gt.id] == i);
    timestamps = [temp_gt.timestamp];
    xx = zeros(3, numel(temp_gt));
    vv = zeros(3, numel(temp_gt));
    for j = 1:numel(timestamps)
      current_gt = temp_gt([temp_gt.timestamp] == timestamps(j));
      xx(:,j) = current_gt.value.euclideanValue();
      vv(:,j) = current_gt.velocity.euclideanValue(current_gt.value);
    end

    subplot(hh1);
    plot(timestamps, xx(1,:), 'k', 'LineWidth', 1);
    subplot(hh2);
    plot(timestamps, xx(2,:), 'k', 'LineWidth', 1);
    subplot(hh3);
    plot(timestamps, xx(3,:), 'k', 'LineWidth', 1);
    subplot(hh4);
    plot(timestamps, vv(1,:), 'k', 'LineWidth', 1);
    subplot(hh5);
    plot(timestamps, vv(2,:), 'k', 'LineWidth', 1);
    subplot(hh6);
    plot(timestamps, vv(3,:), 'k', 'LineWidth', 1);

  end

  ids = unique([tracks.rm_jipda.id]);
  for i = ids
    temp_tracks = tracks.rm_jipda([tracks.rm_jipda.id] == i);
    timestamps = [temp_tracks.timestamp];
    xx = zeros(3, numel(temp_tracks));
    for j = 1:numel(timestamps)
      xx(:,j) = temp_tracks( ...
        [temp_tracks.timestamp] == timestamps(j)).position.euclideanValue();
    end

  end

  ids = unique([tracks.vmf_jipda.id]);
  for i = ids
    temp_tracks = tracks.vmf_jipda([tracks.vmf_jipda.id] == i);
    timestamps = [temp_tracks.timestamp];
    xx = zeros(3, numel(temp_tracks));
    for j = 1:numel(timestamps)
      xx(:,j) = temp_tracks( ...
        [temp_tracks.timestamp] == timestamps(j)).position.euclideanValue();
    end

  end
  

  ids = unique([tracks.proj_kf_jipda.id]);

  colors = colorcube(100);

  for i = ids
    temp_tracks = tracks.proj_kf_jipda([tracks.proj_kf_jipda.id] == i);
    timestamps = [temp_tracks.timestamp];
    xx = zeros(3, numel(temp_tracks));
    for j = 1:numel(timestamps)
      xx(:,j) = temp_tracks( ...
        [temp_tracks.timestamp] == timestamps(j)).position.euclideanValue();
    end

  end

  ids = unique([tracks.cv_rm_jipda.id]);
  for i = ids
    temp_tracks = tracks.cv_rm_jipda([tracks.cv_rm_jipda.id] == i);
    timestamps = [temp_tracks.timestamp];
    xx = zeros(6, numel(temp_tracks));
    for j = 1:numel(timestamps)
      xx(:,j) = temp_tracks( ...
        [temp_tracks.timestamp] == timestamps(j)).state.euclideanValue();
    end

    col = [colors(mod(i, 100) + 1,:), 0.5];

    subplot(hh1);
    plot(timestamps, xx(1,:), 'Color', col, 'LineWidth', 3);
    subplot(hh2);
    plot(timestamps, xx(2,:), 'Color', col, 'LineWidth', 3);
    subplot(hh3);
    plot(timestamps, xx(3,:), 'Color', col, 'LineWidth', 3);
    subplot(hh4);
    plot(timestamps, xx(4,:), 'Color', col, 'LineWidth', 3);
    subplot(hh5);
    plot(timestamps, xx(5,:), 'Color', col, 'LineWidth', 3);
    subplot(hh6);
    plot(timestamps, xx(6,:), 'Color', col, 'LineWidth', 3);
  end
  
  fprintf('\nAverage OSPA of RM-JIPDA: %d', sum(evals(:,1)) / numel(evals(:,1)));
  fprintf('\nAverage OSPA of vMF-JIPDA: %d', sum(evals(:,2)) / numel(evals(:,2)));
  fprintf('\nAverage OSPA of KF-JIPDA: %d', sum(evals(:,3)) / numel(evals(:,3)));
  fprintf('\nAverage OSPA of CV-RM-JIPDA: %d', sum(evals(:,4)) / numel(evals(:,4)));

  fprintf('\n\n');
end


function col = getColor(id)
  colors = colorcube(100);
  i = mod(id, 100) + 1;
  col = colors(i,:);
end