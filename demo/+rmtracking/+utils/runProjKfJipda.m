% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
%
%   rmtracking.utils.runProjKfJipda is a function.
%
%     [tracks, errors] = rmtracking.utils.runProjKfJipda(...
%           jipda, sphere, dets, steps, ospa, gt);
%               Runs the jipda filter and collects its estimates and calculates
%               the ospa error of a tracker. Argument jipda is a m3t.jipda.Jipda
%               objects with rmtracking.projkf.ProjKF valued targets, dets is
%               the struct array of simulated detections, steps is the number of
%               simulation steps, ospa is the MTT error, gt is the struct
%               array of ground truth objects and the sphere is an
%               m3t.geometry.rm.sphere.Sphere object.
%
% ------------------------------------------------------------------------------

function [tracks, errors] = runProjKfJipda(...
    jipda, dets, steps, ospa, gt, dt)
  
  tracks = struct(...
    'id',         {}, ...
    'timestamp',  {}, ...
    'state',      {}, ...
    'position',   {}, ...
    'covariance', {}, ...
    'kappa',      {});
  
  if isempty(dets)
    return;
  end
  manifold = dets(1).value.getManifold();
  
  errors = zeros(1, steps);
  
  for step = 1:steps
    gt_now = gt(abs(([gt.timestamp] - step * dt)/(step*dt)) < 1e-4);
    dets_now = dets(abs(([dets.timestamp] - step * dt)/(step*dt)) < 1e-4);
    
    y = m3t.Detection.empty();
    for i = 1:numel(dets_now)
      y(i) = m3t.Detection(...
        manifold.pointToEuclidean(dets_now(i).value), ...
        dets_now(i).timestamp);
    end
    
    jipda.update(y);
    temp = jipda.getTracks();
    
    x = m3t.geometry.rm.Point.empty();
    for i = 1:numel(temp)
      x_temp = temp(i).getState();
      x(i) = manifold.createPoint(x_temp); %ok<AGROW>
      tracks(end+1) = struct(...
        'id',         temp(i).id_, ...
        'timestamp',  step * dt, ...
        'state',      x_temp, ...
        'position',   x(i), ...
        'covariance', temp(i).getCovariance(), ...
        'kappa',      []); %#ok<AGROW>
    end
    
    errors(1, step) = ospa.evaluate(gt_now, x);
  end
  
end

