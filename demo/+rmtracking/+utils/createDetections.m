% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
%
%   rmtracking.utils.createDetections is a function.
%
%     dets = rmtracking.utils.createDetections(gt, pd, r, fa, steps, sphere)
%         Creates detections for the tracking simulation. Argument gt is the
%         array of ground truth objects, pd is the detection probability, r is
%         the standard deviation of the measurement noise, fa is the number of
%         expected false alarms per step (poisson distributed), steps is the
%         number of simulation steps and sphere is an m3t.geometry.rm.Sphere
%         object.
%
% ------------------------------------------------------------------------------

function [dets] = createDetections(gt, pd, sigma_r, fa_rate, steps, dt, manifold)
  dets = struct(...
    'value', {},...
    'timestamp', {});
  
  dim = manifold.dim();
  R = eye(dim) * sigma_r;
  
  for k = 1:steps
    gt_temp = gt(abs([gt.timestamp] - k * dt) < 1e-4);
    
    for i = 1:numel(gt_temp)
      if rand() <= pd
        x = gt_temp(i).value;
        B = manifold.basis(x);
        a = chol(R) * randn(dim, 1);
        w = manifold.createTangent(x, zeros(dim,1));
        for j = 1:numel(B)
          w = w + a(j) * B(j);
        end
        z = manifold.exp(x, w);
        
        dets(end + 1) = struct(...
          'value', z,...
          'timestamp', k * dt); %#ok<AGROW>
      end
    end
    
    for i = 1:poissrnd(fa_rate)
      z = rand(dim + 1, 1) * 2 - 1;
      while norm(z) < 1e-3
        z = rand(dim + 1, 1) * 2 - 1;
      end
      z = manifold.createPoint(z);
      dets(end + 1) = struct(...
        'value', z,...
        'timestamp', k * dt); %#ok<AGROW>
    end
  end
end

