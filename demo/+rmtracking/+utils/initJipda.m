% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
%
%   rmtracking.utils.initJipda is a function.
%
%     jipda = rmtracking.utils.initJipda(params)
%         Creates m3t.jipda.Jipda object and initialises it with the settings
%         defined in params struct.
%
% ------------------------------------------------------------------------------

function jipda = initJipda(params)
  jipda = m3t.jipda.Jipda();
  jipda.enableIntegrated(true);
  jipda.enableParametric(true);
  jipda.setClutterDensity(params.lambda);
  jipda.setConfirmThres(params.confirm_thres);
  jipda.setDeleteThres(params.delete_thres);
  jipda.setDetectionProbability(params.pd);
  jipda.setGatingProbability(params.pg);
  jipda.setInitExistence(params.init_existence);
  jipda.setInitThres(params.init_thres);
  jipda.setSurvivalProbability(params.ps);
end

