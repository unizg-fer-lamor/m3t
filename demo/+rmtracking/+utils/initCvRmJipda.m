% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------

function jipda = initCvRmJipda(jipda_params, rm_params)
  
  jipda = rmtracking.utils.initJipda(jipda_params);
  track_generator = ...
    @(y)(trackInitialisation(y, rm_params));
  
  jipda.setTrackGenerator(track_generator);
  
  function track = trackInitialisation(detection, params)
    
    rm_kf = m3t.estimation.rm.examples.SphereConstVelocityRmEkf(params.manifold);
    

    bundle = rm_kf.getStateManifold();
    sphere = rm_kf.getMeasurementManifold();
    
    position = params.x0(detection.getValue());
    velocity = sphere.createTangent(position, zeros(sphere.dim, 1));

    x = bundle.createPoint(position, velocity);
    P = params.P0(x);
    
    rm_kf.sigma_q_ = params.sigma_q;
    rm_kf.sigma_r_ = params.sigma_r;
    rm_kf.setX(x);
    rm_kf.setP(P);
    
    track = m3t.jipda.tracks.RmKalmanTrack(rm_kf);
    track.setTimestamp(detection.getTimestamp());
  end
  
end

