% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------

function jipda = initProjKfJipda(jipda_params, kf_params)
  
  jipda = rmtracking.utils.initJipda(jipda_params);
  jipda.setTrackGenerator(@(y)(trackInitialisation(y, kf_params)));
  
  function track = trackInitialisation(detection, params)
    
    proj_kf = rmtracking.projkf.ProjKF(...
      params.dim, int32(0), params.dim, params.projection_fcn);
    
    proj_kf.setF(params.F);
    proj_kf.setH(params.H);
    proj_kf.setQ(params.Q);
    proj_kf.setR(params.R);
    proj_kf.setX(detection.getValue);
    proj_kf.setP(params.P);
  
    track = m3t.jipda.tracks.KalmanTrack(proj_kf);
    track.setTimestamp(detection.getTimestamp);
    
  end
end

