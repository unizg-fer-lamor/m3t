% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
%
%   rmtracking.utils.createGroundTruth is a function.
%
%     gt = rmtracking.utils.createGroundTruth(steps, num, sphere, q, dt, flag)
%         Creates a ground truth for a tracking simulation. Argument steps is
%         the number of steps of the simulation, num is the number of desired GT
%         objects, sphere is the m3t.geometry.rm.Shere object, q is the std.
%         deviation of the process noise (random acceleration), dt is the
%         sampling period and flag controls whether are all gt targets present
%         from first to last step or not.
%
% ------------------------------------------------------------------------------

function gt = createGroundTruth(steps, num_gt, manifold, q, dt, flag)
  gt = struct(...
    'timestamp',{},...
    'id',{},...
    'value',{}, ...
    'velocity', {});
  
  dim = manifold.dim();
  
  Q = eye(dim) * q;
  
  for i = 1:num_gt
    if ~flag
      t_start = randi([1, steps / 2]);
      t_end = randi([t_start + 3, steps]);
    else
      t_start = 1;
      t_end = steps;
    end
    
    x = rand(dim + 1, 1) * 2 - 1;
    while norm(x) < 1e-3
      x = rand(dim + 1, 1) * 2 - 1;
    end
    x = manifold.createPoint(x);
    
    w = mvnrnd(zeros(dim,1),0.5*eye(dim));
    B = manifold.basis(x);
    v = manifold.createTangent(x, zeros(dim, 1));
    for j = 1:dim
      v = v + w(j)*B(j);
    end
    
    for j = t_start:t_end
      B = manifold.basis(x);
      a = chol(Q) * randn(dim, 1);
      vv = manifold.createTangent(x, zeros(dim, 1));
      for k = 1:numel(B)
        vv = vv + a(k) * B(k);
      end
      xx = manifold.exp(x, dt * v);
      v = manifold.parallelTransport(v + dt * vv, x, dt * v);
      x = xx;
      
      gt(end+1) = struct(...
        'timestamp', j * dt,...
        'id', i,...
        'value', x, ...
        'velocity', v); %#ok<AGROW>
    end
    
  end
  
end

