% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
function [states] = runRmEkf(filter, measurements, max_steps, dt)
  states = struct(...
    'timestamp',  0, ...
    'x',          filter.x_, ...
    'y_hat',      filter.y_hat_, ...
    'P',          filter.P_, ...
    'S',          filter.S_, ...
    'kappa',      []);

  for step = 1:max_steps
    filter.predict([], dt);
    filter.update(measurements(step).z);

    states(end + 1) = struct(...
    'timestamp',  step * dt, ...
    'x',          filter.x_, ...
    'y_hat',      filter.y_hat_, ...
    'P',          filter.P_, ...
    'S',          filter.S_, ...
    'kappa',      []);
  end
  
end

