% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------

function jipda = initVmfJipda(jipda_params, vmf_params)
  
  jipda = rmtracking.utils.initJipda(jipda_params);
  track_generator = @(y)(trackInitialisation(y, vmf_params));
  
  jipda.setTrackGenerator(track_generator);
  
  function track = trackInitialisation(detection, params)
    
    x = detection.getValue();
    
    vmf = m3t.estimation.vmf.VonMisesFisherFilter(params.dim);
    
    vmf.setDiffusionKappa(params.kappa_d);
    vmf.setObservationKappa(params.kappa_o);
    vmf.setKappa(params.kappa_init);
    vmf.setX(x);
    
    track = m3t.jipda.tracks.VmfTrack(vmf);
    track.setTimestamp(detection.getTimestamp());
  end
  
end

