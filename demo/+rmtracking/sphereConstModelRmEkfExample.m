function [filter, rmse] = sphereConstModelRmEkfExample()
  
  max_steps = 100;
  manifold = m3t.geometry.rm.Sphere(int32(2), 1);

  filter = m3t.estimation.rm.examples.ConstModelRmEkf(manifold);
  filter.sigma_q_ = 0.04^2;
  filter.sigma_r_ = 0.01^2;

  % starting point and velocity of gt:
  x = manifold.createPoint([1 0 0]');
  v = manifold.createTangent(x, (rand(2, 1) * 2 - 1) * 0.8);

  % set initial state of a filter a bit away from the gt:
  v0 = manifold.createTangent(x, (rand(2, 1) * 2 - 1));
  x0 = manifold.exp(x, v0);

  % calculate initial covariance of a filter at point x0
  V = manifold.basis(x0);
  P0 = zeros(2);
  for i = 1:2
    temp = V(i).getValue();
    P0 = P0 + (temp * temp') * 0.01;
  end

  filter.setX(x0);
  filter.setP(P0);

  rmse = zeros(1, max_steps + 1);
  rmse(1) = manifold.distance(x, filter.x_) ^ 2;

  for step = 1:max_steps
    xx = manifold.exp(x, v);
    v = manifold.parallelTransport(v, x, v);
    x = xx;

    y = generateMeasurement(manifold, x);

    filter.predict([], 1);
    filter.update(y);
    rmse(step + 1) = manifold.distance(x, filter.x_) ^2;
  end

  figure;
  plot(0:max_steps, rmse, 'b', 'LineWidth', 2);

end

function y = generateMeasurement(manifold, x)
  V = manifold.basis(x);
  V_mat = zeros(numel(V), numel(V));
  for j = 1:numel(V)
    V_mat(:,j) = V(j).getValue();
  end
  RR = V_mat * 0.01^2 * V_mat';
  w = mvnrnd(zeros(numel(V), 1), RR)';
  ww = manifold.createTangent(x, w);
  y = manifold.exp(x, ww);
end
