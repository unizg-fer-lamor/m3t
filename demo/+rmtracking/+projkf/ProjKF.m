% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
%
%   rmtracking.projkf.ProjKF - Projected Kalman filter class
%
%     Simple baseline for rmtracking. Runs Kalman filter prediciton and update
%     steps in ambient Euclidean space and then projects state back to the
%     manifold.
%
% ------------------------------------------------------------------------------

classdef ProjKF < m3t.estimation.kalman.DiscreteKalmanFilter
  
  properties (SetAccess = private)
    project_fcn_;
  end
  
  methods
    
    function obj = ProjKF(system_dim, input_dim, output_dim, fcn)
      obj@m3t.estimation.kalman.DiscreteKalmanFilter(...
        system_dim, input_dim, output_dim);
      obj.project_fcn_ = fcn;
    end
    
    function predict(obj, dt, varargin)
      obj.predict@m3t.estimation.kalman.DiscreteKalmanFilter(dt, varargin{:});
      obj.x_ = obj.project_fcn_(obj.x_);
      obj.y_hat_ = obj.project_fcn_(obj.y_hat_);
    end
    
    function update(obj, y)
      obj.update@m3t.estimation.kalman.DiscreteKalmanFilter(y);
      obj.x_ = obj.project_fcn_(obj.x_);
      obj.y_hat_ = obj.project_fcn_(obj.y_hat_);
    end
    
    function weightedUpdate(obj, weights, detections)
      obj.weightedUpdate@m3t.estimation.kalman.DiscreteKalmanFilter(...
        weights, detections);
      obj.x_ = obj.project_fcn_(obj.x_);
      obj.y_hat_ = obj.project_fcn_(obj.y_hat_);
    end
    
  end
  
end

