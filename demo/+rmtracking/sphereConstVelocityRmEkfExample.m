% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
function [rm_ukf, rmse] = sphereConstVelocityRmEkfExample()
  
  max_steps = 500;
  dt = 0.05;
  t_vec = (0:max_steps) * dt;

  manifold = m3t.geometry.rm.Sphere(int32(2), 1);
  bundle = m3t.geometry.rm.TangentBundle(manifold);
  bundle2 = bundle.copy();
  bundle2.setLogOptimisationSettings( ...
    'maxIter', 5, ...
    'relTol', 1e-3, ...
    'absTol', 1e-3, ...
    'warnings', false, ...
    'useConjugate', false);
  bundle2.setKarcherMeanSettings(...
    'MaxIter', int32(10), ...
    'relTol', 1e-3, ...
    'absTol', 1e-3, ...
    'gradTol', 1e-2)

  sigma_q = 0.005 ^2;

  rm_ekf = m3t.estimation.rm.examples.SphereConstVelocityRmEkf(manifold);
  rm_ekf.sigma_q_ = 0.005 ^ 2;
  rm_ekf.sigma_r_ = 0.01 ^ 2;

  rm_ukf = m3t.estimation.rm.examples.ConstVelocityRmUkf(bundle2);
  rm_ukf.sigma_q_ = 0.01 ^ 2 * [0.01 0.01 50 50];
  rm_ukf.sigma_r_ = 0.03 ^ 2;

  p = manifold.createPoint([0 0 1]');
  v = manifold.createTangent(p, (rand(2, 1) * 2 - 1) * 0.2);

  x_gt = m3t.geometry.rm.BundlePoint(bundle, p, v);

  v0 = manifold.createTangent(p, (rand(2, 1) * 2 - 1) * 0.4);
  p0 = manifold.exp(p, v0);
  v0 = manifold.createTangent(p0, zeros(2, 1));

  x0 = m3t.geometry.rm.BundlePoint(bundle, p0, v0);

  p_vec = [0.5, 0.5, 0.5, 0.5];
  V = bundle.basis(x0);
  P0 = zeros(4);
  for i = 1:4
    temp = V(i).getValue();
    P0 = P0 + (temp * temp') * p_vec(i);
  end

  rm_ekf.setX(x0);
  rm_ekf.setP(P0);

  rm_ukf.setX(x0);
  rm_ukf.setP(P0);

  rmse = zeros(2, max_steps + 1);
  rmse(1, 1) = manifold.distance(p, rm_ekf.x_.getBasePoint()) ^ 2;
  rmse(2, 1) = manifold.distance(p, rm_ukf.x_.getBasePoint()) ^ 2;

  p_eucl = zeros(3, max_steps + 1, 3);
  v_eucl = zeros(3, max_steps + 1, 3);

  p_eucl(:, 1, 1) = manifold.pointToEuclidean(p);
  v_eucl(:, 1, 1) = manifold.tangentToEuclidean(p, v);

  p_eucl(:, 1, 2) = manifold.pointToEuclidean(rm_ekf.x_.getBasePoint());
  v_eucl(:, 1, 2) = manifold.tangentToEuclidean(...
    rm_ekf.x_.getBasePoint(), rm_ekf.x_.getTangent());

  p_eucl(:, 1, 3) = manifold.pointToEuclidean(rm_ukf.x_.getBasePoint());
  v_eucl(:, 1, 3) = manifold.tangentToEuclidean(...
    rm_ukf.x_.getBasePoint(), rm_ukf.x_.getTangent());

  fig = figure('units','normalized','outerposition',[0 0 1 1]);

  for step = 1:max_steps
    fprintf('%d\n', step);

    V = manifold.basis(p);
    V_mat = zeros(numel(V));
    for j = 1:numel(V)
      V_mat(:, j) = V(j).getValue();
    end
    QQ = V_mat * sigma_q * V_mat';
    w = mvnrnd(zeros(numel(V), 1), QQ)';
    w = manifold.createTangent(p, w);

    p_new = manifold.exp(p, dt * v);
    v = manifold.parallelTransport(v + w, p, dt * v);
    p = p_new; 
    x_gt = m3t.geometry.rm.BundlePoint(bundle, p, v);

    y = generateMeasurement(manifold, p);

    rm_ekf.predict([], dt);
    rm_ekf.update(y);

%     rm_ukf.predict([], dt);
%     rm_ukf.update(y);

    p_eucl(:, step + 1, 1) = manifold.pointToEuclidean(p);
    v_eucl(:, step + 1, 1) = manifold.tangentToEuclidean(p, v);

    p_eucl(:, step + 1, 2) = manifold.pointToEuclidean(rm_ekf.x_.getBasePoint());
    v_eucl(:, step + 1, 2) = manifold.tangentToEuclidean(...
      rm_ekf.x_.getBasePoint(), rm_ekf.x_.getTangent());

    p_eucl(:, step + 1, 3) = manifold.pointToEuclidean(rm_ukf.x_.getBasePoint());
    v_eucl(:, step + 1, 3) = manifold.tangentToEuclidean(...
      rm_ukf.x_.getBasePoint(), rm_ukf.x_.getTangent());

    plottingS(fig, x_gt, y, rm_ekf.x_, manifold, rm_ekf.S_, rm_ekf.y_hat_);

    rmse(1, step + 1) = manifold.distance(p, rm_ekf.x_.getBasePoint()) ^ 2;
    rmse(2, step + 1) = manifold.distance(p, rm_ukf.x_.getBasePoint()) ^ 2;

  end

  figure;
  plot(t_vec, rmse(1,:), 'b', 'LineWidth', 2);
  hold on;
  plot(t_vec, rmse(2,:), 'g--', 'LineWidth', 2);

  figure;

  subplot(3,2,1);
  hold on
  grid on;
  plot(t_vec, p_eucl(1, :, 1), 'b', 'LineWidth', 2);
  plot(t_vec, p_eucl(1, :, 2), 'r--', 'LineWidth', 2);
  plot(t_vec, p_eucl(1, :, 3), 'g-.', 'LineWidth', 2);
  xlabel('$t$ [s]', 'Interpreter', 'latex');
  ylabel('$x$ [m]', 'Interpreter', 'latex');
  legend('Ground truth', 'CV-RM-EKF', 'CV-RM-UKF');

  subplot(3,2,3);
  hold on
  grid on;
  plot(t_vec, p_eucl(2, :, 1), 'b', 'LineWidth', 2);
  plot(t_vec, p_eucl(2, :, 2), 'r--', 'LineWidth', 2);
  plot(t_vec, p_eucl(2, :, 3), 'g-.', 'LineWidth', 2);
  xlabel('$t$ [s]', 'Interpreter', 'latex');
  ylabel('$y$ [m]', 'Interpreter', 'latex');

  subplot(3,2,5);
  hold on
  grid on;
  plot(t_vec, p_eucl(3, :, 1), 'b', 'LineWidth', 2);
  plot(t_vec, p_eucl(3, :, 2), 'r--', 'LineWidth', 2);
  plot(t_vec, p_eucl(3, :, 3), 'g-.', 'LineWidth', 2);
  xlabel('$t$ [s]', 'Interpreter', 'latex');
  ylabel('$z$ [m]', 'Interpreter', 'latex');

  subplot(3,2,2);
  hold on
  grid on;
  plot(t_vec, v_eucl(1, :, 1), 'b', 'LineWidth', 2);
  plot(t_vec, v_eucl(1, :, 2), 'r--', 'LineWidth', 2);
  plot(t_vec, v_eucl(1, :, 3), 'g-.', 'LineWidth', 2);
  xlabel('$t$ [s]', 'Interpreter', 'latex');
  ylabel('$\dot{x}$ [m/s]', 'Interpreter', 'latex');

  subplot(3,2,4);
  hold on
  grid on;
  plot(t_vec, v_eucl(2, :, 1), 'b', 'LineWidth', 2);
  plot(t_vec, v_eucl(2, :, 2), 'r--', 'LineWidth', 2);
  plot(t_vec, v_eucl(2, :, 3), 'g-.', 'LineWidth', 2);
  xlabel('$t$ [s]', 'Interpreter', 'latex');
  ylabel('$\dot{y}$ [m/s]', 'Interpreter', 'latex');

  subplot(3,2,6);
  hold on
  grid on;
  plot(t_vec, v_eucl(3, :, 1), 'b', 'LineWidth', 2);
  plot(t_vec, v_eucl(3, :, 2), 'r--', 'LineWidth', 2);
  plot(t_vec, v_eucl(3, :, 3), 'g-.', 'LineWidth', 2);
  xlabel('$t$ [s]', 'Interpreter', 'latex');
  ylabel('$\dot{z}$ [m/s]', 'Interpreter', 'latex');

end

function y = generateMeasurement(manifold, x)
  V = manifold.basis(x);
  V_mat = zeros(numel(V));
  for j = 1:numel(V)
    V_mat(:,j) = V(j).getValue();
  end
  RR = V_mat * 0.01 ^ 2 * V_mat';
  w = mvnrnd(zeros(numel(V), 1), RR)';
  ww = manifold.createTangent(x, w);
  y = manifold.exp(x, ww);
end


function plottingS(hfig, gt, y, x, manifold, S, y_hat)
  
  figure(hfig);
  clf;
  
  gt_pos = gt.getBasePoint();
  gt_vel = gt.getTangent();
  
  gt_vel = manifold.tangentToEuclidean(gt_pos,gt_vel);
  gt_pos = manifold.pointToEuclidean(gt_pos);
  
  y = manifold.pointToEuclidean(y);
  
  p = x.getBasePoint();
  v = x.getTangent();

  pval = manifold.pointToEuclidean(p);
  vval = manifold.tangentToEuclidean(p, v);
  
  axes;
  grid on;
  hold on;
  
  [sx,sy,sz] = sphere(35);
  surface(sx,sy,sz,'FaceColor',[0.5,0.5,0.5],'FaceAlpha',0.5,...
    'EdgeColor',[0.6,0.6,0.6],'EdgeAlpha',0.7);
  daspect([1,1,1]);
  view([1 1 1]');
  setAxes3DPanAndZoomStyle(zoom(gca),gca,'camera');
  
  scatter3(gt_pos(1),gt_pos(2),gt_pos(3),'b','Marker','diamond');
  quiver3(gt_pos(1),gt_pos(2),gt_pos(3),gt_vel(1),gt_vel(2),gt_vel(3),...
    'b','LineWidth',2);
  
  scatter3(y(1),y(2),y(3),'g','Marker','*');
  
  scatter3(pval(1),pval(2),pval(3),'r','Marker','o');
  quiver3(pval(1),pval(2),pval(3),vval(1),vval(2),vval(3),...
    'r','LineWidth',2);
  
  [v,e] = eig(S,'vector');
  for i = 1:numel(e)
    vv = manifold.createTangent(p,sqrt(e(i))*v(:,i));
    y1 = manifold.exp(p,vv);
    y2 = manifold.exp(p,-vv);
    y = manifold.pointToEuclidean(y1);
    scatter3(y(1),y(2),y(3),'r','Marker','+'); 
    y = manifold.pointToEuclidean(y2);
    scatter3(y(1),y(2),y(3),'r','Marker','+'); 
  end
  
  yy = manifold.pointToEuclidean(y_hat);
  scatter3(yy(1),yy(2),yy(3),'c','Marker','x');
  
  drawnow('limitrate');
end