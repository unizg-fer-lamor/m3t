%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%

classdef (Abstract) Tracker < matlab.mixin.Heterogeneous & matlab.mixin.Copyable
  %TRACKER - Base class for all trackers
  %
  %   Defines methods that all multi-target trackers must implement.
  %
  %   Tracker methods:
  %     initialise - initialises the state of a tracker
  %     update - update step of a tracker
  %     getTracks - extracts estimated tracks
  %
  %   See also mtt.tracks.Track, mtt.detections.Detection
  
  methods (Abstract)
    %RESET - Initialisation of a multi-target tracker
    reset(obj);
    %UPDATE - Update step of a tracker
    update(obj,detections);
    %GETTRACKS - Extracts estiamted tracks
    tracks = getTracks(obj);
  end
  
    
end

