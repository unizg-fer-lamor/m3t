%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%

function [segments] = matrixSegmentation(A)
  %MATRIXSEGMENTATION - finds independent segments of logical matrix
  %
  % segments = mtt.utils.MATRIXSEGMENTATION(A)
  %   A - matrix with logical values, that describe the possible
  %       associations between two sets of object (i.e. validation matrix
  %       of tracks and measurements in multitarget tracking)
  %   segments - array of independent segments, each element is a struct
  %              with fields 'rows' and 'cols' which contain row and column
  %              indices of matrix A that corespond to this segment
  assert(islogical(A),...
    ['Argument A must be a matrix of logical values.', newline]);
  if isempty(A)
    segments = [];
    return;
  end
  num_rows = size(A,1);
  num_cols = size(A,2);
  segments(min(num_rows,num_cols)) = struct('rows',[],'cols',[]);
  seg_counter = 0; % counter of segments
  row_list = 1:num_rows; % list of unprocessed rows
  col_list = 1:num_cols; % list of unprocessed columns
  sel_rows = []; % selected rows
  sel_cols = []; % selected columns
  % first column of stack  -> row/col index,
  % second column of stack -> indicates if the coresponding index points to
  %                           a row (0) or column(1)
  stack = zeros(num_rows+num_cols,2);
  stack_ptr = 0;
  while true
    if stack_ptr == 0
      if ~isempty(sel_rows) && ~isempty(sel_cols)
        seg_counter = seg_counter + 1;
        segments(seg_counter).rows = sel_rows;
        segments(seg_counter).cols = sel_cols;
      end
      
      if isempty(row_list) && isempty(col_list)
        break;
      elseif numel(row_list) >= numel(col_list)
        stack_ptr = 1;
        stack(1,1) = row_list(1);
        stack(1,2) = 0;
        sel_rows = row_list(1);
        sel_cols = [];
        row_list(1) = [];
      else
        stack_ptr = 1;
        stack(1,1) = col_list(1);
        stack(1,2) = 1;
        sel_rows = [];
        sel_cols = col_list(1);
        col_list(1) = [];
      end
    end
    
    if stack(stack_ptr,2)
      % remove current column from the stack and iterate through it
      idx = stack(stack_ptr,1);
      stack_ptr = stack_ptr - 1;
      temp = false(1,numel(row_list));
      for ii = 1:numel(row_list)
        if A(row_list(ii),idx)
          stack_ptr = stack_ptr + 1;
          stack(stack_ptr,1) = row_list(ii);
          stack(stack_ptr,2) = 0;
          temp(ii) = true;
        end
      end
      sel_rows = [sel_rows,row_list(temp)]; %#ok<AGROW>
      row_list(temp) = [];
    else
      % remove current row from the stack and iterate through it
      idx = stack(stack_ptr,1);
      stack_ptr = stack_ptr - 1;
      temp = false(1,numel(col_list));
      for ii = 1:numel(col_list)
        if A(idx,col_list(ii))
          stack_ptr = stack_ptr + 1;
          stack(stack_ptr,1) = col_list(ii);
          stack(stack_ptr,2) = 1;
          temp(ii) = true;
        end
      end
      sel_cols = [sel_cols,col_list(temp)]; %#ok<AGROW>
      col_list(temp) = [];
    end
  end
  segments((seg_counter+1):end) = [];
end


