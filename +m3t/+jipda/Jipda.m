% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
%
%   m3t.jipda.Jipda - Joint integrated probabilistic data association filter
%
%     Jipda is a subclass of m3t.Tracker class.
%
%     Jipda methods:
%       getTracks   - returns confirmed tracks
%       reset       - resets tracker (terminates all tracks)
%       update      - updates tracks with new detections
%
%       enableIntegrated - enables or disables integrated mode of jipda
%       enableParametric - enables or disables parametric mode of jipda
%
%       isIntegrated - returns true if integrated mode is enabled
%       isParametric - returns true if parametric mode is enabled
%
%       getClutterDensity         - returns false alarm density
%       getConfirmThres           - returns target confirmation threshold
%       getDeleteThres            - returns target deleting threshold
%       getDetectionProbability   - returns detection probability
%       getGatingProbability      - returns gating probability
%       getInitExistence          - returns target initial existence probability
%       getInitThres              - returns target initialisation threshold
%       getMaxUndetectedFrames    - returns max undetected frames number
%       getMinDetectedFrames      - returns min detected frames number
%       getSurvivalProbability    - returns survival probability
%       getTrackGenerator         - returns track generator
%
%       setClutterDensity         - sets false alarm density
%       setConfirmThres           - sets target confirmation threshold
%       setDeleteThres            - sets target deleting threshold
%       setDetectionProbability   - sets detection probability
%       setGatingProbability      - sets gating probability
%       setInitExistence          - sets target initial existence probability
%       setInitThres              - sets target initialisation threshold
%       setMaxUndetectedFrames    - sets max undetected frames number
%       setMinDetectedFrames      - sets min detected frames number
%       setMotionModel            - sets target motion model
%       setSurvivalProbability    - sets survival probability
%
%     Jipda protected methods:
%       copyElement       - creates deep copy of Jipda object
%       createNewTracks   - new track creation
%       nextId            - returns next valid track id number
%       predict           - runs motion prediction of tracks
%       softAssignment    - jipda core soft assignment logic
%       trackManagement   - track initialisation, confirmation and deleting
%                           logic
%
%     See also m3t.Tracker, m3t.jipda.MotionModel, m3t.jipda.Track
%
% ------------------------------------------------------------------------------

classdef Jipda < m3t.Tracker
  
  properties (Access = protected)
    tracks_;
  end
  
  properties (Access = private)
    clutter_density_;
    confirm_thres_;
    delete_thres_;
    detection_probability_;
    gating_probability_;
    id_counter_;
    init_existence_;
    init_thres_;
    integrated_;
    max_undetected_frames_;
    min_detected_frames_;
    parametric_;
    survival_probability_;
    track_generator_;
  end
  
  properties (Constant = true, Access = private)
    probability_assert_ = ...
      @(x)(isa(x,'double') && isscalar(x) && x > 0 && x < 1);
    existence_assert_ = ...
      @(x)(isa(x,'double') && isscalar(x) && x >= 0 && x <= 1);
    clutter_assert_ = ...
      @(x)(isa(x,'double') && isscalar(x) && x >= 0);
    int_assert_ = ...
      @(x)(isinteger(x) && isscalar(x) && x > int32(0));
  end
  
  
  methods
    
    function obj = Jipda()
      obj@m3t.Tracker();
      % init properties:
      obj.enableIntegrated(true);
      obj.enableParametric(false);
      obj.id_counter_ = int32(-1);
      obj.setClutterDensity(0);
      obj.setConfirmThres(0.8);
      obj.setDeleteThres(0.001);
      obj.setDetectionProbability(0.95);
      obj.setGatingProbability(0.99);
      obj.setInitExistence(0.5);
      obj.setInitThres(0.5);
      obj.setMaxUndetectedFrames(int32(3));
      obj.setMinDetectedFrames(int32(2));
      obj.setSurvivalProbability(0.98);
      obj.tracks_ = m3t.jipda.tracks.Track.empty();
    end
    
  end
  
  methods
    
    function enableIntegrated(obj, b)
      % m3t.jipda.Jipda.enableIntegrated
      %
      %   obj.ENABLEINTEGRATED(b) 
      %       Enables or disables the integrated mode of Jipda. Argument b
      %       must be a lgical value. In integrated mode each track has
      %       existence probability that is estimated together with the
      %       state of the target. This probability is used to confirm and
      %       terminate the track. If integrated mode is not enabled,
      %       tracker behaves like jpda with track management logic based
      %       on number of subsequent frames in which the target was
      %       (un)detected. If you change this mode, you should also reset
      %       the tracker.
      assert(islogical(b) && isscalar(b));
      obj.integrated_ = b;
    end
    
    function enableParametric(obj, b)
      % m3t.jipda.Jipda.enableParametric
      %
      %   obj.ENABLEPARAMETRIC(b)
      %       Enables or disables the parametric mode of Jipda. Argument b
      %       must be a logical value. In parametric mode, the clutter
      %       density parameter is used. Otherwise, the clutter density is
      %       estimated based on the position of tracks and obtained
      %       detections.
      assert(islogical(b) && isscalar(b));
      obj.parametric_ = b;
    end
    
    function lambda = getClutterDensity(obj)
      % m3t.jipda.Jipda.getClutterDensity
      %   Returns the density of false alarms.
      lambda = obj.clutter_density_;
    end
    
    function thres = getConfirmThres(obj)
      % m3t.jipda.Jipda.getConfirmThres
      %   Returns the track confirmation threshold. Ignored when not in
      %   integrated mode.
      thres = obj.confirm_thres_;
    end
    
    function thres = getDeleteThres(obj)
      % m3t.jipda.Jipda.getDeleteThres
      %   Returns the track deleting threshold. Ignored when not in integrated
      %   mode.
      thres = obj.delete_thres_;
    end
    
    function pd = getDetectionProbability(obj)
      % m3t.jipda.Jipda.getDetectionProbability
      %   Returns the detection probability.
      pd = obj.detection_probability_;
    end
    
    function pg = getGatingProbability(obj)
      % m3t.jipda.Jipda.getGatingProbability
      %   Returns the gating probability.
      pg = obj.gating_probability_;
    end
    
    function existence = getInitExistence(obj)
      % m3t.jipda.Jipda.getInitExistence
      %   Returns the initial existence of tracks.
      existence = obj.init_existence_;
    end
    
    function thres = getInitThres(obj)
      % m3t.jipda.Jipda.getInitThres
      %   Returns track initialisation threshold.
      thres = obj.init_thres_;
    end
    
    function n = getMaxUndetectedFrames(obj)
      % m3t.jipda.Jipda.getMaxUndetectedFrames
      %   Returns the maximum number of consequtive frames in which no detection
      %   is assigned to track before it is terminated. This is used only when
      %   integrated mode is not enabled.
      n = obj.max_undetected_frames_;
    end
    
    function n = getMinDetectedFrames(obj)
      % m3t.jipda.Jipda.getMinDetectedFrames
      %   Returns the minimum number of consequtive frames in which at least one
      %   detection is assigned to object before the object gets confirmed. Used
      %   only when integrated mode is not enabled.
      n = obj.min_detected_frames_;
    end
    
    function ps = getSurvivalProbability(obj)
      % m3t.jipda.Jipda.getSurvivalProbability
      %   Returns the survival probability.
      ps = obj.survival_probability_;
    end
    
    function tracks = getTracks(obj)
      % m3t.jipda.Jipda.getTracks
      %   Returns confirmed tracks.
      tracks = obj.tracks_([obj.tracks_.confirmed_]).copy();
    end
    
    function generator = getTrackGenerator(obj)
      % m3t.jipda.Jipda.getTrackGenerator
      %   Return the handle to a function that generates new Jipda tracks.
      generator = obj.track_generator_;
    end
    
    function b = isIntegrated(obj)
      % m3t.jipda.Jipda.isIntegrated
      %   Returns true if the integrated mode is enabled.
      b = obj.integrated_;
    end
    
    function b = isParametric(obj)
      % m3t.jipda.Jipda.isParametric
      %   Returns true if the parametric mode is enabled.
      b = obj.parametric_;
    end
    
    function reset(obj)
      % m3t.jipda.Jipda.reset
      %   Resets the Jipda. All tracks are deleted.
      for i = 1:numel(obj.tracks_)
        obj.tracks_(i).delete();
      end
      obj.tracks_ = m3t.jipda.tracks.Track.empty();
      obj.id_counter_ = int32(-1);
    end
    
    function setClutterDensity(obj, lambda)
      % m3t.jipda.Jipda.setClutterDensity
      %
      %   obj.SETCLUTTERDENSITY(lambda)
      %       Sets the false alarm density. Argument lambda must be a double
      %       scalar value that is greater than or equal to zero.
      assert(obj.clutter_assert_(lambda));
      obj.clutter_density_ = lambda;
    end
    
    function setConfirmThres(obj, thres)
      % m3t.jipda.Jipda.setConfirmThres
      %
      %   obj.SETCONFIRMTHRES(thres)
      %       Sets the target confirmation threshold used to confirm targets
      %       based on their existence probability in integrated mode of Jipda.
      %       If integrated mode is not enabled, this value is ignored. Argument
      %       thres must be a double scalar between 0 and 1.
      assert(obj.probability_assert_(thres));
      obj.confirm_thres_ = thres;
    end
    
    function setDeleteThres(obj, thres)
      % m3t.jipda.Jipda.setDeleteThres
      %
      %   obj.SETDELETETHRES(thres)
      %       Sets target deleting threshold used to terminate targets based on
      %       their existence probability in integrated mode of Jipda. If the
      %       integrated mode is not enabled, this value is ignored. Argument
      %       thres must be a double scalar between 0 and 1.
      assert(obj.probability_assert_(thres));
      obj.delete_thres_ = thres;
    end
    
    function setDetectionProbability(obj, pd)
      % m3t.jipda.Jipda.setDetectionProbability
      %
      %   obj.SETDETECTIONPROBABILITY(pd)
      %       Sets the detection probability. Argument pd must be a double
      %       scalar between 0 and 1.
      assert(obj.probability_assert_(pd));
      obj.detection_probability_ = pd;
    end
    
    function setGatingProbability(obj, pg)
      % m3t.jipda.Jipda.setGatingProbability
      %
      %   obj.SETGATINGPROBABILITY(pg)
      %       Sets the gating probability used in validation process. Argument
      %       pg must be a double scalar between 0 and 1.
      assert(obj.probability_assert_(pg));
      obj.gating_probability_ = pg;
    end
    
    function setInitExistence(obj, existence)
      % m3t.jipda.Jipda.setInitExistence
      %
      %   obj.SETINITEXISTENCE(existence)
      %       Sets initial existece that is given to new tracks in integrated
      %       mode of Jipda. Argument existence must be a double scalar between
      %       0 and 1.
      assert(obj.existence_assert_(existence));
      obj.init_existence_ = existence;
    end
    
    function setInitThres(obj, thres)
      % m3t.jipda.Jipda.setInitThres
      %
      %   obj.SETINITTHRES(thres)
      %       Sets target initialisation threshold. Argument thres must be a
      %       double value between 0 and 1.
      assert(obj.probability_assert_(thres));
      obj.init_thres_ = thres;
    end
    
    function setMaxUndetectedFrames(obj, n)
      % m3t.jipda.Jipda.setMaxUndetectedFrames
      %
      %   obj.SETMAXUNDETECTEDFRAMES(n)
      %       Sets the maximum alowed number of consequtive frames in which no
      %       detections were assigned to a target before the target gets
      %       terminated. This is used only when integrated mode of Jipda is
      %       not enabled. Argument n must be a positive integer scalar.
      assert(obj.int_assert_(n));
      obj.max_undetected_frames_ = n;
    end
    
    function setMinDetectedFrames(obj, n)
      % m3t.jipda.Jipda.setMinDetectedFrames
      %
      %   obj.SETMINDETECTEDFRAMES(n)
      %       Sets the minimum number of consequtive frames in which at least
      %       one detection is assigned to a target before the target gets
      %       confirmed. This is used only when integrated mode of Jipda is not
      %       enabled. Argument n must be a positive integer scalar.
      assert(obj.int_assert_(n));
      obj.min_detected_frames_ = n;
    end
    
    function setSurvivalProbability(obj, ps)
      % m3t.jipda.Jipda.setSurvivalProbability
      %
      %   obj.SETSURVIVALPROBABILITY(ps)
      %       Sets the survival probability used to predict the targets
      %       existence. Argument ps must be a double scalar between 0 and 1.
      assert(obj.probability_assert_(ps));
      obj.survival_probability_ = ps;
    end
    
    function setTrackGenerator(obj, generator)
      % m3t.jipda.Jipda.setTrackGenerator
      %
      %   obj.SETTRACKGENERATOR(generator)
      %       Argument generator must be a handle to a function that generates
      %       the new Jipda tracks.
      assert(isa(generator,'function_handle'));
      obj.track_generator_ = generator;
    end
    
    function update(obj, detections)
      % m3t.jipda.Jipda.update
      %
      %   obj.UPDATE(detections)
      %       Runs the Jipda update step for a set of a detections. Argument
      %       detections must be an array of m3t.Detection objects.
      assert(isa(detections,'m3t.Detection'));
      
      num_dets = numel(detections);
      num_tracks = numel(obj.tracks_);
      new_existence = zeros(num_tracks, 1);
      
      if num_dets == 0
        return;
      end
      
      beta = zeros(num_dets, num_tracks);
      
      pd = obj.detection_probability_;
      pg = obj.gating_probability_;
      
      validation_matrix = false(num_dets, num_tracks);
      likelihoods = zeros(num_dets, num_tracks);
      
      if num_tracks > 0
        
        dt = detections(1).getTimestamp() - obj.tracks_(1).getTimestamp();
        obj.predict(dt);
        
        for i = 1:num_tracks
          for j = 1:num_dets
            [validation_matrix(j,i), likelihoods(j,i)] = ...
              obj.tracks_(i).validate(detections(j), pg);
          end
        end
        
        gate_volume = zeros(num_tracks, 1);
        if isempty(obj.clutter_density_)
          for i = 1:num_tracks
            gate_volume(i) = obj.tracks_(i).gateVolume(pg);
          end
        end
      
        % TODO(borna): use clustering only when number of possible associations 
        % is too large
        clusters = m3t.utils.matrixSegmentation(validation_matrix);
        num_clusters = numel(clusters);
        
        existence = zeros(num_tracks, 1);
        for i = 1:num_tracks
          existence(i) = obj.tracks_(i).getExistence();
          new_existence(i) = ...
            existence(i) * (1 - pd * pg)/(1 - pd * pg * existence(i)); 
        end
        
        if num_clusters > 0
          for k = 1:num_clusters
            rows = clusters(k).rows;
            cols = clusters(k).cols;
            
            % if density of false alarms is not specified, use
            % nonparametric clutter model
            if ~obj.parametric_
              v_ap = numel(rows) / sum(sum(validation_matrix(rows,cols))) ...
                * sum(gate_volume(cols));
              v = max(v_ap, max(gate_volume));
              clutter_density = 0;
              for i = 1:numel(rows)
                temp = 1;
                for j = 1:numel(cols)
                  if validation_matrix(rows(i),cols(j))
                    temp = temp * ...
                      (1 - pd * pg ...
                      * existence(cols(j)) ...
                      / sum(validation_matrix(rows,cols(j))));
                  end
                end
                clutter_density = clutter_density + temp;
              end
              clutter_density = clutter_density / v;
            else
              clutter_density = obj.clutter_density_;
            end
            
            [beta_temp, w_temp] = obj.softAssignment(...
              likelihoods(rows,cols),...
              validation_matrix(rows,cols),...
              existence(cols),...
              clutter_density);
            
            beta(rows,cols) = beta_temp;
            new_existence(cols) = w_temp;
          end
        end
      end
      
      obj.trackManagement(detections, beta, new_existence);
      
    end
    
  end
  
  methods (Access = protected)
    
    function tracks = createNewTracks(obj, detections, likelihoods)
      % m3t.jipda.Jipda.createNewTracks
      %
      %   Creates and initialises new targets based on received detections and
      %   likelihoods that the detections don't belong to tracked objects.
      n = numel(detections);
      tracks = m3t.jipda.tracks.Track.empty();
      for i = 1:n
        tracks(i,1) = ...
          obj.track_generator_(detections(i));
        if obj.integrated_
          tracks(i,1).setExistence(obj.init_existence_ * likelihoods(i));
        else
          tracks(i,1).setExistence(1);
        end
        tracks(i,1).setTimestamp(detections(i).getTimestamp());
      end
    end
    
    function id = nextId(obj)
      % m3t.jipda.Jipda.nextId
      %
      %   obj.NEXTID()
      %       Returns the next valid target id number.
      obj.id_counter_ = obj.id_counter_ + int32(1);
      id = obj.id_counter_;
      if id == intmax('int32')
        obj.id_counter_ = int32(-1);
      end
    end
    
    function predict(obj, dt)
      % m3t.jipda.Jipda.predict
      %
      %   obj.PREDICT(dt)
      %       Predicts the states of targets after time dt. Argument dt must be
      %       a positive double scalar.
      assert(isa(dt, 'double') && isscalar(dt) && dt > -10*eps);
      for i = 1:numel(obj.tracks_)
        track = obj.tracks_(i);
        if obj.integrated_
          existence = track.getExistence() * obj.survival_probability_;
          track.setExistence(existence);
        end
        track.predict(dt);
        track.setTimestamp(track.timestamp_ + dt);
      end
    end
    
    function [beta, post] = softAssignment(obj, probs, val, pri, lambda)
      %SOFTASSIGNMENT - soft measurement to track assignment
      %
      %   Computes posterior association probabilities between the set of I
      %   measurements and set of J tracked objects and posterior existence
      %   probabilities of tracks based on prior existence probabilities and
      %   measurement likelihoods.
      %
      %   Inputs:
      %
      %      probs - I�J matrix of a-priori association probabilities
      %        val - a logical I�J matrix of valid assignments
      %        pri - 1xJ vector of tracks' prior existence probabilities
      %     lambda - density of a false detections (clutter) per unit of a
      %                measurement space
      %
      %   Outputs:
      %
      %     beta - I�J matrix of a-posteriori measurement-to-track
      %                association probabilities
      %     post - 1�J vector of objects' posterior existence probabilities
      %
      num_measurements = int32(size(probs,1));
      num_tracks = int32(size(probs,2));
      pd = obj.detection_probability_;
      pg = obj.gating_probability_;
      % precalculate some values that are used frequently in recursion
      p = pd .* pg .* pri;
      p_i = probs / lambda;
      % matrix of a-posteriori likelihoods that measurement i was
      % generated by track j
      beta = zeros(num_measurements,num_tracks);
      % logical matrix of current track to measurement associations
      assignments = false(num_measurements,num_tracks);
      % sum of a-posteriori probabilities of joint event, needed to
      % normalize beta and beta0
      p_sum = 0;
      % stack pointer
      i = int32(1);
      % table of measurement hypotheses of tracks, 0 means that the track is
      % not detected
      stack_state = int32(zeros(1,num_tracks));
      
      % iterate through all joint events
      while i > 0
        % recursion stop condition:
        if i > num_tracks
          % a-posteriori probability of current joint event
          p_temp = prod(1 - p(~sum(assignments,1))) * ...
            prod(p(logical(sum(assignments,1)))) * ...
            prod(p_i(assignments));
          
          % update the sum of probabilities of joint events, update the
          % a-posterior probabilities of active assignment hypotheses, update
          % the number of events
          p_sum = p_sum + p_temp;
          beta(assignments) = beta(assignments) + p_temp;
          
          while true
            i = i - 1;
            if i == 0
              break;
            end
            
            % deallocate the measurement assigned to track i
            if stack_state(i) > 0
              assignments(stack_state(i),i) = false;
            end
            
            while true
              % find next valid measurement for track i
              stack_state(i) = stack_state(i) + 1;
              if stack_state(i) > num_measurements || ...
                  sum(stack_state == stack_state(i)) == 1 && ...
                  val(stack_state(i),i)
                
                % allocate new measurement to track i
                if stack_state(i) <= num_measurements
                  assignments(stack_state(i),i) = true;
                end
                break;
              end
            end
            
            if stack_state(i) > num_measurements
              stack_state(i) = 0;
            else
              break;
            end
          end
          
        else
          i = i + 1;
        end
      end
      
      if p_sum > 0
        beta = beta/p_sum;
      end
      beta0 = (1 - pd.*pg).*pri ...
        ./(1 - p).*(1 - sum(beta, 1)');
      % update existence probabilities
      post = beta0 + sum(beta, 1)';
      % normalise a-posteriori association probabilities
      beta = beta ./ post';
    end
    
    function trackManagement(obj, detections, beta, existence)
      %TRACKMANAGEMENT - track management logic of jipda
      %
      %   Logic for deleting old tracks and confirming potential tracks and
      %   creating new track candidates given sensor measurements and
      %   association probabilities. When the jipda is in the integrated mode,
      %   tracks are confirmed and deleted depending on their existence
      %   probability and confirm and deleting threshold of jipda. When in
      %   not-integrated mode, tracks are deleted when they are not detected in
      %   n consecutive frames, where n is the max_undetected_frames_ member
      %   and they are confirmed when they appear in n consecutive frames, where
      %   n is min_detected_frames_ member.
      %
      %   Inputs:
      %
      %     detections - a list of observations in curent frame
      %           beta - posterior association probabilities
      %      existence - posterior existence probabilities of targets
      %
      for i = 1:numel(obj.tracks_)
        track = obj.tracks_(i);
        if (obj.integrated_ && existence(i) < obj.delete_thres_) || ...
           (~obj.integrated_ && ...
           track.getUndetectedCount() >= obj.max_undetected_frames_)
          track.delete();
        else
          if ~track.isConfirmed() && ...
              ((obj.integrated_ && ...
              track.getExistence() > obj.confirm_thres_) || ...
              (~obj.integrated_ && ...
              track.getDetectedCount() >= obj.min_detected_frames_))
            track.confirm();
            track.setId(obj.nextId());
          end
          track.setExistence(existence(i));
          track.detectedInCurrentFrame(abs(1 - sum(beta(:,i))) < 0.01); 
          track.update(detections, beta(:,i));
        end
      end
      obj.tracks_(~obj.tracks_.isvalid()) = [];
      
      new_candidates = ...
        detections((1 - sum(beta,2)) >= obj.init_thres_);
      new_likelihoods = (1 - sum(beta,2));
      new_likelihoods = new_likelihoods(new_likelihoods >= obj.init_thres_);
      
      new_tracks = obj.createNewTracks(new_candidates, new_likelihoods);
      
      obj.tracks_ = [obj.tracks_; new_tracks];
    end
    
    % Inherited from matlab.mixin.Copyable
    function obj_copy = copyElement(obj)
      % m3t.jipda.Jipda.copyElement
      %
      %   Creates deep copy of Jipda tracker. Inherited from m3t.Tracker.
      obj_copy = copyElement@m3t.Tracker(obj);
      obj_copy.tracks_ = obj.tracks_.copy();
    end
    
  end
  
end