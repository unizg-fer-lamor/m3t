% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------

classdef VmfTrack < m3t.jipda.tracks.Track
  
  properties (Access = private)
    filter_;
  end
  
  methods
    
    function obj = VmfTrack(vmf)
      obj@m3t.jipda.tracks.Track();
      assert(isa(vmf, 'm3t.estimation.vmf.VonMisesFisherFilter'));
      obj.filter_ = vmf;
    end
    
    function vol = gateVolume(obj, pg)
      dim = double(obj.filter_.system_dim_);
      kx = obj.filter_.getKappa();
      ko = obj.filter_.getObservationKappa();
      ks = m3t.stat.directional.vmf.besselRatioInverse(dim + 1, ...
        m3t.stat.directional.vmf.besselRatio(dim + 1, kx) * ...
        m3t.stat.directional.vmf.besselRatio(dim + 1, ko));
      theta = m3t.stat.directional.vmf.quantiles(pg, ks, dim + 1);
      temp = betainc(sin(theta)^2, 0.5 * (dim - 1), 0.5);
      vol = 2 * pi^(0.5 * (dim - 1)) * temp / gamma(0.5 * (dim - 1));
      if theta > 0.5 * pi
        vol = 2 * pi^(0.5 * (dim - 1)) / gamma(0.5 * (dim - 1)) - vol;
      end
    end
    
    function kappa = getConcentration(obj)
      kappa = obj.filter_.getKappa();
    end
    
    function vmf = getFilter(obj)
      vmf = obj.filter_.copy();
    end
    
    function x = getState(obj)
      x = obj.filter_.getX();
    end
    
    function predict(obj, timestamp)
      obj.filter_.predict(timestamp);
    end
    
    function update(obj, detections, weights)
      obj.filter_.weightedUpdate(weights, detections);
    end
    
    function [valid, likelihood] = validate(obj, detection, pg)
      mu = obj.filter_.getX();
      dim = obj.filter_.system_dim_;
      kx = obj.filter_.getKappa();
      ko = obj.filter_.getObservationKappa();
      ks = m3t.stat.directional.vmf.besselRatioInverse(dim, ...
        m3t.stat.directional.vmf.besselRatio(dim, kx) * ...
        m3t.stat.directional.vmf.besselRatio(dim, ko));
      y = detection.getValue();
      if m3t.stat.directional.vmf.confidenceTest(y, mu, ks, pg)
        valid = true;
        likelihood = m3t.stat.directional.vmf.pdf(y, mu, ks) / pg;
      else
        valid = false;
        likelihood = 0;
      end
    end
    
  end
  
end

