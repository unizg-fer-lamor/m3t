% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------

classdef KalmanTrack < m3t.jipda.tracks.Track
  
  properties (Access = private)
    filter_;
  end
  
  methods
    
    function obj = KalmanTrack(kf)
      obj@m3t.jipda.tracks.Track();
      assert(isa(kf, 'm3t.estimation.kalman.KalmanFilter'));
      obj.filter_ = kf;
    end
    
    function v = gateVolume(obj, pg)
      dim = double(obj.filter_.output_dim_);
      c = pi^(dim/2) / gamma(dim/2 + 1);
      v = c * chi2inv(pg, dim) * det(obj.filter_.getS())^(1/2);
    end
    
    function P = getCovariance(obj)
      P = obj.filter_.getP();
    end
    
    function kf = getFilter(obj)
      kf = obj.filter_.copy();
    end
    
    function S = getMeasurementCovariance(obj)
      S = obj.filter_.getS();
    end
    
    function x = getState(obj)
      x = obj.filter_.getX();
    end
    
    function y = getPredictedMeasurement(obj)
      y = obj.filter_.getYHat();
    end
    
    function predict(obj, dt)
      obj.filter_.predict(dt);
    end
    
    function update(obj, detections, weights)
      obj.filter_.weightedUpdate(weights, detections);
    end
    
    function [valid, likelihood] = validate(obj, detection, pg)
      dim = double(obj.filter_.output_dim_);
      mu = obj.filter_.getYHat() - detection.getValue();
      S = obj.filter_.getS();
      
      if mu' / S * mu < chi2inv(pg, dim)
        valid = true;
        likelihood = mvnpdf(mu, [], S) / pg;
      else
        valid = false;
        likelihood = 0;
      end
    end
    
  end
  
end

