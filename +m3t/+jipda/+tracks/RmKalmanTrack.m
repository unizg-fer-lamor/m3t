% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------

classdef RmKalmanTrack < m3t.jipda.tracks.Track
  
  properties (Access = private)
    filter_;
  end
  
  methods
    
    function obj = RmKalmanTrack(rmkf)
      obj@m3t.jipda.tracks.Track();
      obj.filter_ = rmkf;
    end
    
    function vol = gateVolume(obj, pg)
      vol = [];
    end
    
    function P = getCovariance(obj)
      P = obj.filter_.P_;
    end
    
    function x = getState(obj)
      x = obj.filter_.x_;
    end
    
    function predict(obj, timestamp)
      obj.filter_.predict([], timestamp);
    end
    
    function update(obj, detections, weights)
      obj.filter_.weightedUpdate(weights, detections);
    end
    
    function [valid, likelihood] = validate(obj, detection, pg)
      manifold = obj.filter_.getMeasurementManifold();
      dim = double(manifold.dim());
      
      y_hat = obj.filter_.y_hat_();
      y  = detection.getValue();
      
      % log will fail if the detection is outside of the injectivity area of exp
      % so use try-catch to handle that
      try
        mu = manifold.log(y_hat, y).getValue();
      catch ex
        valid = false;
        likelihood = 0;
        return;
      end

      S = obj.filter_.S_;
      
      if mu' / S * mu < chi2inv(pg, dim)
        valid = true;
        likelihood = mvnpdf(mu, [], S) / pg;
      else
        valid = false;
        likelihood = 0;
      end
    end
    
  end
  
end

