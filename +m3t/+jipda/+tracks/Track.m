% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------

classdef (Abstract) Track < m3t.Track
  %TRACK Summary of this class goes here
  %   Detailed explanation goes here
  
  properties (Access = ?m3t.jipda.Jipda)
    confirmed_;
    detected_count_;
    existence_;
  end
  
  methods
    
    function obj = Track()
      obj.existence_ = 0;
      obj.confirmed_ = false;
      obj.detected_count_ = int32(0);
    end
    
    function confirm(obj)
      obj.confirmed_ = true;
    end
    
    function n = getDetectedCount(obj)
      n = max(obj.detected_count_, 0);
    end
    
    function detectedInCurrentFrame(obj, flag)
      assert(islogical(flag) && isscalar(flag), ...
        ['Argument flag must be a logical scalar.', newline]);
      if flag
        obj.detected_count_ = max(obj.detected_count_, 0) + int32(1);
      else
        obj.detected_count_ = min(obj.detected_count_, 0) - int32(1);
      end
    end
    
    function e = getExistence(obj)
      e = obj.existence_;
    end
    
    function n = getUndetectedCount(obj)
      n = -min(obj.undetected_count_, 0);
    end
    
    function c = isConfirmed(obj)
      c = obj.confirmed_;
    end
    
    function setExistence(obj,existence)
      assert(isscalar(existence) ...
        && isa(existence,'double') ...
        && existence >= 0 ...
        && existence <= 1,...
        ['Argument confidence must be a real number between 0 and 1.',newline]);
      obj.existence_ = existence;
    end
    
  end
  
  methods (Abstract)
    
    % Calculates the volume of validation gate around the track given gating
    % probability pg.
    vol = gateVolume(obj, pg);
    
    % Predicts track's state in time <timestamp>.
    predict(obj, timestamp);
    
    % Updates track's state using soft assignment of detections and their
    % weights (association probabilities).
    update(obj, detections, weights);
    
    % Checks if the detection is inside track's validation area given by gating
    % probability pg and calculates the likelihood g(detection | track).
    [valid, likelihood] = validate(obj, detection, pg);
    
  end
  
  
end

