% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
classdef ConstVelocityRmUkf < m3t.estimation.rm.RmUkf
  
  properties
    sigma_q_;
    sigma_r_;
  end
  
  methods

    function obj = ConstVelocityRmUkf(bundle)
      obj@m3t.estimation.rm.RmUkf(bundle, bundle.getBaseManifold());
    end
    
  end

  methods (Access = protected)

    function x_new = f(obj, x, ~, dt)
      bundle = obj.state_manifold_;
      manifold = obj.measurement_manifold_;

      p = x.getBasePoint();
      v = x.getTangent();

      p_new = manifold.exp(p, dt * v);
      v_new = manifold.parallelTransport(v, p, dt * v);

      x_new = m3t.geometry.rm.BundlePoint(bundle, p_new, v_new);
    end

    function y = h(~, x)
      y = x.getBasePoint();
    end

    function Q = getQ(obj, x, dt)
      dim = obj.state_manifold_.dim();
      Q = diag(obj.sigma_q_);
      V = obj.state_manifold_.basis(x);
      V_mat = zeros(dim);
      for i = 1:dim
        V_mat(:, i) = V(i).getValue();
      end
      Q = V_mat * Q * V_mat';
    end

    function R = getR(obj, y)
      dim = obj.measurement_manifold_.dim();
      R = eye(dim) * obj.sigma_r_;
      V = obj.measurement_manifold_.basis(y);
      V_mat = zeros(dim);
      for i = 1:dim
        V_mat(:, i) = V(i).getValue();
      end
      R = V_mat * R * V_mat';
    end

  end

end

