% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
classdef ConstModelRmUkf < m3t.estimation.rm.RmUkf
  
  properties 
    sigma_q_;
    sigma_r_;
  end
  
  methods

    function obj = ConstModelRmUkf(manifold)
      obj@m3t.estimation.rm.RmUkf(manifold, manifold);
    end
    
  end

  methods (Access = protected)

    function x_new = f(~, x, ~, ~)
      x_new = x.copy();
    end

    function y_hat = h(~, x)
      y_hat = x.copy();
    end

    function Q = getQ(obj, x, dt)
      V = obj.state_manifold_.basis(x);
      Q = zeros(obj.state_manifold_.dim());
      for i = 1:numel(V)
        val = V(i).getValue();
        Q = Q * (val * val') * obj.sigma_q_;
      end
    end

    function R = getR(obj, y_hat)
      V = obj.measurement_manifold_.basis(y_hat);
      R = zeros(obj.measurement_manifold_.dim());
      for i = 1:numel(V)
        val = V(i).getValue();
        R = R + (val * val') * obj.sigma_r_;
      end
    end
  end

end

