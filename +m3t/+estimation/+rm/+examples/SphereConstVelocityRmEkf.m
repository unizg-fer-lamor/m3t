% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
classdef SphereConstVelocityRmEkf < m3t.estimation.rm.RmEkf
  
  properties 
    sigma_q_;
    sigma_r_;
  end
  
  methods
    
    function obj = SphereConstVelocityRmEkf(sphere)
      assert(isa(sphere, 'm3t.geometry.rm.Sphere'));
      bundle = m3t.geometry.rm.TangentBundle(sphere);
      bundle.setLogOptimisationSettings( ...
        'maxIter', 10, ...
        'relTol', 1e-3, ...
        'absTol', 1e-3, ...
        'useConjugate', false);
      obj@m3t.estimation.rm.RmEkf(bundle, sphere);
    end

  end

  methods (Access = protected)
    
    function x = f(obj, x_old, ~, dt)
      bundle = obj.state_manifold_;
      sphere = bundle.getBaseManifold();

      p_old = x_old.getBasePoint();
      v_old = x_old.getTangent();

      p = sphere.exp(p_old, dt * v_old);
      v = sphere.parallelTransport(v_old, p_old, dt * v_old);

      x = m3t.geometry.rm.BundlePoint(bundle, p, v);
    end

    function y = h(~, x)
      y = x.getBasePoint();
    end

    function F = getF(obj, x, ~, dt)
      bundle = obj.state_manifold_;
      sphere = bundle.getBaseManifold();
      dim = sphere.euclideanDim();
      chart1 = x.getChart();

      x_new = obj.f(x, [], dt);
      chart2 = x_new.getChart();

      I = eye(dim);

      p = x.getBasePoint();
      v = x.getTangent();

      xval = sphere.pointToEuclidean(p);
      vval = sphere.tangentToEuclidean(p, v);

      norm_v = norm(vval);
      r = norm(xval);

      xv = xval * vval';
      vv = vval * vval';
      rr = r*r;
      norm_v2 = norm_v * norm_v;
      norm_v3 = norm_v2 * norm_v;

      th = dt * norm_v / r;
      cos_th = cos(th);
      sin_th = sin(th);

      df1_dx = I * cos_th;
      df1_dv = I * dt / th * sin_th ...
        - xv * th / norm_v2 * sin_th ...
        + vv * (dt / norm_v2 * cos_th - r / norm_v3 * sin_th);
      df2_dx = - I * norm_v / r * sin_th;
      df2_dv = I * cos_th ...
        - vv * dt / r / norm_v * sin_th ...
        - xv * (sin_th / r / norm_v + dt / rr * cos_th);

      if norm_v <= 1e-5
        df1_dv = I * dt;
        df2_dv = I;
      end

      F = [df1_dx, df1_dv;
        df2_dx, df2_dv];

      J1 = bundle.getChartTransitionJacobian(x_new, int32(0), chart2);
      J2 = bundle.getChartTransitionJacobian(x, chart1, int32(0));

      F = J1 * F * J2;
    end

    function H = getH(obj, x)
      bundle = obj.state_manifold_;
      sphere = bundle.getBaseManifold();
      dim = sphere.euclideanDim();
      chart1 = x.getChart();

      y = obj.h(x);
      chart2 = y.getChart();
      
      H = [eye(dim) zeros(dim)];

      J1 = sphere.getChartTransitionJacobian(y, int32(0), chart2);
      J2 = bundle.getChartTransitionJacobian(x, chart1, int32(0));

      H = J1 * H * J2;
    end

    function L = getL(obj, x, ~, dt)
      bundle = obj.state_manifold_;
      sphere = bundle.getBaseManifold();
      dim = sphere.euclideanDim();
      chart1 = x.getChart();

      x_new = obj.f(x, [], dt);
      chart2 = x_new.getChart();

      I = eye(dim);

      p = x.getBasePoint();
      v = x.getTangent();

      xval = sphere.pointToEuclidean(p);
      vval = sphere.tangentToEuclidean(p, v);

      norm_v = norm(vval);
      norm_v2 = norm_v * norm_v;
      r = norm(xval);

      xv = xval * vval';
      vv = vval * vval';

      th = dt * norm_v / r;
      cos_th = cos(th);
      sin_th = sin(th);

      L = [zeros(dim); 
        I - xv / r / norm_v * sin_th - vv / norm_v2 * (1 - cos_th)];
      
      if norm_v <= 1e-6
        L = [zeros(dim); I];
      end

      J1 = bundle.getChartTransitionJacobian(x_new, int32(0), chart2);
      J2 = sphere.getChartTransitionJacobian(...
        x.getBasePoint(), chart1, int32(0));

      L = J1 * L * J2;
    end

    function M = getM(obj, ~)
      M = eye(obj.measurement_manifold_.dim());
    end

    function Q = getQ(obj, x, ~)
      sphere = obj.state_manifold_.getBaseManifold();
      dim = sphere.dim();
      Q = eye(dim) * obj.sigma_q_;
      V = sphere.basis(x.getBasePoint());
      V_mat = zeros(dim);
      for i = 1:dim
        V_mat(:, i) = V(i).getValue();
      end
      Q = V_mat * Q * V_mat';
    end
    
    function R = getR(obj, y)
      dim = obj.measurement_manifold_.dim();
      R = eye(dim) * obj.sigma_r_;
      V = obj.measurement_manifold_.basis(y);
      V_mat = zeros(dim);
      for i = 1:dim
        V_mat(:, i) = V(i).getValue();
      end
      R = V_mat * R * V_mat';
    end

  end

end

