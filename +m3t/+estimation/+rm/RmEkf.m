% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
%
%   m3t.estimation.rm.RmEkf
%
% ------------------------------------------------------------------------------
classdef (Abstract) RmEkf < matlab.mixin.Copyable

  properties (SetAccess = protected)
    x_;
    P_;
    S_;
    y_hat_;
    state_manifold_;
    measurement_manifold_;
  end

  methods (Abstract, Access = protected)
    x = f(obj, x_old, u, dt);
    y = h(obj, x)
    F = getF(obj, x, u, dt);
    H = getH(obj, x);
    L = getL(obj, x, u, dt);
    M = getM(obj, x);
    Q = getQ(obj, x, dt);
    R = getR(obj, y);
  end

  methods

    function manifold = getStateManifold(obj)
      manifold = obj.state_manifold_;
    end

    function manifold = getMeasurementManifold(obj)
      manifold = obj.measurement_manifold_;
    end

    function obj = RmEkf(state_manifold, measurement_manifold)
      assert(isa(state_manifold, 'm3t.geometry.rm.Manifold'));
      assert(isa(measurement_manifold, 'm3t.geometry.rm.Manifold'));
      obj.state_manifold_ = state_manifold;
      obj.measurement_manifold_ = measurement_manifold;
    end

    function predict(obj, u, dt)
      x = obj.x_;
      P = obj.P_;

      % calculate Jacobians:
      F = obj.getF(x, u, dt);
      L = obj.getL(x, u, dt);

      % process noise covariance:
      Q = obj.getQ(x, dt);

      % predict state:
      x_new = obj.f(x, u, dt);
      P_new = F * P * F' + L * Q * L';

      %predict measurement:
      y_hat = obj.h(x_new);

      % calculate Jacobians:
      H = obj.getH(x_new);
      M = obj.getM(y_hat);

      R = obj.getR(y_hat);
      S = H * P_new * H' + M * R * M';

      obj.x_ = x_new;
      obj.P_ = P_new;
      obj.y_hat_ = y_hat;
      obj.S_ = S;
    end

    function setX(obj, x0)
      assert(isa(x0, 'm3t.geometry.rm.AbstractPoint') ...
        && obj.state_manifold_.isequal(x0.getManifold()));
      obj.x_ = x0;
    end

    function setP(obj, P0)
      assert(isa(P0, 'double') && all(size(P0) == obj.state_manifold_.dim()));
      obj.P_ = P0;
    end

    function update(obj, y)
      manifold_M = obj.state_manifold_;
      manifold_N = obj.measurement_manifold_;

      x = obj.x_;
      P = obj.P_;
      y_hat = obj.y_hat_;
      S = obj.S_;

      H = obj.getH(x);

      % calculate innovation:
      mu = manifold_N.log(y_hat, y);

      R = obj.getR(y_hat);
      M = obj.getM(y_hat);

      % Kalman gain:
      K = P * H' / S;
      
      B = eye(manifold_M.dim()) - K * H;
      P_new = B * P * B' + K * M * R * M' * K'; 
      
      dx = manifold_M.createTangent(x, K * mu.getValue());
      x_new = manifold_M.exp(x, dx);

      P_new = m3t.geometry.rm.utils.covarianceParallelTransport(...
        P_new, ...
        x, ...
        dx);

      obj.x_ = x_new;
      obj.P_ = P_new;
    end

    function weightedUpdate(obj, weights, detections)
      manifold_M = obj.state_manifold_;
      manifold_N = obj.measurement_manifold_;
      
      dim_m = double(manifold_M.dim());
      dim_n = double(manifold_N.dim());

      num_d = numel(detections);

      if num_d > 0
        x = obj.x_;
        P = obj.P_;
        y_hat = obj.y_hat_;
        S = obj.S_;

        H = obj.getH(x);

        R = obj.getR(y_hat);
        M = obj.getM(y_hat);

        % Kalman gain:
        K = P * H' / S;

        y_sum = manifold_N.createTangent(y_hat, zeros(dim_n, 1));
        A = zeros(dim_n, dim_n);

        for i = 1:num_d
          try
            y = manifold_N.log(y_hat, detections(i).getValue());
          catch ex
            continue;
          end
          y_val = y.getValue();
          y_sum = y_sum + weights(i) * y;
          A = A + weights(i) * (y_val * y_val');
        end

        mu = y_sum.getValue();
        A = A - mu * mu';

        B = eye(dim_m) - K * H;
        P_new = B * P * B' + K * M * R * M' * K' + K * A * K';
        
        dx = manifold_M.createTangent(x, K * mu);
        
        obj.x_ = manifold_M.exp(x, dx);
        obj.P_ = m3t.geometry.rm.utils.covarianceParallelTransport(P_new, x, dx);
        
      end
    end

  end

end

