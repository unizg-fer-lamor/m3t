% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
classdef (Abstract) RmUkf < matlab.mixin.Copyable
  
  properties (Access = protected)
    alpha_;                 % controls spread of sigma points
    beta_;                  % depends on the distribution
    kappa_;                 % controls spread of sigma points
    state_manifold_;        % system state space manifold
    measurement_manifold_;  % observation space manifold
  end
  
  properties (SetAccess = protected)
    x_;         % system state
    P_;         % system state covariance
    S_;         % predicted measurement covariance
    Cxy_;       % cross-covariance
    y_hat_;     % predicted measurement
  end

  methods (Abstract, Access = protected)
    x_new = f(obj, x, u, dt);
    y_hat = h(obj, x);
    Q = getQ(obj, x, dt);
    R = getR(obj, y_hat);
  end
  
  methods
    
    function obj = RmUkf(state_manifold, measurement_manifold)
      obj.state_manifold_ = state_manifold;
      obj.measurement_manifold_ = measurement_manifold;
      obj.alpha_ = sqrt(0.5);
      obj.beta_ = 2;
      obj.kappa_ = 1;
    end

    function Cxy = getCxy(obj)
      % m3t.estimation.rm.UnscentedKalman.getCxy
      %   Returns cross-covariance matrix
      Cxy = obj.Cxy_;
    end
    
    function manifold = getStateManifold(obj)
      manifold = obj.state_manifold_;
    end

    function manifold = getMeasurementManifold(obj)
      manifold = obj.measurement_manifold_;
    end

    function predict(obj, ~, dt)
      obj.predictState(dt);
      obj.predictMeasurement();
    end
        
    function setLambda(obj,lambda)
      % m3t.estimation.rm.UnscentedKalman.setLambda
      %
      %   obj.SETLAMBDA(lambda)
      %       Sets the lambda parameter of UKF which controls the spread of
      %       sigma points. Argument lambda must be a scalar of a double type.
      assert(isa(lambda,'double') && isscalar(lambda));
      obj.lambda_ = lambda;
    end
   
    function setP(obj,P)
      % m3t.estimation.rm.UnscentedKalman.setP
      %
      %   obj.SETP(P)
      %       Sets the filter's state covariance matrix. Argument P must be a
      %       positive semidefinite matrix with rank equal to dimension of a
      %       state space manifold.
      obj.P_ = P;
    end
    
    function setX(obj, x)
      obj.x_ = x;
    end
    
    function update(obj, y)
      manifold_M = obj.state_manifold_;
      manifold_N = obj.measurement_manifold_;
      
      dim_m = double(manifold_M.dim());
      
      x = obj.x_;
      P = obj.P_;
      S = obj.S_;
      Cxy = obj.Cxy_;
      y_hat = obj.y_hat_;
      
      R = obj.getR(y_hat);
      
      K = Cxy / S;
      
      mu = manifold_N.log(y_hat, y);
      
      B = eye(dim_m) - K * Cxy' / P;
      P = B * P * B' + K * R * K';
      
      %P = m3t.utils.nearestSPD(P);
      P = (P + P') / 2;
      
      dx = manifold_M.createTangent(x, K * mu.getValue());
      
      obj.x_ = manifold_M.exp(x, dx);
      obj.P_ = m3t.geometry.rm.utils.covarianceParallelTransport(P, x, dx);
      
      obj.predictMeasurement();
    end
    
    function weightedUpdate(obj, weights, detections)
      manifold_M = obj.state_manifold_;
      manifold_N = obj.measurement_manifold_;
      
      dim_m = double(manifold_M.dim());
      dim_n = double(manifold_N.dim());
      
      num_d = numel(detections);
      
      if num_d > 0
        
        x = obj.x_;
        P = obj.P_;
        S = obj.S_;
        Cxy = obj.Cxy_;
        y_hat = obj.y_hat_;
        
        R = obj.getR(y_hat);
        
        K = Cxy / S;
        
        y_sum = manifold_N.createTangent(y_hat, zeros(dim_n, 1));
        A = zeros(dim_n, dim_n);
        
        for i = 1:num_d
          y = manifold_N.log(y_hat, detections(i).getValue());
          y_val = y.getValue();
          y_sum = y_sum + weights(i) * y;
          A = A + weights(i) * (y_val * y_val');
        end
        mu = y_sum.getValue();
        A = A - mu * mu';
        
        B = eye(dim_m) - K * Cxy' / P;
        P = B * P * B' + K * R * K' + K * A * K';
        
        %P = m3t.utils.nearestSPD(P);
        P = (P + P') / 2;
        
        dx = manifold_M.createTangent(x, K * mu);
        
        obj.x_ = manifold_M.exp(x, dx);
        obj.P_ = m3t.geometry.rm.utils.covarianceParallelTransport(P, x, dx);
        
        obj.predictMeasurement();
      end
    end
    
  end
  
  methods (Access = protected)
    
    function predictState(obj, dt)
      manifold = obj.state_manifold_;
      
      dim = double(manifold.dim());
      
      a = obj.alpha_;
      b = obj.beta_;
      k = obj.kappa_;
      
      x = obj.x_;
      P = obj.P_;
      
      %a = 0.05 / max(eig(P));

      % generate sigma vectors on the tangent space of the state and their
      % weights
      sigma_vec = a * sqrt(k) * cholcov(P);
      temp = a * a * k;
      weights_a = [(temp  - dim) / temp, repmat(1 / (2 *  temp), 1, 2 * dim)];
      weights_c = weights_a;
      weights_c(1) = weights_c(1) + 1 - a*a + b;
      
      % project sigma vectors to the manifold (creating sigma points)
      sigma = m3t.geometry.rm.Point(...
        manifold, x.getChart(), zeros(dim, 2 * dim + 1));
      sigma(1) = x;
      for i = 1:dim
        temp = manifold.createTangent(x, sigma_vec(:, i));
        sigma(1 + i) = manifold.exp(x, temp);
        sigma(1 + i + dim) = manifold.exp(x, -temp);
      end
      
      % evaluate state trasition model to each sigma point
      for i = 1:(2 * dim + 1)
        sigma(i) = obj.f(sigma(i), [], dt);
      end
      
      % estimate mean of predicted sigma points
      x = manifold.karcherMean(sigma, weights_a, sigma(1));
      
      % get the process noise covariance at predicted mean
      P = obj.getQ(x, dt);
      
      % project predicted sigma points to tangent space at estimated mean
      for i = 1:(2 * dim + 1)
        temp = manifold.log(x, sigma(i)).getValue();
        P = P + weights_c(i) * (temp * temp');
      end
      
      P = (P + P')/2;
      
      obj.x_ = x;
      obj.P_ = P;
    end
    
    function predictMeasurement(obj)
      manifold_M = obj.state_manifold_;
      manifold_N = obj.measurement_manifold_;
      
      dim_m = double(manifold_M.dim());
      dim_n = double(manifold_N.dim());
      
      a = obj.alpha_;
      b = obj.beta_;
      k = obj.kappa_;
      
      x = obj.x_;
      P = obj.P_;

      %a = 0.05 / max(eig(P));
      
      % generate sigma vectors on the tangent space of the state
      sigma_vec = a * sqrt(k) * cholcov(P);
      sigma_vec2 = [zeros(dim_m, 1), sigma_vec, -sigma_vec];
      temp = a * a * k;
      weights_a = [(temp  - dim_m) / temp, repmat(1 / (2 *  temp), 1, 2 * dim_m)];
      weights_c = weights_a;
      weights_c(1) = weights_c(1) + 1 - a*a + b;
      
      % project sigma points to the manifold
      sigma = m3t.geometry.rm.Point(...
        manifold_M, x.getChart(), zeros(dim_m, 2 * dim_m + 1));
      sigma(1) = x;
      for i = 1:dim_m
        temp = manifold_M.createTangent(x, sigma_vec(:, i));
        sigma(1 + i) = manifold_M.exp(x, temp);
        sigma(1 + i + dim_m) = manifold_M.exp(x, -temp);
      end
      
      % project sigma points to measurement space
      sigma_z = m3t.geometry.rm.Point(...
        manifold_N, x.getChart(), zeros(dim_n, 2 * dim_m + 1));
      for i = 1:(2 * dim_m + 1)
        sigma_z(i) = obj.h(sigma(i));
      end
      
      % estimate the mean of predicted sigma points (predicted measurement)
      y_hat = manifold_N.karcherMean(sigma_z, weights_a, sigma_z(1));
      %y_hat = sigma_z(1);
      
      S = obj.getR(y_hat);
      Cxy = zeros(dim_m, dim_n);
      for i = 1:(2 * dim_m + 1)
        temp_z = manifold_N.log(y_hat, sigma_z(i)).getValue();
        S =  S + weights_c(i) * (temp_z * temp_z');
        Cxy = Cxy + weights_c(i) * (sigma_vec2(:, i) * temp_z');
      end
      
      obj.S_ = S;
      obj.Cxy_ = Cxy;
      obj.y_hat_ = y_hat;
    end
    
  end
  
end

