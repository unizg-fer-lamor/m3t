% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
%
%   m3t.estimation.vmf.VonMisesFisherFilter
%
%     A discrete filtering algorithm for directional data. The state
%     uncertainty is described by the von Mises-Fisher distribution that is
%     defined by two parameters: mean direction and concentration. It is
%     supposed that the target moves slowly compared to the sampling time.
%     Mesurement likelihood is modeled as a von Mises-Fisher distribution.
%
%     VonMisesFisherFilter is a subclass of matlab.mixin.Copyable class.
%
%     VonMisesFisherFilter properties:
%       dim_  - dimension of filtering data
%
%     VonMisesFisherFilter methods:
%       predict   - von Mises-Fisher filter prediction 
%       update    - von Mises-Fisher filter update
%
%       weightedUpdate  - probabilistic data association (PDA) update
%
%       getX                  - returns filter's state
%       getKappa              - returns filter's concentration parameter
%       getDiffusionKappa     - returns diffusion concentration parameter
%       getObservationKappa   - returns observation concentration parameter
%
%       setX                  - sets filter's state
%       setKappa              - sets filter's concentration parameter
%       setDiffusionKappa     - sets diffusion concentration parameter
%       setObservationKappa   - sets observation concentration parameter
%
% ------------------------------------------------------------------------------

classdef VonMisesFisherFilter < matlab.mixin.Copyable
  
  properties (SetAccess = immutable)
    system_dim_;
  end
  
  properties (Access = private)
    x_;
    kappa_;
    diffusion_kappa_;
    observation_kappa_;
  end
  
  methods
    
    function obj = VonMisesFisherFilter(n)
      % m3t.estimation.vmf.VonMisesFisherFilter
      %
      %   obj = m3t.estimation.vmf.VonMisesFisherFilter(n)
      %     Creates n-dimensional von Mises-Fisher filter. Argument n must be a
      %     positive integer.
      assert(isinteger(n) && isscalar(n) && n > int32(1));
      obj.system_dim_ = n;
    end
    
    function x = getX(obj)
      % m3t.estimation.vmf.VonMisesFisherFilter.getX
      %   Returns filter's state.
      x = obj.x_;
    end
    
    function k = getKappa(obj)
      % m3t.estimation.vmf.VonMisesFisherFilter.getKappa
      %   Returns the concentration of the filter's state.
      k = obj.kappa_;
    end
    
    function k = getDiffusionKappa(obj)
      % m3t.estimation.vmf.VonMisesFisherFilter.getDiffusionKappa
      %   Returns the concentration parameter of the diffusion process.
      k = obj.diffusion_kappa_;
    end
    
    function k = getObservationKappa(obj)
      % m3t.estimation.vmf.VonMisesFisherFilter.getObservationKappa
      %   Returns concentration parameter of the measurement process.
      k = obj.observation_kappa_;
    end
    
    function predict(obj, dt)
      % m3t.estimation.vmf.VonMisesFisherFilter.predict
      %
      %   obj.PREDICT(dt) - Prediction step of the von Mises-Fisher filter.
      %                     Argument dt is the time since last update/predict
      %                     steps, it must be a positive double value.
      assert(isa(dt, 'double') && isscalar(dt) && dt > 0);
      
      k = obj.kappa_;
      kd = obj.diffusion_kappa_ / dt;
      dim = obj.system_dim_;
      
      temp = m3t.stat.directional.vmf.besselRatio(dim, k) * ...
        m3t.stat.directional.vmf.besselRatio(dim, kd);
      k = m3t.stat.directional.vmf.besselRatioInverse(dim, temp);
      
      obj.kappa_ = k;
    end
    
    function setX(obj,x)
      % m3t.estimation.vmf.VonMisesFisherFilter.setX
      %
      %   obj.SETX(x) - Sets the state of the von Mises-Fisher filter. Argument
      %                 x must be a column vector of double values such that
      %                 numel(x) == obj.dim_ + 1.
      assert(isa(x, 'double') && iscolumn(x) && numel(x) == obj.system_dim_ + 1);
      obj.x_ = x;
    end
    
    function setKappa(obj, k)
      % m3t.estimation.vmf.VonMisesFisherFilter.setKapppa
      %
      %   obj.SETKAPPA(k) - Sets the concentration parameter of the filter's
      %                     state. Argument k must be a positive double value.
      assert(isa(k, 'double') && isscalar(k) && k > 0);
      obj.kappa_ = k;
    end
    
    function setDiffusionKappa(obj, k)
      % m3t.estimation.vmf.VonMisesFisherFilter.setDiffusionKappa
      %
      %   obj.SETDIFFUSIONKAPPA(k) - Sets the concentration parameter of the
      %                              diffusion proces of the filter. Argument k
      %                              must be a positive double value.
      assert(isa(k, 'double') && isscalar(k)  && k > 0);
      obj.diffusion_kappa_ = k;
    end
    
    function setObservationKappa(obj, k)
      % m3t.estimation.vmf.VonMisesFisherFilter.setObservationKappa
      %
      %   obj.SETOBSERVATIONKAPPA(k) - Sets the concentration parameter of the
      %                                measurement process of the filter.
      %                                Argument k must be a positive double
      %                                value.
      assert(isa(k, 'double') && isscalar(k) && k > 0);
      obj.observation_kappa_ = k;
    end
    
    function update(obj, y)
      % m3t.estimation.vmf.VonMisesFisherFilter.update
      %
      %   obj.UPDATE(y) - Updates the state of the von Mises-Fisher filter with
      %                   detection y. Argument y must be an unit column vector
      %                   of double values of appropriate dimension.
      assert(isa(y, 'double') && iscolumn(y) && ...
        numel(y) == obj.dim_ + 1 && abs(norm(y) - 1) < 1e-3);
      
      x = obj.x_;
      k = obj.kappa_;
      ko = obj.observation_kappa_;
      
      x = ko * y + k * x;
      k = norm(x);
      x = x / k;
      
      obj.x_ = x;
      obj.kappa_ = k;
    end
    
    function weightedUpdate(obj, weights, detections)
      % m3t.estimation.vmf.VonMisesFisherFilter.weightedUpdate
      %
      %   obj.WEIGHTEDUPDATE(weights, detections) 
      
      num_d = numel(detections);
      dim = obj.system_dim_;
      x = obj.x_;
      k = obj.kappa_;
      ko = obj.observation_kappa_;
      
      if num_d > 0
        mu = (1 - sum(weights)) ...
          * m3t.stat.directional.vmf.besselRatio(dim, k) * x;
        
        for j = 1:num_d
          z = detections(j).getValue();
          mu_j = ko * z + k * x;
          k_j = norm(mu_j);
          mu_j = mu_j / k_j;
          mu = mu + weights(j) * ...
            m3t.stat.directional.vmf.besselRatio(dim, k_j) * mu_j;
        end
        
        temp = norm(mu);
        k = m3t.stat.directional.vmf.besselRatioInverse(dim, temp);
        mu = mu / temp;
        
        obj.x_ = mu;
        obj.kappa_ = k;
      end
    end
    
  end
  
end

