% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
%
%   m3t.estimation.kalman.DiscreteKalmanFilter
%
%     DKF - linear discrete Kalman filter class
%
%     A discrete linear Kalman filter algorithm for the systems that can be
%     modeled with following equations:
%       x(k) = F x(k-1) + G u(k-1) + w(k-1),
%       y(k) = H x(k) + v(k),
%     where k is the time index, x is the system state vector, u is control
%     input, w and v are independent zero-mean normal random variables, F
%     is a state transition matrix, G is the input matrix and H is the
%     measurement matrix.
%
%     DiscreteKalmanFilter is a subclass of m3t.estimation.kalman.KalmanFilter
%     class.
%
%     DiscreteKalmanFilter methods:
%       concatenate - concatenates multiple DKFs
%
%       predict   - discrete Kalman filter prediction step
%       update    - discrete Kalman filter update step
%
%       weightedUpdate  - probabilistic data association (PDA) update
%
%       getF - returns state transition matrix
%       getG - returns input matrix
%       getH - returns output matrix
%       getQ - returns process noise covariance matrix
%       getR - returns measurement noise covariance matrix
%
%       setF - sets state transition matrix F_
%       setG - sets input matrix G_
%       setH - sets output matrix H_
%       setQ - sets process noise covariance matrix Q_
%       setR - sets measurement noise covariance matrix R_
%
%     DiscreteKalmanFilter static methods:
%       constVelocity1D       - creates 1-dimensional constant velocity DKF
%       constVelocity2D       - creates 2-dimensional constant velocity DKF
%       constVelocity3D       - creates 3-dimensional constant velocity DKF
%       constVelocity         - creates n-dimensional constant velocity DKF
%       constAcceleration1D   - creates 1-dimensional constant acceleration DKF
%       constAcceleration2D   - creates 2-dimensional constant acceleration DKF
%       constAcceleration3D   - creates 3-dimensional constant acceleration DKF
%       constAcceleration     - creates n-dimensional constant acceleration DKF
%
%     See also m3t.estimation.kalman.KalmanFilter
%
% ------------------------------------------------------------------------------

classdef DiscreteKalmanFilter < m3t.estimation.kalman.KalmanFilter
  
  properties (Access = private)
    F_; % state transition matrix
    G_; % input matrix
    H_; % output matrix
    Q_; % process noise covariance
    R_; % measurement noise covariance
  end
  
  methods
    
    function obj = DiscreteKalmanFilter(nx, nu, ny)
      % m3t.estimation.kalman.DiscreteKalmanFilter
      %   Creates a discrete Kalman Filter object.
      %
      %   Syntax:
      %     nx = int32(3); % system dimension
      %     nu = int32(1); % num of inputs
      %     ny = int32(2); % num of outputs
      %     kf = m3t.estimation.kalman.DiscreteKalmanFilter(nx, nu, ny);
      %
      obj@m3t.estimation.kalman.KalmanFilter(nx, nu, ny);
    end
    
  end
  
  methods
    
    function new_kf = concatenate(obj, varargin)
      % m3t.estimation.kalman.DiscreteKalmanFilter.concatenate
      %   Concatenates multiple discrete Kalman filters into one.
      %   
      %   Syntax:
      %     kf = CONCATENATE(kf1, kf2, ...);
      %     kf = kf1.CONCATENATE(kf2, ...);
      
      dim_x = obj.system_dim_;
      dim_u = obj.input_dim_;
      dim_y = obj.output_dim_;
      
      F = obj.F_;
      G = obj.G_;
      H = obj.H_;
      Q = obj.Q_;
      R = obj.R_;
      
      for i = 1:numel(varargin)
        kf = varargin{i};
        assert(isa(kf,'m3t.estimation.kalman.DiscreteKalmanFilter') && ...
          isscalar(kf));
        
        dim_x = dim_x + kf.system_dim_;
        dim_u = dim_u + kf.input_dim_;
        dim_y = dim_y + kf.output_dim_;
        
        F = blkdiag(F, kf.getF());
        G = blkdiag(G, kf.getG());
        H = blkdiag(H, kf.getH());
        Q = blkdiag(Q, kf.getQ());
        R = blkdiag(R, kf.getR());
      end
      
      new_kf = m3t.estimation.kalman.DiscreteKalmanFilter(dim_x, dim_u, dim_y);
      new_kf.setF(F);
      new_kf.setG(G);
      new_kf.setH(H);
      new_kf.setQ(Q);
      new_kf.setR(R);
    end
    
    function F = getF(obj)
      % m3t.estimation.kalman.DiscreteKalmanFilter.getF
      %   Returns private property F_ (state transition matrix).
      F = obj.F_;
    end
    
    function G = getG(obj)
      % m3t.estimation.kalman.DiscreteKalmanFilter.getG
      %   Returns private property G_ (input matrix).
      G = obj.G_;
    end
    
    function H = getH(obj)
      % m3t.estimation.kalman.DiscreteKalmanFilter.getH
      %   Returns private property H_ (output matrix).
      H = obj.H_;
    end
    
    function Q = getQ(obj)
      % m3t.estimation.kalman.DiscreteKalmanFilter.getQ
      %   Returns private property Q_ (process noise covariance matrix).
      Q = obj.Q_;
    end
    
    function R = getR(obj)
      % m3t.estimation.kalman.DiscreteKalmanFilter.getR
      %   Returns private property R_ (measurement noise covariance matrix).
      R = obj.R_;
    end
    
    function predict(obj, ~, varargin)
      % m3t.estimation.kalman.DiscreteKalmanFilter.predict
      %
      %   obj.PREDICT(dt) - DKF prediction step for zero system input (if
      %                     obj.input_dim_ is nonzero. Argument dt is ignored
      %                     since this is a discrete Kalman filter.
      % 
      %   obj.PREDICT(dt,u); - DKF prediction step for input signal u. Argument
      %                        dt is ignored since this is a discrete Kalman
      %                        filter. Argument u must be a coulumn vector of
      %                        double values such that numel(u) equalt to
      %                        obj.input_dim_.
      u = zeros(obj.input_dim_,1);
      if nargin == 3
        u = varargin{1};
        assert(isa(u,'double') && iscolumn(u) && numel(u) == obj.input_dim_);
      elseif nargin > 2
        error(['Too many input arguments.', newline]);
      end
      
      x = obj.x_;
      P = obj.P_;
      
      F = obj.F_;
      G = obj.G_;
      H = obj.H_;
      Q = obj.Q_;
      R = obj.R_;
      
      x = F*x;
      if ~isempty(G)
        x = x + G*u;
      end
      P = F*P*F' + Q;
      P = (P + P')/2;
      
      y_hat = H*x;
      S = H*P*H' + R;
      
      obj.x_ = x;
      obj.P_ = P;
      obj.y_hat_ = y_hat;
      obj.S_ = S;
      
    end
    
    function setF(obj, F)
      % m3t.estimation.kalman.DiscreteKalmanFilter.setF
      %   Sets system transition matrix.
      %
      %   obj.SETF(F) - F must be an n�n matrix where n = obj.system_dim_.
      assert(isa(F,'double') && size(F,1) == obj.system_dim_ && ...
        size(F,2) == obj.system_dim_);
      obj.F_ = F;
    end
    
    function setG(obj, G)
      % m3t.estimation.kalman.DiscreteKalmanFilter.setG
      %   Sets system transition matrix.
      %
      %   obj.SETG(G) - G must be an n�m matrix where n = obj.system_dim_ and
      %                 m = obj.input_dim_.
      assert(isa(G,'double') && size(G,1) == obj.system_dim_ && ...
        size(G,2) == obj.input_dim_ );
      obj.G_ = G;
    end
    
    function setH(obj, H)
      % m3t.estimation.kalman.DiscreteKalmanFilter.setH
      %   Sets system transition matrix.
      %
      %   obj.SETH(H) - H must be an p�n matrix where n = obj.system_dim_ and
      %                 p = obj.output_dim_.
      assert(isa(H,'double') && size(H,1) == obj.output_dim_ && ...
        size(H,2) == obj.system_dim_);
      obj.H_ = H;
    end
    
    function setQ(obj, Q)
      % m3t.estimation.kalman.DiscreteKalmanFilter.setQ
      %   Sets system transition matrix.
      %
      %   obj.SETQ(Q) - Q must be an n�n positive semidefinite matrix where 
      %                 n = obj.system_dim_.
      assert(isa(Q,'double') && issymmetric(Q) && size(Q,1) == obj.system_dim_);
      e = eig(Q);
      assert(all(e >= 0));
      obj.Q_ = Q;
    end
    
    function setR(obj, R)
      % m3t.estimation.kalman.DiscreteKalmanFilter.setR
      %   Sets system transition matrix.
      %
      %   obj.SETR(R) - R must be an p�p positive semidefinite matrix where 
      %                 p = obj.output_dim_.
      assert(isa(R,'double') && issymmetric(R) && size(R,1) == obj.output_dim_);
      e = eig(R);
      assert(all(e >= 0));
      obj.R_ = R;
    end
    
    function update(obj, y)
      % m3t.estimation.kalman.DiscreteKalmanFilter.update
      %
      %   obj.UPDATE(y) - Updates the state of the filter with measurement y.
      %                   Argument y must be a column vector of double values
      %                   such that numel(y) == obj.output_dim_.
      
      assert(isa(y,'double') && iscolumn(y) && numel(y) == obj.output_dim_);
      
      x = obj.x_;
      P = obj.P_;
      S = obj.S_;
      y_hat = obj.y_hat_;
      
      H = obj.H_;
      R = obj.R_;
      
      K = P*H'/S;
      x = x + K*(y - y_hat);
      T = (eye(obj.system_dim_) - K*H);
      P = T*P*T' + K*R*K';
      P = (P + P')/2;
      
      y_hat = H*x;
      S = H*P*H' + R;
      
      obj.x_ = x;
      obj.P_ = P;
      obj.y_hat_ = y_hat;
      obj.S_ = S;
    end
    
    function weightedUpdate(obj, weights, detections)
      % m3t.estimation.kalman.DiscreteKalmanFilter.weightedUpdate
      %
      %   Soft assignment Kalman filter update used by probabilistic data
      %   association (PDA) tracking algorithms.
      %
      %   Syntax:
      %
      %     obj.WEIGHTEDUPDATE(weights, detections)
      %       Applys soft assignemnt Kalman filter update using weights and
      %       detections. Argument detections must be a matrix of double values
      %       where each column represents one detections, thus size(detections,
      %       size(detections, 1) must be equal to obj.output_dim_. Argument
      %       weights must be a vector of nonnegative double values representing
      %       association probabilities of detections, hence the number of
      %       elements of weights must be equal to the number of columns of
      %       detections.
      
      assert(isa(weights, 'double') &&...
        isvector(weights) && all(weights >= 0));
      assert(numel(weights) == numel(detections));
      
      num_d = numel(detections);
      dim = obj.output_dim_;
      
      if num_d > 0
        
        x = obj.x_;
        P = obj.P_;
        S = obj.S_;
        y_hat = obj.y_hat_;
        
        H = obj.H_;
        R = obj.R_;
        
        K = P*H'/S;
        
        y_sum = zeros(dim, 1);
        A = zeros(dim);
        sum_w = sum(weights);
        
        for j = 1:num_d
          y = detections(j).getValue();
          y_sum = y_sum + weights(j)*y;
          A = A + weights(j)*(y - y_hat)*(y - y_hat)';
        end
        mu = y_sum - sum_w*y_hat;
        A = A - mu*mu';
        
        x = x + K*(y_sum - sum_w * y_hat);
        T = (eye(obj.system_dim_) - K*H);
        P = P + sum_w *(T*P*T' + K*R*K' - P) + K*A*K';
        P = (P + P')/2;
        
        y_hat = H*x;
        S = H*P*H' + R;
        
        obj.x_ = x;
        obj.P_ = P;
        obj.y_hat_ = y_hat;
        obj.S_ = S;
      end
    end
    
  end
  
  % Some simple filters
  methods (Static)
    
    function obj = constVelocity1D(d_t, q, r)
      % m3t.estimation.kalman.DiscreteKalmanFilter.constVelocity1D
      %
      %   Creates a constant velocity filter with one degree of freedom.
      %   d_t is the sapling period, q is the process noise variance and r
      %   is the measurement noise variance.
      obj = m3t.estimation.kalman.DiscreteKalmanFilter.constVelocity(...
        d_t,q,r,uint8(1));
    end
    
    function obj = constVelocity2D(d_t, q, r)
      % m3t.estimation.kalman.DiscreteKalmanFilter.constVelocity2D
      %
      %   Creates a constant velocity filter with two degrees of freedom.
      %   d_t is the sapling period, q is the process noise variance and r
      %   is the measurement noise variance. Arguments q and r can be
      %   either scalars or arrays with 2 elements.
      obj = m3t.estimation.kalman.DiscreteKalmanFilter.constVelocity(...
        d_t,q,r,uint8(2));
    end
    
    function obj = constVelocity3D(d_t, q, r)
      % m3t.estimation.kalman.DiscreteKalmanFilter.constVelocity3D
      %
      %   Creates a constant velocity filter with three degrees of freedom.
      %   d_t is the sapling period, q is the process noise variance and r
      %   is the measurement noise variance. Arguments q and r can be
      %   either scalars or arrays with 3 elements.
      obj = m3t.estimation.kalman.DiscreteKalmanFilter.constVelocity(...
        d_t,q,r,uint8(3));
    end
    
    function obj = constVelocity(d_t, q, r, dof)
      % m3t.estimation.kalman.DiscreteKalmanFilter.constVelocity
      %
      %   Creates a constant velocity filter with dof degrees of freedom.
      %   d_t is the sapling period, q is the process noise variance and r
      %   is the measurement noise variance. Arguments q and r can be
      %   either scalars or arrays with dof elements.
      assert(isa(d_t, 'double') && isscalar(d_t) && d_t > 0);
      assert(isa(q, 'double') && ...
        (isscalar(q) || (isvector(q) && numel(q) == dof)) && all(q>0));
      assert(isa(r, 'double') && ...
        (isscalar(r) || (isvector(r) && numel(r) == dof)) && all(r>0));
      assert(isinteger(dof) && isscalar(dof) && dof > 0);
      
      dof = uint8(dof);
      
      F = [1, d_t; 0, 1];
      F = kron(eye(dof),F);
      
      H = [1 0];
      H = kron(eye(dof),H);
      
      Q = [d_t^3/3 d_t^2/2; d_t^2/2 d_t];
      if numel(q) == 1
        q = q*eye(dof);
      else
        q = diag(q);
      end
      Q = kron(q,Q);
      
      if numel(r) == 1
        R = r*eye(dof);
      else
        R = diag(r);
      end
      
      obj = m3t.estimation.kalman.DiscreteKalmanFilter(...
        int32(dof)*2, int32(0),int32(dof));
      obj.setF(F);
      obj.setH(H);
      obj.setQ(Q);
      obj.setR(R);
    end
    
    function obj = constAcceleration1D(d_t, q, r)
      % m3t.estimation.kalman.DiscreteKalmanFilter.constAcceleration1D
      % 
      %   Creates a constant acceleration filter with one degree of
      %   freedom. d_t is the sampliing period, q is the process noise
      %   variance and r is the measurement noise variance.
      obj = m3t.estimation.kalman.DiscreteKalmanFilter.constAcceleration(...
        d_t,q,r,uint8(1));
    end
    
    function obj = constAcceleration2D(d_t, q, r)
      % m3t.estimation.kalman.DiscreteKalmanFilter.constAcceleration2D
      %
      %   Creates a constant acceleration filter with two degrees of
      %   freedom. d_t is the sampliing period, q is the process noise
      %   variance and r is the measurement noise variance. Arguments
      %   q and r can be either scalars or arrays with 2 elements.
      obj = m3t.estimation.kalman.DiscreteKalmanFilter.constAcceleration(...
        d_t,q,r,uint8(2));
    end
    
    function obj = constAcceleration3D(d_t, q, r)
      % m3t.estimation.kalman.DiscreteKalmanFilter.constAcceleration3D
      %
      %   Creates a constant acceleration filter with three degrees of
      %   freedom. d_t is the sampliing period, q is the process noise
      %   variance and r is the measurement noise variance. Arguments
      %   q and r can be either scalars or arrays with 3 elements.
      obj = m3t.estimation.kalman.DiscreteKalmanFilter.constAcceleration(...
        d_t,q,r,uint8(3));
    end
    
    function obj = constAcceleration(d_t, q, r, dof)
      % m3t.estimation.kalman.DiscreteKalmanFilter.constAcceleration
      %
      %   Creates a constant acceleration filter with dof degrees of
      %   freedom. d_t is the sampliing period, q is the process noise
      %   variance and r is the measurement noise variance. Arguments
      %   q and r can be either scalars or arrays with dof elements.
      assert(isa(d_t, 'double') && isscalar(d_t) && d_t > 0);
      assert(isa(q, 'double') && ...
        (isscalar(q) || (isvector(q) && numel(q) == dof)) && all(q>0));
      assert(isa(r, 'double') && ...
        (isscalar(r) || (isvector(r) && numel(r) == dof)) && all(r>0));
      assert(isinteger(dof) && isscalar(dof) && dof > 0);
      
      dof = uint8(dof);
      
      F = [1, d_t, d_t^2/2; 0, 1, d_t; 0, 0, 1];
      F = kron(eye(dof),F);
      
      Q = [
        d_t^5/20  d_t^4/8   d_t^3/6;
        d_t^4/8   d_t^3/3   d_t^2/2;
        d_t^3/6   d_t^2/2   d_t    ];
      if numel(q) == 1
        q = q*eye(dof);
      else
        q = diag(q);
      end
      Q = kron(q,Q);
      
      H = [1 0 0];
      H = kron(eye(dof),H);
      
      if numel(r) == 1
        R = r*eye(dof);
      else
        R = diag(r);
      end
      
      obj = m3t.estimation.kalman.DiscreteKalmanFilter(...
        int32(dof)*3, int32(0),int32(dof));
      obj.setF(F);
      obj.setH(H);
      obj.setQ(Q);
      obj.setR(R);
    end
    
  end
  
end

