%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
%
%   m3t.estimation.kalman.ExtendedKalmanFilter
%

classdef (Abstract) ExtendedKalmanFilter < m3t.estimation.kalman.KalmanFilter
  
  %% Constructor
  methods
    
    function obj = ExtendedKalmanFilter(system_dim,input_dim,output_dim)
      %EXTENDEDKALMANFILTER Construct an instance of this class
      %   Detailed explanation goes here
      obj@m3t.estimation.kalman.KalmanFilter(system_dim,input_dim,output_dim);
    end
    
  end
  
  methods
    
    function y_hat = getYHat(obj,x)
      y_hat = obj.getH(x) * obj.state_.x_;
    end
    
    function predict(obj,dt,varargin)
      %PREDICT - Kalman filter state prediction
      %
      % PREDICT(kf,dt);
      % PREDICT(kf,dt,u);
      if nargin == 2
        u = zeros(kf.input_dim_,1);
      elseif nargin == 3
        u = varargin{1};
        assert(isa(u,'double') && iscolumn(u) && numel(u) == obj.input_dim_, ...
          ['Input u must be a column vector of corresponding dimension.' ...
          newline]);
      else
        error(['Too many input arguments.', newline]);
      end
      
      x = obj.x_;
      P = obj.P_;
      
      F = obj.getF(x,dt,u);
      L = obj.getL(x,dt,u);
      Q = obj.getQ(dt);
      
      x = obj.stateTransition(x,dt,u);
      P = F*P*F' + L*Q*L';
      P = (P + P')/2;
      
      y_hat = obj.measurementModel(x);
      
      R = obj.getR();
      H = obj.getH(x);
      M = obj.getM(x);
      S = H*P*H' + M*R*M';
      
      obj.x_ = x;
      obj.P_ = P;
      obj.y_hat_ = y_hat;
      obj.S_ = S;
    end
    
    function update(obj,y)
      x = obj.x_;
      P = obj.P_;
      S = obj.S_;
      y_hat = obj.y_hat_;
      
      R = obj.getR();
      H = obj.getH(x);
      M = obj.getM(x);
      
      K = P*H'/S;
      x = x + K*(y - y_hat);
      T = (eye(obj.system_dim_) - K*H);
      P = T*P*T' + K*M*R*M'*K';
      P = (P + P')/2;
      
      obj.x_ = x;
      obj.P_ = P;
    end
    
  end
  
  
  %% Abstract
  methods (Abstract)
    F = getF(obj,x,dt,u);
    L = getL(obj,x,dt,u);
    H = getH(obj,x);
    M = getM(obj,x);
    Q = getQ(obj,dt);
    R = getR(obj);
    y = measurementModel(obj,x);
    x = stateTransition(obj,x,dt,u);
  end
  
end

