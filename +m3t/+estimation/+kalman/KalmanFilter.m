% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
%
%   m3t.estimation.kalman.KalmanFilter
%
%     KalmanFilter is an abstract class that defines methods
%     that Kalman filters must implement.
%
%     KalmanFilter is a subclass of matlab.mixin.Copyable.
%
%     m3t.estimation.kalman.KalmanFilter properties:
%       input_dim_    - dimension of the system input
%       output_dim_   - dimension of the system output
%       system_dim_   - dimension of the system state
%
%       x_      - state
%       P_      - covariance
%       S_      - predicted measurement covariance
%       y_hat_  - predicted measurement
%
%     m3t.estimation.kalman.KalmanFilter methods:
%       predict   - (abstract) Kalman filter prediction
%       update    - (abstract) Kalman filter update
%
%       weightedUpdate  - (abstract) probabilistic data association (PDA) update
%
%       getX      - returns property x_
%       getP      - returns property P_
%       getS      - returns property S_
%       getYHat   - returns property y_hat_
%       setX      - sets property x_
%       setP      - sets property P_
%       setS      - sets property S_
%       setYHat   - sets property y_hat_
%
% ------------------------------------------------------------------------------

classdef KalmanFilter < matlab.mixin.Copyable
  
  properties (SetAccess = immutable)
    input_dim_;  % number of system control inputs
    output_dim_; % number of system output inputs
    system_dim_; % number of system states
  end
  
  properties (Access = protected)
    x_;
    P_;
    S_;
    y_hat_;
  end
  
  methods (Abstract)
    % m3t.estimation.kalman.KalmanFilter.predict
    %
    %   obj.PREDICT(dt) - DKF prediction step for zero system input (if
    %                     obj.input_dim_ is nonzero. Argument dt is ignored
    %                     since this is a discrete Kalman filter.
    %
    %   obj.PREDICT(dt,u); - DKF prediction step for input signal u. Argument
    %                        dt is ignored since this is a discrete Kalman
    %                        filter. Argument u must be a coulumn vector of
    %                        double values such that numel(u) equalt to
    %                        obj.input_dim_.
    predict(obj, dt, varargin);
    % m3t.estimation.kalman.KalmanFilter.update
    %
    %   obj.UPDATE(y) - Updates the state of the filter with measurement y.
    %                   Argument y must be a column vector of double values
    %                   such that numel(y) == obj.output_dim_.
    update(obj, y);
    % m3t.estimation.kalman.KalmanFilter.weighteUpdate
    %
    %   Soft assignment Kalman filter update used by probabilistic data
    %   association (PDA) tracking algorithms.
    %
    %   Syntax:
    %
    %     obj.WEIGHTEDUPDATE(weights, detections)
    %       Applys soft assignemnt Kalman filter update using weights and
    %       detections. Argument detections must be a matrix of double values
    %       where each column represents one detections, thus size(detections,
    %       size(detections, 1) must be equal to obj.output_dim_. Argument
    %       weights must be a vector of nonnegative double values representing
    %       association probabilities of detections, hence the number of
    %       elements of weights must be equal to the number of columns of
    %       detections.
    weightedUpdate(obj, weights, detections);
  end
  
  methods
    
    function obj = KalmanFilter(nx, nu, ny)
      % m3t.estimation.kalman.KalmanFilter
      %
      %   m3t.estimation.kalman.KalmanFilter(nx, nu, ny) - 
      %     Creates Kalman filter object. All arguments must be positive
      %     integers with only exception being that nu can be zero. Arguments
      %     are as follows:
      %         nx - dimension of a system (number of states)
      %         nu - dimension of a input (number of control inputs)
      %         ny - dimension of a output (number of measured variables)
      assert(isinteger(nx) && isscalar(nx) && nx > 0);
      assert(isinteger(nu) && isscalar(nu) && nu >= 0);
      assert(isinteger(ny) && isscalar(ny) && ny > 0);
      
      obj.system_dim_ = nx;
      obj.input_dim_ = nu;
      obj.output_dim_ = ny;
    end
    
    function x = getX(obj)
      % m3t.estimation.kalman.KalmanFilter.getX
      %   Returns system state vector x_.
      x = obj.x_;
    end
    
    function P = getP(obj)
      % m3t.estimation.kalman.KalmanFilter.getP
      %   Returns system state covariance matrix P_.
      P = obj.P_;
    end
    
    function S = getS(obj)
      % m3t.estimation.kalman.KalmanFilter.getS
      %   Returns covariance matrix S_ of predicted measurement. 
      S = obj.S_;
    end
    
    function y_hat = getYHat(obj)
      % m3t.estimation.kalman.KalmanFilter.getYHat
      %   Returns predicted measurement y_hat_;
      y_hat = obj.y_hat_;
    end
    
    function setX(obj, x)
      % m3t.estimation.kalman.KalmanFilter.setX
      %
      %   obj.SETX(x) - Sets system state (property x_). Argument x must be a
      %                 column vector of double values of appropriate dimension.
      assert(isa(x,'double') && iscolumn(x) && ...
        numel(x) == obj.system_dim_);
      obj.x_ = x;
    end
    
    function setP(obj, P)
      % m3t.estimation.kalman.KalmanFilter.setP
      %
      %   obj.SETP(P) - Sets the covariance matrix P_ of a state. Argument P
      %                 must be a symmetric positive definite matrix of
      %                 appropriate dimensions.
      assert(isa(P,'double') && issymmetric(P) && ...
        size(P,1) == obj.system_dim_);
      [~, p] = chol(P);
      assert(p == 0);
      obj.P_ = P;
    end
    
    function setS(obj, S)
      % m3t.estimation.kalman.KalmanFilter.setS
      %
      %   obj.SETS(S) - Sets the covariance matrix S_ of a predicted
      %                 measurement. Argument Pmust be a symmetric positive
      %                 definite matrix of appropriate dimensions.
      assert(isa(S,'double') && issymmetric(S) && ...
         ize(S,1) == obj.output_dim_);
      [~, p] = chol(S);
      assert(p == 0);
      obj.S_ = S;
    end
    
    function setYHat(obj, y)
      % m3t.estimation.kalman.KalmanFilter.setYHat
      %
      %   obj.SETYHAT(y) - Sets the predicted measurement of a system.
      %                    Argument y must be a column vector of double values
      %                    of appropriate dimension.
      assert(isa(y,'double') && iscolumn(y) && numel(y) == obj.output_dim_, ...
        ['Argument ymust be a column vector of double values '...
        'of dimension equal to obj.output_dim_.', newline]);
      obj.y_hat_ = y;
    end
    
  end
  
end

