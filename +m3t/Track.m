% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
%
%   m3t.Track
%
%   Class used in multi-target tracking classes to represent the
%   states of tracked objects
%
%   See also m3t.Tracker, m3t.Detection
%
% ------------------------------------------------------------------------------
  
classdef (Abstract) Track < matlab.mixin.Copyable & matlab.mixin.Heterogeneous

  properties (SetAccess = private)
    id_;        % track label (identificator)
    timestamp_; % timestamp of the track state
  end
  
  methods
    
    function obj = Track()
      obj.id_ = int32(-1);
    end
    
    function timestamp = getTimestamp(obj)
      timestamp = obj.timestamp_;
    end
    
    function setId(obj, id)
      assert(isa(id, 'int32') && isscalar(id), ...
        ['Argument id must be an int32 scalar.', newline]);
      obj.id_ = id;
    end
    
    function setTimestamp(obj, timestamp)
      obj.timestamp_ = timestamp;
    end
    
  end
  
  methods (Abstract)
    state = getState(obj);
  end
  
end

