% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
%
%   m3t.eval.OptimalSubPatternAssignment
%
%     Optimal SubPattern Assignment (OSPA) metric for evaluation of multi-target
%     trackers.
%
%     References:
%       [1] Schuhmacher, Dominic, Ba-Tuong Vo, and Ba-Ngu Vo. "A consistent
%           metricfor performance evaluation of multi-object filters." IEEE
%           transactionson signal processing 56.8 (2008): 3447-3457.
%           https://ieeexplore.ieee.org/abstract/document/4567674
%
%     m3t.eval.OptimalSubPatternAssignment is a subclass of
%     matlab.mixin.Copyable.
%
%     m3t.eval.OptimalSubPatternAssignment methods:
%       evaluate - evaluates metric
%
%       getC            - returns cutoff distance of the metric
%       getP            - returns order of the metric
%       getDistanceFcn  - returns distance function
%
%       setC            - sets cutoff distance of the metric
%       setP            - sets order of the metric
%       setDistanceFcn  - sets distance function
%
% ------------------------------------------------------------------------------

classdef OptimalSubPatternAssignment < matlab.mixin.Copyable
  
  properties (Access = private)
    c_;
    distance_fun_;
    p_;
  end
  
  methods
    
    function obj = OptimalSubPatternAssignment(c, p)
      % m3t.eval.OptimalSubPatternAssignment
      %
      %   obj = m3t.eval.OptimalSubPatternAssignment(c, p);
      %         - Creates an OSPA object with cutoff distance c and order p.
      %           Argument c must be a positive scalar double value and p
      %           must ba a scalar double value greater than or equal to zero.
      obj.setC(c);
      obj.setP(p);
    end
    
    function c = getC(obj)
      % m3t.eval.OptimalSubPatternAssignment.getC
      %   Returns cutoff distance of the metric.
      c = obj.c_;
    end
    
    function f = getDistanceFcn(obj)
      % m3t.eval.OptimalSubPatternAssignment.getDistanceFcn
      %   Returns the handle of the distance function.
      f = obj.distance_fun_;
    end
    
    function p = getP(obj)
      % m3t.eval.OptinalSubPatternAssignment.getP
      %   Returns the order of the metric.
      p = obj.p_;
    end
    
    function [err, err_pos, err_num] = evaluate(obj, gt, tracks)
      % m3t.eval.OptimalSubPatternAssignment.evaluate
      %
      %   err_tot = obj.EVALUATE(gt, tracks);
      %   [err_tot, err_pos, err_num] = obj.EVALUATE(gt, tracks)
      %
      %         Evaluates OSPA error for ground truth objects and tracks.
      %         Argument gt must be an array of ground truth objects and track
      %         is an array of tracked objects.
      %
      %         err_tot - Total OSPA error: err_tot = err_pos + err_num
      %         err_pos - Location OSPA error
      %         err_num - Error in the number of estimated tracks
      c = obj.c_;
      p = obj.p_;
      
      if isempty(gt) && isempty(tracks)
        err = 0;
        err_pos = 0;
        err_num = 0;
        return;
      elseif isempty(gt) || isempty(tracks)
        err = c;
        err_pos = 0;
        err_num = c;
        return;
      end
      
      num_gt = numel(gt);
      num_tr = numel(tracks);
      num    = max(num_gt, num_tr);
      
      % calculate distance matrix
      D = zeros(num_gt, num_tr);
      for i = 1:num_gt
        for j = 1:num_tr
          D(i,j) = obj.distance_fun_(gt(i), tracks(j));
        end
      end
      
      pairs = matchpairs(D, c);
      
      num_paired = size(pairs, 1);
      num_unpaired = max(num_gt, num_tr) - num_paired;
      
      err_pos = 0;
      for i = 1:num_paired
        err_pos = err_pos + D(pairs(i, 1), pairs(i, 2))^p;
      end
      
      err_num = c^p * num_unpaired;
      
      err = ((err_pos + err_num)/num)^(1 / p);
      err_pos = (err_pos / num)^(1 / p);
      err_num = (err_num / num)^(1 / p);
    end
    
    function setC(obj, c)
      % m3t.eval.OptimalSubPatternAssignment.setC
      %
      %   obj.SETC(c) - Sets cutoff distance of the metric. Argument c must be
      %                 a positive double scalar value.
      assert(isa(c, 'double') && isscalar(c) && c > 0);
      obj.c_ = c;
    end
    
    function setP(obj, p)
      % m3t.eval.OptimalSubPatternAssignment.setP
      %
      %   obj.SETP(p) - Sets the order of the metric. Argument p must be a
      %                 double scalar value greater than, or equal to, one.
      assert(isa(p, 'double') && isscalar(p) && p >= 1);
      obj.p_ = p;
    end
    
    function setDistanceFcn(obj, f)
      % m3t.eval.OptimalAssignment.setDistanceFcn
      %
      %   obj.SETDISTANCEFCN(f) - Sets the distance function handle of the OSPA
      %                           metric objects. Argument f must be a handle to
      %                           a function that takes 2 inputs and returns 1
      %                           output.
      assert(isa(f, 'function_handle') && ...
        nargin(f) == 2 && (nargout(f) == 1 || nargout(f) == -1));
      obj.distance_fun_ = f;
    end
    
  end
  
end

