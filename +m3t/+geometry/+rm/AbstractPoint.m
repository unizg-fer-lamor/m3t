% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
classdef (Abstract) AbstractPoint ...
    < matlab.mixin.Copyable & matlab.mixin.Heterogeneous
  
  methods (Abstract)
    d = dim(obj);
    x = euclideanValue(obj);
    c = getChart(obj);
    m = getManifold(obj);
    x = getValue(obj);
    r = isequal(obj, other);
  end

  methods (Static, Sealed, Access = protected)

    function default_obj = getDefaultScalarElement()
      default_obj = m3t.geometry.rm.Point();
    end

  end

end

