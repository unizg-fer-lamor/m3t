% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------

classdef Point < m3t.geometry.rm.Point
  
  properties (Access = private)
    value_;
  end
  
  methods (Access = ?m3t.geometry.rm.euclidean.Euclidean)
    
    function obj = Point(x, manifold)
      
      assert(isa(manifold, 'm3t.geometry.rm.euclidean.Euclidean'));
      
      obj = obj@m3t.geometry.rm.Point(manifold);
      obj.value_ = x;
      
    end
    
  end
  
  methods 
    
    function d = dim(obj)
      d = numel(obj.value_);
    end
    
    function x = getValue(obj)
      x = obj.value_;
    end
    
    function is_eq = isequal(obj, other)
      
      if ~isa(other, 'm3t.geometry.rm.euclidean.Point')
        is_eq = false;
      elseif obj.dim() ~= other.dim()
        is_eq = false;
      else
        is_eq = norm(obj.getValue() - other.getValue()) < 1e-4;
      end
      
    end
    
  end
  
end

