% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------

classdef Tangent < m3t.geometry.rm.Tangent
  
  properties (Access = private)
    value_;
  end
  
  methods (Access = ?m3t.geometry.rm.euclidean.Euclidean)
    
    function obj = Tangent(value, point)
      
      assert(isa(value, 'double') && iscolumn(value));
      assert(isa(point, 'm3t.geometry.rm.euclidean.Point'));
      
      obj = obj@m3t.geometry.rm.Tangent(point);
      obj.value_ = value;
      
    end
    
  end
  
  methods
    
    function d = dim(obj)
      d = numel(obj.value_);
    end
    
    function val = getValue(obj)
      val = obj.value_;
    end
    
    function v = minus(obj, obj2)
      v = obj + (-obj2);
    end
    
    function v = mtimes(obj, obj2)
      if isa(obj, 'm3t.geometry.rm.euclidean.Tangent') && ...
          isa(obj2, 'double') && isscalar(obj2)
        
        v = m3t.geometry.rm.euclidean.Tangent(....
          obj2 * obj.getValue(), obj.getPoint());
        
      elseif isa(obj2, 'm3t.geometry.rm.euclidean.Tangent') && ...
          isa(obj, 'double') && ...
          (isscalar(obj) || all(size(obj) == obj2.dim()) || ...
          isrow(obj) && numel(obj) == obj2.dim())
        
        v = m3t.geometry.rm.euclidean.Tangent(...
          obj * obj2.getValue(), obj2.getPoint());
        
      else
        error(['Invalid argument types.', newline]);
      end
    end
    
    function v = plus(obj, obj2)
      assert(isa(obj2, 'm3t.geometry.rm.euclidean.Tangent') && ...
        obj.getPoint().isequal(obj2.getPoint()));
      
      value = obj.getValue() + obj2.getValue();
      v = m3t.geometry.rm.euclidean.Tangent(value, obj.getPoint);
    end
    
    function v = uminus(obj)
      v = m3t.geometry.rm.euclidean.Tangent(-obj.getValue(), obj.getPoint());
    end
    
    function v = mrdivide(obj, obj2)
      if isa(obj, 'm3t.geometry.rm.euclidean.Tangent') && ...
          isa(obj2, 'double') && isscalar(obj2)
        
        v = m3t.geometry.rm.euclidean.Tangent(...
          obj.getValue() / obj2, obj.getPoint());
        
      else
        error(['Invalid argument type.', newline]);
      end
    end
    
  end
  
end

