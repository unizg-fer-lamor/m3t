% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------

classdef Euclidean < m3t.geometry.rm.Manifold
  
  properties (Access = private)
    dim_;
  end
  
  methods
    
    function obj = Euclidean(d)
      % m3t.geometry.rm.euclidean.Euclidean
      %
      %   obj = m3t.geometry.rm.euclidean.Euclidean(d)
      %     Creates d-dimensional Euclidean space endowed with Riemannian
      %     metric.
      
      assert(isinteger(d) && isscalar(d) && d > 0);
      
      obj.dim_ = d;
    end
    
    function cov = createCovariance(obj, x, P)
      
      if ~isa(x, 'm3t.geometry.rm.euclidean.Point')
        assert(isa(x, 'double') && iscolumn(x) && numel(x) == obj.dim_);
        x = obj.createPoint(x);
      end
      
      [~, p] = chol(P);
      assert(p == 0 && all(size(P) == obj.dim_));
      
      cov = m3t.geometry.rm.euclidean.Covariance(x, P);
      
    end
    
    function p = createPoint(obj, x)
      assert(isa(x, 'double') && iscolumn(x) && ...
        numel(x) == obj.dim_);
      p = m3t.geometry.rm.euclidean.Point(x, obj);
    end
    
    function v = createTangent(obj, x, u)
      
      if isa(x, 'double') && isa(u, 'double')
        assert(iscolumn(x) && numel(x) == obj.dim_);
        assert(iscolumn(u) && numel(u) == obj.dim_);
        
        p = obj.createPoint(x);
        v = m3t.geometry.rm.euclidean.Tangent(u, p);
      elseif isa(x, 'm3t.geometry.rm.euclidean.Point') && isa(u, 'double')
        assert(obj.isequal(x.getManifold()) && ...
          iscolumn(u) && numel(u) == obj.dim_);
        
        v = m3t.geometry.rm.euclidean.Tangent(u, x);
      else
        error(['Invalid arguments.', newline]);
      end
      
    end
    
    function d = dim(obj)
      d = obj.dim_;
    end
    
    function d = distance(obj, x, y)
      
      assert(isa(x, 'm3t.geometry.rm.euclidean.Point') && ...
        obj.isequal(x.getManifold));
      
      assert(isa(y, 'm3t.geometry.rm.euclidean.Point') && ...
        obj.isequal(y.getManifold));
      
      t = x.getValue() - y.getValue();
      d = sqrt(t' * t);
    end
    
    function d = dot(obj, u, v)
      
      assert(isa(u, 'm3t.geometry.rm.euclidean.Tangent') && ...
        obj.isequal(u.getManifold()));
      
      assert(isa(v, 'm3t.geometry.rm.euclidean.Tangent') && ...
        obj.isequal(v.getManifold()));
      
      d = u.getValue()' * v.getValue();
      
    end
    
    function d = euclideanDim(obj)
      d = obj.dim_;
    end
    
    function y = exp(obj, v)
      
      assert(isa(v, 'm3t.geometry.rm.euclidean.Tangent') && ...
        obj.isequal(v.getManifold()));
      
      x = v.getPoint().getValue();
      
      y = obj.createPoint(x + v.getValue());
      
    end
    
    function b = isequal(obj, other)
      assert(isa(other, 'm3t.geometry.rm.euclidean.Euclidean'));
      b = obj.dim_ == other.dim();
    end
    
    function v = log(obj, x, y)
      
      assert(isa(x, 'm3t.geometry.rm.euclidean.Point') && x.dim() == obj.dim_);
      assert(isa(y, 'm3t.geometry.rm.euclidean.Point') && y.dim() == obj.dim_);
      
      v_value = y.getValue() - x.getValue();
      v = obj.createTangent(x.getValue(), v_value);
      
    end
    
    function mu = mean(obj, p_array, varargin)
      
      assert(isa(p_array, 'm3t.geometry.rm.euclidean.Point'));
      num_p = numel(p_array);
      
      if nargin == 2
        weights = ones(1, num_p);
      elseif nargin == 3
        
        if isa(varargin{1}, 'm3t.geometry.rm.euclidean.Point')
          % We are not using optimisation here, so initial guess is ignored
          weights = ones(1, num_p);
        elseif  isa(varargin{1}, 'double')
          weights = varargin{1};
          assert(isvector(w) && numel(w) == num_p && all(w >= 0));
        else
          error(['Invalid arguments.', newline]);
        end
        
      elseif nargin == 4
        weights = varargin{1};
        % Ignore last arg since we don't need initial guess
      else
        error(['Illegal number of arguments.', newline]);
      end
      
      weights = weights ./ sum(weights);
      
      mu_value = zeros(obj.dim_, 1);
      for i = 1:num_p
        mu_value = mu_value + p_array(i).getValue() * weights(i);
      end
      
      mu = obj.createPoint(mu_value);
      
    end
    
    function g = metricTensor(obj, ~)
      g = eye(obj.dim_);
    end
    
    function v = parallelTransport(obj, u, varargin)
      assert(isa(u, 'm3t.geometry.rm.euclidean.Tangent') && ...
        obj.isequal(u.getManifold()));
      v = u.copy();
    end
    
    function x = pointToEuclidean(obj, p)
      assert(isa(p, 'm3t.geometry.rm.euclidean.Point') && ...
        obj.isequal(p.getManifold()));
      x = p.getValue();
    end
    
    function mu = riemannCurvature(obj, u, v, w)
      assert(isa(u, 'm3t.geometry.rm.euclidean.Tangent') && ...
        obj.isequal(u.getManifold()));
      assert(isa(v, 'm3t.geometry.rm.euclidean.Tangent') && ...
        obj.isequal(v.getManifold()));
      assert(isa(w, 'm3t.geometry.rm.euclidean.Tangent') && ...
        obj.isequal(w.getManifold()));
      
      mu = obj.createTangent(u.getPoint(), zeros(obj.dim_, 1));
    end
    
    function v = tangentToEuclidean(obj, u)
      assert(isa(u, 'm3t.geometry.rm.euclidean.Tangent') && ...
        obj.isequal(u.getManifold()));
      v = u.getValue();
    end
    
  end
  
end

