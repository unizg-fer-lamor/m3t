% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
%
%   m3t.geometry.rm.Tangent - Riemannian manifold tangent vector class
%
%     Tangent is a subclass of the matlab.mixin.Copyable and
%     matlab.mixin.Heterogeneous classes.
%
%     Tangent methods (abstract):
%       dim       - returns dimension of the tangent space
%       getValue  - returns the value of the tangent
%       
%       mtimes    - scalar or matrix multiplication of a tangent vector
%       uminus    - unary minus
%       mrdivide  - scalar division of a tangent vector
%
%     See also m3t.geometry.rm.Manifold, m3t.geometry.rm.Point
%
% ------------------------------------------------------------------------------

classdef Tangent < matlab.mixin.Copyable & matlab.mixin.Heterogeneous
  
  properties (SetAccess = private)
    chart_;
    manifold_;
    value_;
  end
  
  methods
    
    function obj = Tangent(manifold, chart, value)
      if nargin ~= 0
        assert(isa(manifold, 'm3t.geometry.rm.Manifold') && isscalar(manifold));
        assert(isa(chart, 'int32'));
        assert(isa(value, 'double') ...
          && size(value, 1) == manifold.dim());

        n = size(value, 2);
        obj(n) = obj;

        for i = 1:n
          obj(i).manifold_ = manifold;
          obj(i).chart_ = chart;
          obj(i).value_ = value(:,i);
        end
      end
    end
    
    function d = dim(obj)
      d = obj.manifold_.dim();
    end

    function v = euclideanValue(obj, x)
      v = obj.manifold_.tangentToEuclidean(x, obj);
    end

    function c = getChart(obj)
      c = obj.chart_;
    end

    function m = getManifold(obj)
      m = obj.manifold_;
    end

    function v = getValue(obj)
      v = obj.value_;
    end

    function v = minus(obj, obj2)
      v = obj + (-obj2);
    end
    
    function v = mtimes(obj, obj2)

      if isa(obj, 'm3t.geometry.rm.Tangent') ...
          && isa(obj2, 'double') && isscalar(obj2)

        v = m3t.geometry.rm.Tangent(...
          obj.manifold_, obj.chart_, obj2 * obj.value_);

      elseif isa(obj2, 'm3t.geometry.rm.Tangent') ...
          && isa(obj, 'double')

        if isscalar(obj) || all(size(obj) == numel(obj2.value_))
          v = m3t.geometry.rm.Tangent(...
            obj2.manifold_, obj2.chart_, obj * obj2.value_);
        else
          error(['Invalid argument types.',newline]);
        end
      else
        error(['Invalid argument types.',newline]);
      end
    end

    function v = plus(obj, obj2)
      if isa(obj, 'm3t.geometry.rm.Tangent') ...
          && isa(obj2, 'm3t.geometry.rm.Tangent')

        assert(obj.manifold_.isequal(obj2.manifold_));
        assert(obj.chart_ == obj2.chart_);
        val = obj.value_ + obj2.value_;

        v = m3t.geometry.rm.Tangent(obj.manifold_, obj.chart_, val);
      end
    end

    function v = uminus(obj)
      v = m3t.geometry.rm.Tangent(obj.manifold_, obj.chart_, -obj.value_);  
    end

    function v = mrdivide(obj, obj2)
      if isa(obj, 'm3t.geometry.rm.Tangent') && isa(obj2, 'double') ...
          && isscalar(obj2)

        v = m3t.geometry.rm.Tangent(...
          obj.manifold_, obj.chart_, obj.value_ / obj2);

      else
        error(['Invalid argument type.', newline]);
      end
    end

  end

  methods (Static)

    function v = zeros(manifold, chart, n)
      assert(isa(manifold, 'm3t.geometry.rm.Manifold'));
      assert(isa(chart, 'int32') && isscalar(chart) && chart > 0);
      assert(isscalar(n) && n > 0);

      values = zeros(manifold.dim(), n);
      v = m3t.geometry.rm.Tangent(manifold, chart, values);
    end

  end
  
end

