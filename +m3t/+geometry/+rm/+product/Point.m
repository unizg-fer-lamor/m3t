% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------

classdef Point < m3t.geometry.rm.Point
  
  properties (Access = private)
    point_list_;
  end
  
  methods (Access = ?m3t.geometry.rm.product.ProductSpace)
    
    function obj = Point(point_list, manifold)
      obj@m3t.geometry.rm.Point(manifold);
      obj.point_list_ = point_list(:);
    end
    
  end
  
  methods
    
    function d = dim(obj)
      d = obj.getManifold().dim();
    end
    
    function x = getValue(obj)
      
      num_x = obj.getManifold().dim();
      current_index = 1;
      
      x = zeros(num_x, 1);
      
      for i = 1:numel(obj.point_list_)
        num_current = obj.point_list_(i).dim();
        temp = current_index + num_current;
        current_value = obj.point_list_(i).getValue();
        x(current_index:temp, 1) = current_value;
        current_index = temp;
      end
      
    end
    
    function is_eq = isequal(obj, other)
      
      if obj.getManifold().isequal(other.getManifold())
        
        is_eq = true;
        other_point_list = other.getElements();
        
        for i = 1:numel(obj.point_list_)
          if ~obj.point_list_(i).isequal(other_point_list(i))
            is_eq = false;
            return;
          end
        end
        
      else
        is_eq = false;
      end
    end
    
    function points = getElements(obj)
      points = obj.point_list_;
    end
    
  end
  
end

