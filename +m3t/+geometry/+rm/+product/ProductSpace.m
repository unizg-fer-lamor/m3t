% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------

classdef ProductSpace < m3t.geometry.rm.Manifold
  
  properties (Access = private)
    manifold_list_;
  end
  
  methods
    
    function obj = ProductSpace(varargin)
      
      if nargin == 0
        error(['Invalid number of arguments.', newline]);
      elseif nargin == 1
        
        assert(isa(varargin{1}, 'm3t.geometry.rm.Manifold') && ...
          isvector(varargin{1}));
        obj.manifold_list_ = varargin{1}(:);
      else
        
        for i = 1:nargin
          assert(isa(varargin{i}, 'm3t.geometry.rm.Manifold') && ...
            numel(varargin{i}) == 1);
          obj.manifold_list_(end + 1, 1) = varargin{i};
        end
        
      end
      
    end
    
    function cov = createCovariance(obj, x, P)
      
    end
    
    function p = createPoint(obj, x)
      
    end
    
    function v = createTangent(obj, x, u)
      
    end
    
    function d = dim(obj)
      
      d = int32(0);
      for manifold = obj.manifold_list_
        d = d + manifold.dim();
      end
      
    end
    
    function d = distance(obj, x, y)
      
      assert(isa(x, 'm3t.geometry.rm.product.Point') && ...
        obj.isequal(x.getManifold()));
      assert(isa(y, 'm3t.geometry.rm.product.Point') && ...
        obj.isequal(y.getManifold()));
      
      d = 0;
      x_list = x.getElements();
      y_list = y.getElements();
      
      for i = 1:numel(obj.manifold_list_)
        temp = obj.manifold_list_(i).distance(x_list(i), y_list(i));
        d = d + temp * temp;
      end
      
      d = sqrt(d);
      
    end
    
    function d = dot(obj, u, v)
      
      assert(isa(u, 'm3t.geometry.rm.product.Tangent') && ...
        obj.isequal(u.getManifold()));
      assert(isa(v, 'm3t.geometry.rm.product.Tangent') && ...
        obj.isequal(v.getManifold()));
      assert(u.getPoint().isequal(v.getPoint()));
      
      d = 0;
      u_list = u.getElements();
      v_list = v.getElements();
      for i = 1:numel(obj.manifold_list_)
        d = d + obj.manifold_list_(i).dot(u_list(i), v_list(i));
      end
      
    end
    
    function d = euclideanDim(obj)
      
      d = int32(0);
      for manifold = obj.manifold_list_
        d = d + manifolld.euclideanDim();
      end
      
    end
    
    function y = exp(obj, v)
      
      assert(isa(v, 'm3t.geometry.rm.product.Tangent') && ...
        obj.isequal(v.getManifold()));
      
      v_list = v.getElements();
      y_list = v.getPoint().getElements();
      
      for i = 1:numel(obj.manifold_list_)
        y_list(i) = obj.manifold_list_(i).exp(v_list(i));
      end
      
      y = obj.createPoint(y_list);
      
    end
    
    function manifolds = getElements(obj)
      manifolds = obj.manifold_list_.copy();
    end
    
    function b = isequal(obj, other)
      
      if ~isa(other, 'm3t.geometry.rm.product.ProductSpace')
        b = false;
        return;
      else
        
        other_manifold_list = other.getElements();
        
        if numel(other_manifold_list) ~= numel(obj.manifold_list_)
          b = false;
          return;
        else
          
          b = true;
          
          for i = 1:numel(other_manifold_list)
            if ~obj.manifold_list_(i).isequal(other_manifold_list(i))
              b = false;
              return;
            end
          end
          
        end
        
      end
      
    end
    
    function v = log(obj, x, y)
      
      assert(isa(x, 'm3t.geometry.rm.product.Point') && ...
        obj.isequal(x.getManifold()));
      
      assert(isa(y, 'm3t.geometry.rm.product.Point') && ...
        obj.isequal(y.getManifold()));
      
      x_list = x.getElements();
      y_list = y.getElements();
      
      v_list = m3t.geometry.rm.product.Tangent.empty();
      
      for i = 1:numel(obj.manifold_list_)
        v_list(i, 1) = obj.manifold_list_(i).log(x_list(i), y_list(i));
      end
      
      v = obj.createTangent(y, v_list);
      
    end
    
    function mu = mean(obj, p_array, varargin)
      
      assert(isa(p_array, 'm3t.geometry.rm.product.Point') && ...
        isvector(p_array) && obj.isequal(p_array(1).getManifold()));
      
      num_p = numel(p_array);
      
    end
    
    function g = metricTensor(obj, x)
      assert(isa(x, 'm3t.geometry.rm.product.Point') && ...
        obj.isequal(x.getManifold()));
      
      num = numel(obj.manifold_list_);
      metric_tensors = cell(num, 1);
      point_list = x.getElements();
      
      for i = 1:num
        metric_tensors{i, 1} = ...
          obj.manifold_list_(i).metricTensor(point_list(i));
      end
      
      g = blkdiag(metric_tensors{:});
      
    end
    
    function v = parallelTransport(obj, u, varargin)
      
      assert(isa(u, 'm3t.geometry.rm.product.Tangent') && ...
        obj.isequal(u.getManifold()));
      
      if numel(u) > 1
        for i = 2:numel(u)
          assert(u(1).getPoint().isequal(u(i).getPoint()));
        end
      end
      
      x = u(1).getPoint();
      
      if nargin == 3
        
        y = varargin{1};
        
        if isa(y, 'm3t.geometry.rm.product.Tangent')
          
        else
        
        end
          
      elseif nargin > 3
        
      else
        error(['Illegal number of arguments.',newline]);
      end
      
    end
    
    function x = pointToEuclidean(obj, p)
      assert(isa(p, 'm3t.geometry.rm.product.Point') && ...
        isscalar(p) && ...
        obj.isequal(p.getManifold()));
      
      p_list = p.getElements();
      d = obj.euclideanDim();
      
      x = zeros(d, 1);
      
      next_index = 1;
      for i = 1:numel(p_list)
        current_index = next_index;
        manifold = p_list(i).getManifold();
        next_index = current_index + manifold.euclideanDim();
        x(current_index:next_index - 1, 1) = ...
          manifold.pointToEuclidean(p_list(i));
      end
      
    end
    
    function mu = riemannCurvature(obj, u, v, w)
      assert(isa(u, 'm3t.geometry.rm.product.Tangent') && ...
        isscalar(u) && ...
        obj.isequal(u.getManifold()));
      assert(isa(v, 'm3t.geometry.rm.product.Tangent') && ...
        isscalar(v) && ...
        obj.isequal(v.getManifold()));
      assert(isa(w, 'm3t.geometry.rm.product.Tangent') && ...
        isscalar(w) && ...
        obj.isequal(w.getManifold()));
      
      x = u.getPoint();
      
      assert(x.isequal(v.getPoint()) && x.isequal(w.getPoint()));
      
      u_list = u.getElements();
      v_list = v.getElements();
      w_list = w.getElements();
      mu_list = u.copy();
      
      for i = 1:obj.manifold_list_
        mu_list(i) = obj.manifold_list_(i).riemannCurvature(...
          u_list(i), v_list(i), w_list(i));
      end
      
      mu = obj.createTangent(x, mu_list);
      
    end
    
    function x = tangentToEuclidean(obj, v)
      assert(isa(p, 'm3t.geoemtry.rm.product.Tangent') && ...
        isscalar(p) && ...
        obj.isequal(v.getManifold()));
      
      d = obj.euclideanDim();
      v_list = v.getElements();
      
      x = zeros(d, 1);
      
      next_index = 1;
      
      for i = 1:numel(v_list)
        current_index = next_index;
        manifold = v_list(i).getManifold();
        next_index = current_index + manifold.euclideanDim();
        x(current_index:next_index - 1, 1) = ...
          manifold.tangentToEuclidean(v_list(i));
      end
      
    end
    
  end
  
end

