% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------

classdef Tangent < m3t.geometry.rm.Tangent
  
  properties (Access = private)
    tangent_list_;
  end
  
  methods (Access = ?m3t.geometry.rm.product.ProductSpace)
    
    function obj = Tangent(tangent_list, point)
      
      obj@m3t.geometry.rm.Tangent(point)
      obj.tangent_list_ = tangent_list(:);
      
    end
    
  end
  
  methods
    
    function d = dim(obj)
      d = obj.getManifold().dim();
    end
    
    function v = getValue(obj)
      
      num_v = obj.getManifold().dim();
      current_index = 1;
      
      v = zeros(num_v, 1);
      
      for i = 1:numel(obj.tangent_list_)
        num_current = obj.tangent_list_(i).dim();
        temp = current_index + num_current;
        current_value = obj.tangent_list_(i).getValue();
        v(current_index:temp, 1) = current_value;
        current_index = temp;
      end
      
    end
    
    function is_eq = isequal(obj, other)
      
      if obj.getManifold().isequal(other.getManifold())
        
        is_eq = true;
        other_tangent_list = other.getElements();
        
        for i = 1:numel(obj.tangent_list_)
          if ~obj.tangent_list_(i).isequal(other_tangent_list(i))
            is_eq = false;
            return;
          end
        end
        
      else
        is_eq = false;
      end
      
    end
    
    function tangents = getElements(obj)
      tangents = obj.tangent_list_;
    end
    
    function v = minus(obj, obj2)
      v = obj + (-obj2);
    end
    
    function v = mtimes(obj, obj2)
      if isa(obj, 'm3t.geometry.rm.product.Tangent') && ...
          isa(obj2, 'double') && isscalar(obj2)
        
        v = obj.getManifold().createTangent(...
          obj.getPoint(), obj2 * obj.getValue());
        
      elseif isa(obj2, 'm3t.geometry.rm.product.Tangent') && ...
          isa(obj, 'double') && ...
          (isscalar(obj) || all(size(obj) == obj2.dim()))
        
        v = obj.getManifold().createTangent(...
          obj2.getPoint(), obj * obj2.getValue());
        
      else
        error(['Invalid argument types.', newline]);
      end
    end
    
    function v = plus(obj, obj2)
      
      assert(isa(obj2, 'm3t.geometry.rm.product.Tangent') && ...
        obj.getPoint().isequal(obj2.getPoint()));
      
      tangent_list = obj.point_list_;
      other_tangent_list = obj2.getElements();
      
      for i = 1:numel(this_tangent_list)
        tangent_list(i) = tangent_list(i) + other_tangent_list(i);
      end
      
      v = obj.getManifold().createTangent(obj.getPoint(), tangent_list);
      
    end
    
    function v = uminus(obj)
      
      tangent_list = obj.tangent_list_;
      for i = 1:numel(tangent_list)
        tangent_list(i) = - tangent_list(i);
      end
      
      v = obj.getManifold().createTangent(obj.getPoint(), tangent_list);
      
    end
    
    function v = mrdivide(obj, obj2)
      
      if isa(obj, 'm3t.geometry.rm.product.Tangent') && ...
          isa(obj2, 'double') && isscalar(obj2)
        
        tangent_list = obj.tangent_list_;
        for i = 1:numel(tangent_list)
          tangent_list(i) = tangent_list(i) / obj2;
        end
        
        v = obj.getManifold().createTangent(...
          obj.getPoint(), tangent_list);
        
      else
        error(['Invalid argument type.', newline]);
      end
      
    end
    
  end
  
end

