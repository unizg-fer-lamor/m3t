% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
%
%   m3t.geometry.rm.Point - Riemannian manifold point class
%
%     Point is a subclass of matlab.mixin.Copyable and
%     matlab.mixin.Heterogeneous.
%
%     Point methods (abstract):
%         dim       - returns the dimension of a point
%         getValue  - returns the point value
%
%     See also m3t.geometry.rm.Manifold, m3t.geometry.rm.Tangent.
%
% ------------------------------------------------------------------------------

classdef Point < m3t.geometry.rm.AbstractPoint
  
  properties (SetAccess = private)
    chart_;
    manifold_;
    value_;
  end
  
  methods
    
    function obj = Point(manifold, chart, value)
      if nargin > 0
        assert(isa(manifold, 'm3t.geometry.rm.Manifold') && isscalar(manifold));
        assert(isa(value, 'double') ...
          && size(value, 1) == manifold.dim());

        n = size(value, 2);
        obj(n) = obj;
        for i = 1:n
          obj(i).manifold_ = manifold;
          obj(i).chart_ = chart;
          obj(i).value_ = value(:, i);
        end
      end
    end
    
    function d = dim(obj)
      d = obj.manifold_.dim();
    end

    function x = euclideanValue(obj)
      x = obj.manifold_.pointToEuclidean(obj);
    end

    function c = getChart(obj)
      c = obj.chart_;
    end

    function manifold = getManifold(obj)
      manifold = obj.manifold_;
    end
    
    function x = getValue(obj)
      x = obj.value_;
    end

    function r = isequal(obj, other)
      if obj.manifold_.isequal(other.getManifold())
        r = obj.manifold_.distance(obj, other);
      else
        r = false;
      end
    end
    
  end
  
end

