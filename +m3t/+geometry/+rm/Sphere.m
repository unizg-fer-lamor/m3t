% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
%
%   m3t.geometry.rm.sphere.Sphere
%
%     Sphere is a subclass of the m3t.geometry.rm.Manifold class.
%
%     Sphere methods:
%       basis               - returns orthonormal basis of tangent space
%       createCovariance    - creates Riemannian manifold covarianve object
%       createPoint         - creates point on the Sphere
%       createTangent       - creates tangent vector on the Sphere
%       dim                 - returns dimension of a Sphere
%       distance            - returns great circle distance between points
%       dot                 - returns scallar product of tangent vectors
%       euclideanDim        - returns dimension of ambient Euclidean space
%       exp                 - applies exponential mapping
%       isequal             - compares two manifolds
%       karcherMean         - calculates Kärcher mean of points if possible
%       log                 - applies logarithmic mapping
%       mean                - calculates mean of points on the Sphere
%       metricTensor        - returns metric tensor in local coordinates
%       norm                - returns norm of a tangent vector
%       parallelTransport   - parallel transport of tangent vectors
%       pointToEuclidean    - converts point to ambient Euclidean representation
%       riemannCurvature    - evaluates Riemannian curvature
%       tangentToEuclidean  - converts tangent vector to ambient Euclidean
%                             representation
%
%       getKarcherMeanSettings  - gets karcher_mean_optim_settings_ property
%       setKarcherMeanSettings  - sets karcher_mean_optim_settings_ property
%
%       getRadius   - returns the radius of the Sphere
%
%     See also m3t.geometry.rm.Manifold, m3t.geometry.rm.sphere.Point,
%     m3t.geometry.rm.sphere.Tangent
%
% ------------------------------------------------------------------------------

classdef Sphere < m3t.geometry.rm.Manifold
  
  properties (SetAccess = immutable)
    dim_;
    radius_;
  end
  
  properties (Constant, Access = private)
    chart_ambient_  = int32(0); % ambient euclidean space
    chart_north_    = int32(1); % stereographic projection from north pole
    chart_south_    = int32(2); % stereographic projection from south pole
    point_class_str = 'm3t.geometry.rm.Point';
    tangent_class_str = 'm3t.geometry.rm.Tangent';
  end
  
  %% Constructor
  methods
    
    function obj = Sphere(d, r)
      % m3t.geometry.rm.sphere.Sphere
      %
      %   obj = m3t.geometry.rm.sphere.Sphere(d, r)
      %       Creates d-dimensional sphere of radius r. Argument d must be a
      %       positive scalar of integer type. Argument r must be a positive
      %       scalar of double type.
      assert(isinteger(d) && isscalar(d) && d > 0);
      assert(isa(r, 'double') && isscalar(r) && r > 0);
      
      obj.dim_ = d;
      obj.radius_ = r;
    end
    
  end
  
  %% Other methods
  methods
    
    function c = christoffelSymbols(obj, x)
      assert(isa(x, obj.point_class_str) && obj.isequal(x.getManifold()));

      dim = obj.dim_;
      r = obj.radius_;
      xval = x.getValue();

      c = zeros(dim, dim, dim);
      temp = 2 / (r * r + xval' * xval);

      for i = 1:dim
        for j = 1:dim
          c(i, j, j) = c(i, j, j) + temp * xval(i);
          c(j, i, j) = c(j, i, j) - temp * xval(i);
          c(j, j, i) = c(j, j, i) - temp * xval(i);
        end
      end

    end
    
    function y = convertPointToChart(obj, x, chart)

      assert(isa(x, obj.point_class_str) ...
        && obj.isequal(x.getManifold));

      assert(isa(chart, 'int32') && isscalar(chart) ...
        && chart == obj.chart_north_ || chart == obj.chart_south_);

      xval = x.getValue();
      xchart = x.getChart();

      if xchart == chart
        y = x.copy();
        return;
      end

      xx = xval' * xval;
      r = obj.radius_;
      rr = r * r;
      
      yval = rr * xval / xx;
            
      y = m3t.geometry.rm.Point(obj, chart, yval);
    end
    
    function v = convertTangentToPointChart(obj, x, u)
      
      xchart = x.getChart();
      uchart = u.getChart();
      
      if xchart == uchart
        v = u.copy();
        return;
      end
            
      uval = u.getValue();
      
      J = obj.getChartTransitionJacobian(x, uchart, xchart);
      
      vval = J * uval;
      
      v = m3t.geometry.rm.Tangent(obj, xchart, vval);
    end
        
    function p = createPoint(obj, x, varargin)
      if nargin == 2

        assert(isa(x, 'double') && iscolumn(x) ...
          && numel(x) == obj.euclideanDim() && norm(x) > 1e-3);

        xval = obj.projectPoint(x);

        r = obj.radius_;

        pval = r * xval(1:end-1);
        xend = xval(end);

        if xend >= 0
          pval = pval/(r + xend);
          chart = obj.chart_south_;
        else
          pval = pval/(r - xend);
          chart = obj.chart_north_;
        end

      elseif nargin == 3

        chart = varargin{1};
        assert(isa(chart, 'int32') ...
          && (chart == obj.chart_north_ || obj.chart_south_));
        assert(isa(x, 'double') && iscolumn(x) && obj.dim_ == numel(x));
        
        pval = x;
        
      else
        error(['Invalid number of arguments.', newline]);
      end

      p = m3t.geometry.rm.Point(obj, chart, pval);
    end
    
    function v = createTangent(obj, x, u)
      
      if isa(x, 'double')
        assert(iscolumn(x) && numel(x) == obj.euclideanDim() ...
          && norm(x) > 1e-5);
        assert(isa(u, 'double') ...
          && iscolumn(u) && numel(u) == obj.euclideanDim());
        
        r = obj.radius_;
        
        x = obj.projectPoint(x);
        u = obj.projectTangent(x, u);
        
        uend = u(end);
        xend = x(end);
        
        x = x(1:end-1);
        u = u(1:end-1);
        
        if xend >= 0
          chart = obj.chart_south_;
          t = r + xend;
          tt = t * t;
          vval = r / t * u - r * uend / tt * x;
        else
          chart = obj.chart_north_;
          t = r - xend;
          tt = t * t;
          vval = r/t * u + r * uend/tt * x;
        end
        
        v = m3t.geometry.rm.Tangent(obj, chart, vval);

      elseif isa(x, 'm3t.geometry.rm.Point')

        assert(obj.isequal(x.getManifold()));
        assert(isa(u, 'double') && iscolumn(u) && numel(u));

        if numel(u) == obj.dim_
          v = m3t.geometry.rm.Tangent(obj, x.getChart(), u);
        elseif numel(u) == obj.dim_ + 1
          v = obj.createTangent(obj.pointToEuclidean(x), u);
        else
          error(['Invalid argument size.', newline]);
        end
        
      else
        error(['Invalid argument type.', newline]);
      end
    end
    
    function d = dim(obj)
      d = obj.dim_;
    end
    
    function d = distance(obj, x, y)
      
      assert(isa(x,'m3t.geometry.rm.Point') ...
        && obj.isequal(x.getManifold()));
      assert(isa(y,'m3t.geometry.rm.Point') ...
        && obj.isequal(y.getManifold()));
      
      r = obj.radius_;
      xval = obj.pointToEuclidean(x);
      yval = obj.pointToEuclidean(y);
      
      t = xval' * yval / (r*r);
      if t - 1 > 0
        d = 0;
        return;
      end
      
      d = r * acos(t);
    end
    
    function d = dot(obj, x, u, v)
      assert(isa(x, obj.point_class_str) && ...
        obj.isequal(x.getManifold()));
      
      assert(isa(u, obj.tangent_class_str) ...
        && obj.isequal(u.getManifold()));
      assert(isa(v, obj.tangent_class_str) ...
        && obj.isequal(v.getManifold()));
      
      d = obj.tangentToEuclidean(x, u)' * obj.tangentToEuclidean(x, v);
    end
    
    function d = euclideanDim(obj)
      d = obj.dim_+ 1;
    end
    
    function y = exp(obj, x, u)
      assert(isa(x, obj.point_class_str) && ...
        obj.isequal(x.getManifold()));
      assert(isa(u, obj.tangent_class_str) ...
        && obj.isequal(u.getManifold()));
      
      r = obj.radius_;
      
      xx = obj.pointToEuclidean(x);
      uu = obj.tangentToEuclidean(x, u);
      
      tt = norm(uu) / r;
      if tt > 1e-2
        yy = xx * cos(tt) + uu / tt * sin(tt);
      else
        % for small tt use second-order Taylor approximation of sin(tt)/tt
        yy = xx * cos(tt) + uu * (1 - tt * tt / 6);
      end
      
      y = obj.createPoint(yy);
    end
    
    function b = isequal(obj, other)
      if isa(other, class(obj))
        b = (obj.dim_ == other.dim_) ...
          && abs(obj.radius_ - other.radius_) < 1e-6;
      else
        b = false;
      end
    end
    
    function charts = getChartList(obj)
      charts = [obj.chart_ambient_, obj.chart_north_, obj.chart_south_];
    end

    function J = getChartTransitionJacobian(obj, x, chart1, chart2, varargin)

      assert(isa(x, obj.point_class_str) ...
        && obj.isequal(x.getManifold()));

      r = obj.radius_;
      dim = obj.dim_;

      if nargin == 4

        if chart1 == chart2

          if chart1 == obj.chart_ambient_
            J = eye(dim + 1);
            return;
          else
            J = eye(dim);
            return;
          end

        elseif chart1 == obj.chart_ambient_

          xval = obj.pointToEuclidean(x);
          if chart2 == obj.chart_north_
            t = r - xval(end);
            J = r / t * [eye(dim), xval(1:end-1) / t];
            return;
          else % chart2 == obj.chart_south_
            t = r + xval(end);
            J = r / t * [eye(dim), -xval(1:end-1) / t];
            return;
          end

        elseif chart2 == obj.chart_ambient_

          xval = obj.convertPointToChart(x, chart1).getValue();
          rr = r * r;
          t = rr + xval' * xval;

          if chart1 == obj.chart_north_
            J = 2 * rr / t ...
              * [eye(dim) - 2 / t * (xval * xval'); 2 * r / t * xval'];
            return;
          else % chart1 == obj.chart_south_
            J = 2 * rr / t ...
              * [eye(dim) - 2 / t * (xval * xval'); -2 * r / t * xval'];
            return;
          end

        else
          xval = obj.convertPointToChart(x, chart1).getValue();
          t = xval' * xval;
          J = r * r / t * (eye(dim) - 2 * (xval * xval') / t);
          return;
        end

      elseif nargin == 5

        v = varargin{1};
        assert(isa(v, obj.tangent_class_str) ...
          && obj.isequal(v.getManifold()));

        if chart1 == chart2

          if chart1 == obj.chart_ambient_
            J = zeros(dim + 1);
            return;
          else
            J = zeros(dim);
            return;
          end

        elseif chart1 == obj.chart_ambient_

          xval = obj.pointToEuclidean(x);
          vval = obj.tangentToEuclidean(x, v);

          if chart2 == obj.chart_north_
            t = r - xval(end);
            J = r / (t * t) ...
              * [ vval(end) * eye(dim), ...
                 vval(1:end-1) + 2 * vval(end) / t *xval(1:end-1)];
            return;
          else
            t = r + xval(end);
            J = r / (t * t) ...
              * [ -vval(end) * eye(dim), ...
                 -vval(1:end-1) + 2 * vval(end) / t *xval(1:end-1)];
            return;
          end

        elseif chart2 == obj.chart_ambient_

          x = obj.convertPointToChart(x, chart1);
          v = obj.convertTangentToPointChart(x, v);

          xval = x.getValue();
          vval = v.getValue();

          rr = r * r;
          t = rr + xval' * xval;
          xv = xval' * vval;
          xx = xval * xval';

          if chart1 == obj.chart_north_
            J = 4 * rr / (t * t) ...
              * [4 * xv * xx / t - vval * xval' - xval * vval' - xv * eye(dim);
                 r * vval' - 4 * r * xv * xval' / t];
            return;
          else
            J = 4 * rr / (t * t) ...
              * [4 * xv * xx / t - vval * xval' - xval * vval' - xv * eye(dim);
                 -r * vval' + 4 * r * xv * xval' / t];
            return;
          end

        else

          x = obj.convertPointToChart(x, chart1);
          v = obj.convertTangentToPointChart(x, v);

          xval = x.getValue();
          vval = v.getValue();

          t = xval' * xval;
          xx = xval * xval';
          xv = xval' * vval;
          rr = r * r;

          J = 2 * rr / (t * t) ...
            * (4 * xv * xx / t - xv * eye(dim) - vval * xval' - xval * vval');
          return;
        end
        
      else
        error(['Invalid number of arguments.', newline]);
      end
    end

    function r = getRadius(obj)
      % m3t.geometry.rm.sphere.Sphere.getRadius
      %   Returns the radius of the sphere.
      r = obj.radius_;
    end
    
    function v = log(obj, x, y)
      
      assert(isa(x, obj.point_class_str) ...
        && obj.isequal(x.getManifold()));
      assert(isa(y, obj.point_class_str) ...
        && obj.isequal(y.getManifold()));
      
      r = obj.radius_;
      xx = obj.pointToEuclidean(x);
      yy = obj.pointToEuclidean(y);
      
      assert(norm(xx + yy) > 1e-3, ...
        ['Arguments x and y are antipodal points. There is no unique ', ...
        'tangent vector x so that exp(x,v) = y.', newline]);
      
      d = obj.distance(x, y);
      t = yy - xx .* (xx' * yy) / (r * r);

      if d < 1e-3
        % when xx' * yy is close to 1, use the fact that 
        % lim (x->1) acos(x) / sqrt(1 - x^2) = 1, so d/norm(t) -> 1 and
        % the logarithm formula then reduces to
        vv = t;
      else
        vv = d * t / norm(t);
      end

      v = obj.createTangent(xx, vv);
    end
    
    function mu = mean(obj, p, varargin)
      
      assert(isa(p, obj.point_class_str));
      num_p = numel(p);
      mu0 = [];
      
      if nargin == 2
        w = ones(1, num_p);
      elseif nargin == 3
        
        if isa(varargin{1}, obj.point_class_str)
          mu0 = varargin{1};
          w = ones(1, num_p);
          assert(isscalar(mu0));
        elseif isa(varargin{1}, 'double')
          w = varargin{1};
          assert(isvector(w) && numel(w) == num_p && all(w >= 0));
        else
          error(['Invalid arguments.', newline]);
        end
        
      elseif nargin == 4
        w = varargin{1};
        mu0 = varargin{2};
        
        assert(isa(mu0, obj.point_class_str) && isscalar(mu0));
        assert(isa(w, 'double') && isvector(w) && ...
          numel(w) == num_p && all(w > 0));
      else
        error(['Illegal number of arguments.', newline]);
      end
      
      if num_p == 1
        mu = p;
        return;
      end
      
      % if no initial guess is given choose the one of the given points such
      % that sum of its logs to other points has minimal norm
      if isempty(mu0)
        d = zeros(num_p);
        
        for i = 1:num_p
          for j = 1:num_p
            d(i,j) = obj.distance(p(i), p(j));
          end
        end
        
        [~, i] = min(sum(d));
        mu0 = p(i(1)).copy();
      end
        
      mu = obj.karcherMean(p, w, mu0);
    end
    
    function g = metricTensor(obj, x)
      assert(isa(x, obj.point_class_str) ...
        && obj.isequal(x.getManifold()));
      r = obj.radius_;
      rr = r*r;
      xval = x.getValue();
      xx = xval' * xval;
      g = 4 * rr * rr / (rr + xx)^2 * eye(obj.dim_);
    end
    
    function v = parallelTransport(obj, u, varargin)
      assert(isa(u, obj.tangent_class_str) ...
        && obj.isequal(u(1).getManifold()));
      
      r = obj.radius_;
      
      if numel(u) > 1
        for i = 2:numel(u)
          assert(obj.isequal(u(i).getManifold()));
        end
      end
      
      if nargin == 4
        
        x = varargin{1};
        y = varargin{2};
        
        assert(isa(x, obj.point_class_str) ...
          && obj.isequal(x.getManifold()));
          
        assert((isa(y, obj.point_class_str) ...
          && obj.isequal(y.getManifold()) ...
          || isa(y, obj.tangent_class_str)) ...
          && obj.isequal(y.getManifold()));
        
        if isa(y, obj.tangent_class_str)
          
          yy = obj.norm(x, y);
          
          if yy >= 2 * r * pi
            temp = mod(yy, (2 * r * pi));
            y = (temp / yy) * y;
          end
          
          yy = obj.norm(x, y);
          if yy > r * pi
            y = (1 - 2 * r * pi / yy) * y;
          end
            
          if obj.norm(x, y) > r * pi / 2
            v = obj.parallelTransport(u, x, y / 2);
            x2 = obj.exp(x, y/2);
            y2 = obj.parallelTransport(y, x, y / 2);
            v = obj.parallelTransport(v, x2, y2 / 2);
            return;
          end
          
          y = obj.exp(x, y);
        end
        
      elseif nargin > 4
        
        x = varargin{:};
        assert(isa(x, obj.point_class_str) ...
          && obj.isequal(x.getManifold()));
        num_x = numel(x);
        
        v = u;
        for i = 2:num_x
          assert(obj.isequal(x(i).getManifold()));
          v = obj.parallelTransport(v, x(i - 1), x(i));
        end
        
        return;
      else
        error(['Illegal number of arguments.',newline]);
      end
      
      xx = obj.pointToEuclidean(x);
      yy = obj.pointToEuclidean(y);
      
      xy = xx' * yy;
      rr = r * r;
      
      %logxy = obj.tangentToEuclidean(obj.log(x, y));
      %logyx = obj.tangentToEuclidean(obj.log(y, x));
      %d2 = obj.distance(x, y)^2;
        
      v = m3t.geometry.rm.Tangent.zeros(obj, u(1).getChart(), numel(u));

      for i = 1:numel(u)
        uu = obj.tangentToEuclidean(x, u(i));
        %vv = uu - logxy' * uu /d2 * (logxy - logyx);
        % avoid using expression with log maps since it is undefined when 
        % d2 == 0.
        vv = uu - ((rr*yy - xx*xy)' * uu)/(rr * (rr + xy)) * (xx + yy); 
        v(i) = obj.createTangent(yy, vv);
      end
      
    end
    
    function x_eucl = pointToEuclidean(obj, x)
      assert(isa(x, obj.point_class_str) ...
        && isscalar(x) ...
        && obj.isequal(x.getManifold()));
      
      r = obj.radius_;
      rr = r*r;
      xval = x.getValue();
      xx = xval' * xval;
      
      if x.getChart() == obj.chart_north_
        x_eucl = [2*r*xval; (xx - rr)] * r/(xx + rr);
      else
        x_eucl = [2*r*xval; (rr - xx)] * r/(xx + rr);
      end
      
    end
    
    function mu = riemannCurvature(obj, x, u, v, w)
      
      assert(isa(x, obj.point_class_str) ...
        && isscalar(x) && obj.isequal(x.getManifold()));
      
      assert(isa(u, obj.tangent_class_str) ...
        && isscalar(u) && obj.isequal(u.getManifold()));
      assert(isa(v, obj.tangent_class_str) ...
        && isscalar(v) && obj.isequal(v.getManifold()));
      assert(isa(w, obj.tangent_class_str) ...
        && isscalar(w) && obj.isequal(w.getManifold()));
      
      uu = obj.tangentToEuclidean(x, u);
      vv = obj.tangentToEuclidean(x, v);
      ww = obj.tangentToEuclidean(x, w);
      
      r = obj.radius_;
      
      muval = ((ww' * vv) * uu - (ww' * uu) * vv) / (r * r);
      mu = obj.createTangent(x, muval);
    end
    
    function R = riemannCurvatureTensor(obj, x)
      assert(isa(x, obj.point_class_str) ...
        && obj.isequal(x.getManifold()));

      r = obj.radius_;
      xval = x.getValue();
      dim = obj.dim();

      R = zeros(dim, dim, dim, dim);
      temp = 4 * r * r / (r * r + xval' * xval);

      for i = 1:dim
        for j = 1:dim
          for k = 1:dim
            for l = 1:dim
              R(i, j, k, l) = ...
                ((k == i) * (l == j) - (k == j) * (i == l)) * temp;
            end
          end
        end
      end
    end

    function v_eucl = tangentToEuclidean(obj, x, v)
      
      assert(isa(x, obj.point_class_str) ...
        && isscalar(x) && obj.isequal(x.getManifold()));
      assert(isa(v, obj.tangent_class_str) ...
        && isscalar(v) && obj.isequal(v.getManifold()));
      
      if x.getChart() ~= v.getChart()
        v = obj.convertTangentToPointChart(x, v);
      end
      
      r = obj.radius_;
      rr = r*r;
      
      vval = v.getValue();
      xval = x.getValue();
      
      xx = xval' * xval;
      xv = xval' * vval;
      
      t = rr + xx;
      
      if x.getChart() == obj.chart_north_
        v_eucl = [2*rr*vval/t - 4*rr/(t*t)*xv*xval; 4*rr*r/(t*t)*xv];
      else
        v_eucl = [2*rr*vval/t - 4*rr/(t*t)*xv*xval; -4*rr*r/(t*t)*xv];
      end
    end
    
  end
  
  methods (Access = protected)

    function y = projectPoint(obj, x)
      y = x / norm(x) * obj.radius_;
    end

    function v = projectTangent(obj, x, u)
      x = obj.projectPoint(x);
      v = u - (u' * x)/(obj.radius_ * obj.radius_) * x;
    end

  end
  
end

