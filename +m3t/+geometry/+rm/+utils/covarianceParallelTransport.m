% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
%
%   m3t.geometry.rm.utils.covarianceParallelTransport
%
%     P = m3t.geometry.rm.utils.COVARIANCEPARALLELTRANSPORT(P1, x, y)
%         Computes the parallel transport of a covariance matrix on a manifold
%         from the tangent space at point x to the tangent space at point y
%         along the unique shortest geodesic (Log(x, y)) if it exists.
%
%     P = m3t.geometry.rm.utils.COVARIANCEPARALLELTRANSPORT(P1, x, v)
%         Computes the parallel transport of a covariance matrix on a manifold
%         from the tangent space at point x along a geodesic defined by the
%         tangent vector v.
%
%     P = m3t.geometry.rm.utils.COVARIANCEPARALLELTRANSPORT(P1, x1, x2, ...)
%         Computes the parallel transport of a covariance matrix on a manifold
%         along a piecewise geodesic path among points x1, x2, ...
%
% ------------------------------------------------------------------------------
function P2 = covarianceParallelTransport(P1, x, varargin)
  assert(isa(x, 'm3t.geometry.rm.AbstractPoint'));
  manifold = x.getManifold();
  dim = manifold.dim();
  P2 = zeros(dim);
  [V, e] = eig(P1, 'vector');
  for i = 1:dim
    vec = m3t.geometry.rm.Tangent(manifold, x.getChart(), V(:, i));
    vec = manifold.parallelTransport(vec, x, varargin{:});
    val = vec.getValue();
    P2 = P2 + e(i) * (val * val');
  end
end

