% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
%
%   m3t.geometry.rm.utils.covarianceTrace - Trace of covariance matrix
%
%     tr = m3t.geometry.rm.utils.COVARIANCETRACE(x, P)
%         Calculates the trace of covariance matrix P at point x of a Riemannian
%         manifold, where P is expressed in same local coordinates as point x.
%
% ------------------------------------------------------------------------------
function tr = covarianceTrace(x, P)
  assert(isa(x, 'm3t.geometry.rm.AbstractPoint'));
  manifold = x.getManifold();
  G = manifold.metricTensor(x);
  tr = trace(G * P);
end

