% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
%
%   m3t.geometry.rm.Manifold - Abstract Riemannian manifold class
%
%     Manifold is a subclass of a matlab.mixin.Copyable class
%
%     Manifold methods (abstract):
%       createCovariance    - creates Riemannian manifold covariance object
%       createPoint         - creates Riemannian manifold point
%       createTangent       - creates tangent vector object
%       dim                 - returns dimension of a manifold
%       distance            - returns distance between points
%       dot                 - returns scallar product of tangent vectors
%       euclideanDim        - returns dimension of ambient Euclidean space
%       exp                 - applies exponential mapping
%       log                 - applies logarithmic mapping
%       mean                - calculates mean of manifold points
%       metricTensor        - returns metric tensor in local coordinates
%       parallelTransport   - parallel transport of tangent vectors
%       pointToEuclidean    - converts point to ambient Euclidean representation
%       riemannCurvature    - evaluates Riemannian curvature
%       tangentToEuclidean  - converts tangent vector to ambient Euclidean
%                             representation
%
%     Manifold methods:
%       basis         - returns orthonormal basis of tangent space
%       karcherMean   - calculates Kärcher mean of points if possible
%       norm          - returns norm of a tangent vector
%
%       getKarcherMeanSettings  - gets karcher_mean_optim_settings_ property
%       setKarcherMeanSettings  - sets karcher_mean_optim_settings_ property
%
% ------------------------------------------------------------------------------

classdef (Abstract) Manifold < matlab.mixin.Copyable
  
  properties (Access = private)
    karcher_mean_optim_setting_;
  end
  
  methods
    
    function obj = Manifold()
      obj.setKarcherMeanSettings();
    end
    
  end
  
  methods(Abstract)
    
    % m3t.geometry.rm.Manifold.christofferSymbols
    %
    %   c = obj.CHROSTOFFELSYMBOLS(x)
    %       Returns the Christoffel symbols of the manifold at point x in
    %       the same coordinate chart as x. Symbols are stored in 3-D array.
    c = christoffelSymbols(obj, x);

    y = convertPointToChart(obj, x, chart)

    v = convertTangentToPointChart(obj, x, u)
    
    % m3t.geometry.rm.Manifold.createPoint
    %
    %   p = obj.CREATEPOINT(x)
    %       Creates a m3t.geometry.rm.Point object describing manifold point
    %       given by the value x. Argument x must be of the appropriate
    %       dimensions depending on the subclass implementation.
    p = createPoint(obj, x);
    
    % m3t.geometry.rm.Manifold.createTangent
    %
    %   v = obj.CREATETANGENT(x, u)
    %       Creates a m3t.geometry.rm.Tangent object describing a tangent
    %       vector given by the value u at point x. Argument x must be of
    %       m3t.geometry.rm.Point type. Argument u must be of the appropriate
    %       dimensions depending on the subclass implementation.
    v = createTangent(obj, x, u);
    
    % m3t.geometry.rm.Manifold.dim
    %
    %   d = obj.DIM()
    %       Returns the dimension of the manifold. This may differ from the
    %       dimension of the Euclidean space in which the manifold is
    %       embedded.
    d = dim(obj);
    
    % m3t.geometry.rm.Manifold.distance
    %
    %   d = obj.DISTANCE(x, y)
    %       Returns the Riemannian (geodesic) distance between two points of
    %       the manifold. Both x and y must be of m3t.geometry.rm.Point type.
    d = distance(obj, x, y);
    
    % m3t.geometry.rm.Manifold.dot
    %
    %   d = obj.DOT(x, u, v)
    %       Returns the Riemannian dot product of tangent vectors u and v at 
    %       point x. Argument x must be of m3t.geometry.rm.Point type and both
    %        u and v must be of m3t.geometry.rm.Tangent type.
    d = dot(obj, x, u, v);
    
    % m3t.geometry.rm.Manifold.euclideanDim
    %
    %   d = obj.EUCLIDEANDIM()
    %       Returns the dimension of the ambient Euclidean space in which the
    %       manifold is embedded.
    d = euclideanDim(obj);
    
    % m3t.geometry.rm.Manifold.exp
    %
    %   y = obj.EXP(x, v)
    %       Returns the exponential map ot the tangent vector v at point x of
    %       the manifold. Argument x must be of the m3t.geometry.rm.point type 
    %       and  v must be of the m3t.geometry.rm.Tangent type.
    y = exp(obj, x, v);
    
    % m3t.geometry.rm.Manifold.getChartList
    %
    %   list = obj.GETCHARTLIST()
    %       Returns the list of coordinate charts used by manifold.
    charts = getChartList(obj);
    
    % m3t.geometry.rm.Manifold.getChartTransitionJacobian
    %
    %   J = obj.GETCHARTTRANSITIONJACOBIAN(obj, x, chart1, chart2, varargin)
    %       Returns Jacobian of coordinate transformation from chart1 to chart2
    J = getChartTransitionJacobian(obj, x, chart1, chart2, varargin)

    % m3t.geometry.rm.Manifold.isequal
    %
    %   res = obj.ISEQUAL(other)
    %       Returns true if obj and other represent the same manifold.
    b = isequal(obj, other);
    
    % m3t.geometry.rm.Manifold.log
    %
    %   v = obj.LOG(x, y)
    %       Returns the logaritmic (inverse exponential) map of point y to the
    %       tangent space at point x. Both x and y must be of
    %       m3t.geometry.rm.Point type.
    v = log(obj, x, y);
    
    % m3t.geometry.rm.Manifold.mean
    %
    %   mu = obj.MEAN(p)
    %       Returns the mean of points p on the manifold. Argument p must be an
    %       array of m3t.geometry.rm.Point type.
    %
    %   mu = obj.MEAN(p, w)
    %       Returns the weighted mean of points on the manifold. Argument p must
    %       be an array of m3t.geometry.rm.Point type and argument w must be an
    %       array of nonnegative double values. Both arrays must be of the same
    %       length.
    %
    %   mu = obj.MEAN(p, p0)
    %       Returns the mean of points p on the manifold. Argument p0 is used as
    %       the initial guess if an optimisation method is used to calculate the
    %       mean. Argument p must be an array of m3t.geometry.rm.Point type and
    %       p0 must be a scalar of m3t.geometry.rm.Point type.
    %
    %   mu = obj.MEAN(p, w, p0)
    %       Returns the weighted mean of points on the manifold. Argument p0 is
    %       used as the initial guess if an optimisation method is used to
    %       calculate the mean. Argument p must be an array of
    %       m3t.geometry.rm.Point type and p0 must be a scalar of
    %       m3t.geometry.rm.Point type. Argument w must be an array of
    %       nonnegative double values. Both p and w must be arrays of the same
    %       length.
    mu = mean(obj, p, varargin);
    
    % m3t.geometry.rm.Manifold.metricTensor
    %
    %   obj.METRICTENSOR(x)
    %     Returns metric tensor g of a manifold at point x. Argument x must be a
    %     scalar of m3t.geometry.rm.Point type.
    g = metricTensor(obj, x);
    
    % m3t.geometry.rm.Manifold.parallelTransport
    %
    %   v = obj.PARALLELTRANSPORT(u, w)
    %       Applies the parallel transport of tangent vector u from point x
    %       along a geodesic defined by the tangent vector w. Equivalent to
    %       obj.PARALLELTRANSPORT(u, obj.exp(w)). Both u and w must be scalars
    %       of m3t.geometry.rm.Tangent type belonging to the same tangent space.
    %
    %   v = obj.PARALLELTRANSPORT(u, x)
    %       Applies the parallel transport of the tangent vector u to the
    %       tangent space at point y along a geodesic. If there is no unique
    %       shortest geodesic connecting x and y, the method fails. Argument u
    %       must be a scalar of m3t.geometry.rm.Tangent type, x must be a
    %       scalar of m3t.geometry.rm.Point type.
    %
    %   v = obj.PARALLELTRANSPORT(u, x1, x2, ...)
    %       Applies the parallel transport of tangent vector u along a path
    %       defined by geodesics connecting points x1, x2, ... If there is no
    %       unique shorthest geodesic between two consequtive points x, the
    %       method fails. Argument u must ba a scalar of m3t.geometry.rm.Tangent
    %       type. Arguments x1, x2, ... must be scalars of m3t.geometry.rm.Point
    %       type.
    v = parallelTransport(obj, u, varargin);
    
    % m3t.geometry.rm.Manifold.pointToEuclidean
    %
    %   x = obj.POINTTOEUCLIDEAN(p)
    %       Converts point p to Euclidean representation in embedding space.
    x = pointToEuclidean(obj, p);
    
    % m3t.geometry.rm.Manifold.riemannCurvature
    %
    %   mu = obj.RIEMANNCURVATURE(x, u, v, w)
    %       Evaluates the Riemann curvature of tangent vectors u, v and w at
    %       point x. Argument x must be of m3t.geometry.rm.Point type and 
    %       arguments u, v and w must all be of m3t.geometry.rm.Tangent type.
    mu = riemannCurvature(obj, x, u, v, w);

    % m3t.geometry.rm.Manifold.riemannCurvatureTensor
    %
    %   R = obj.RIEMANNCURVATURETENSOR(x)
    %       Returns Riemann curvature tensor R^{i}_{jkl} as a 4-D array in the
    %       same coordinate chart as point x. Returned value is contravariant in
    %       first index and covariant in other indices.
    R = riemannCurvatureTensor(obj,x);
        
    % m3t.geometry.rm.Manifold.tangentToEuclidean
    %
    %   x = obj.TANGENTTOEUCLIDEAN(x, v)
    %       Converts tangent vector v at point x to Euclidean representation
    %       in embedding space.
    x = tangentToEuclidean(obj, x, v);
  end
  
  methods
    
    % m3t.geometry.rm.Manifold.basis
    %
    %   b = obj.BASIS(x)
    %       Returns an orthonormal basis that spans the tangent space of
    %       manifold at point x.
    function b = basis(obj, x)
      assert(isa(x, 'm3t.geometry.rm.Point') ...
        && obj.isequal(x.getManifold()));
      b = m3t.geometry.rm.Tangent.empty();
      g = obj.metricTensor(x);
      [U, d] = eig(g, 'vector');
      for i = 1:obj.dim()
        vval = U(:,i) / sqrt(d(i));
        b(i) = obj.createTangent(x, vval);
      end
    end
    
    function settings = getKarcherMeanSettings(obj)
      % m3t.geometry.rm.Manifold.getKarcherMeanSettings
      %   Returns the structure containing optimisation settings used by Kärcher
      %   mean method.
      settings = obj.karcher_mean_optim_setting_;
    end
    
    function setKarcherMeanSettings(obj, varargin)
      % m3t.geometry.rm.Manifold.setKarcherMeanSettings
      %
      %   obj.SETKARCHERMEANSETTINGS(param1, value1, param2, value2, ...)
      %       Sets optimisation parameters of the Karcher mean method.
      %       Parameters are specified by the name-value pairs. Following
      %       parameters are available:
      %
      %         'maxIter'     - Max number of iterations. Must be a positive
      %                         integer. Default value is 20.
      %
      %         'absTol'      - Absolute tolerance, must be a positive double
      %                         scalar value. Default is 1e-5.
      %
      %         'relTol'      - Relative tolerance, must be a positive double
      %                         scalar value. Default is 1e-4.
      %
      %         'gradTol'     - Gradient tolerance, must be a positive double
      %                         scalar value. Default is 1e-3.
      %
      %         'warnings'    - Enables displaying warnings. Must be a logical
      %                         scalar. Default is true.
      %
      %         'display'     - Controls the display of optimisation info.
      %                         Alowed values are 'on', 'off' and 'final'. When
      %                         on, the info is displayed after each iteration.
      %                         When final, only final info is displayed. When
      %                         off (default), no info is displayed.
      
      parser = inputParser();
      
      valid_tol = @(x) isa(x,'double') && isscalar(x) && isnumeric(x) && x > 0;
      valid_int = @(x) isinteger(x) && isscalar(x) && isnumeric(x) && x > 0;
      valid_logical = @(x) islogical(x) && isscalar(x);
      valid_display = @(x) ischar(x) && any(strcmp(x, {'on', 'off', 'final'}));
      
      parser.addParameter('maxIter', int32(20), valid_int);
      parser.addParameter('absTol', 1e-5, valid_tol);
      parser.addParameter('relTol', 1e-4, valid_tol);
      parser.addParameter('gradTol', 1e-3, valid_tol);
      parser.addParameter('warnings', true, valid_logical);
      parser.addParameter('display', 'off', valid_display);
      
      parser.parse(varargin{:});
      res = parser.Results;
      
      obj.karcher_mean_optim_setting_ = struct(...
        'maxIter', res.maxIter, ...
        'absTol', res.absTol, ...
        'relTol', res.relTol, ...
        'gradTol', res.gradTol, ...
        'warnings', res.warnings, ...
        'display', res.display);
    end
    
    function mu = karcherMean(obj, p, w, mu0)
      % m3t.geometry.rm.Manifold.KarcherMean
      %
      %   mu = obj.KARCHERMEAN(p, w, mu0)
      %     Computes weighted Karcher mean of the points in the array p with
      %     weights w using the Gauss-Newton method starting from the point
      %     mu0. Argument p must be an array of m3t.geometry.rm.Point type.
      %     Argument mu0 must be a scalar of m3t.geometry.rm.Point type.
      %     Argument w must be an array of nonnegative double values. Arrays p
      %     and w must be of the same length.
      assert(isa(p, 'm3t.geometry.rm.AbstractPoint'));
      assert(isa(mu0, 'm3t.geometry.rm.AbstractPoint'));
      assert(isa(w, 'double') && isvector(w));
      assert(numel(w) == numel(p));
      
      num_p = numel(p);
      max_iter = obj.karcher_mean_optim_setting_.maxIter;
      abs_tol = obj.karcher_mean_optim_setting_.absTol;
      rel_tol = obj.karcher_mean_optim_setting_.relTol;
      grad_tol = obj.karcher_mean_optim_setting_.gradTol;
      warnings = obj.karcher_mean_optim_setting_.warnings;
      display = obj.karcher_mean_optim_setting_.display;
      mu = mu0;
      
      [cost, dir] = evaluateCost(p, w, mu);
      
      iter = 1;
      termination_cause = 'Unknown.';
      init_cost = cost;
      
      if isequal(display,'on')
        num_digits = floor(log10(double(max_iter) + 1)) + 1;
        fprintf('\n');
      end
      
      while true
        
        step = obj.norm(mu, dir);
        
        mu = obj.exp(mu, dir);
        [new_cost, dir] = evaluateCost(p, w, mu); 
        
        if isequal(display,'on')
          fprintf(['\tIter: %' num2str(num_digits) 'd | Cost: %.4e | Cost diff: %.4e | ' ...
            'Step size: %.5e |\n'],...
            iter, cost, abs(cost - new_cost), step);
        end
        
        if step < grad_tol
          termination_cause = 'Gradient tolerance reached.';
          break;
        end
        
        if (abs(cost - new_cost) < cost*rel_tol) || new_cost < abs_tol
          termination_cause = 'Cost tolerance.';
          break;
        elseif iter == max_iter
          if warnings
            warning(...
              ['Max iteration number exceeded. Cost value: %d', newline], ...
              new_cost);
          end
          termination_cause = 'Maximum number of iterations reached.';
          break;
        else
          iter = iter + 1;
          cost = new_cost;
        end
        
      end
      
      if isequal(display, 'on') || isequal(display, 'final')
        fprintf(['\n\tOptimisation terminated by: %s ' ...
          '\n\tIterations: %d.\n\tInitial cost: %f.\n\tFinal cost: %f.\n'], ...
          termination_cause, iter, init_cost, cost);
      end
      
      function [cost, dir] = evaluateCost(p, w, mu)
        
        cost = 0;
        sum_w = 0;
        dir = obj.log(mu, mu);
        
        for ii = 1:num_p
          log = obj.log(mu ,p(ii));
          cost = cost + w(ii)*obj.dot(mu, log, log);
          dir = dir + w(ii)*log;
          sum_w = sum_w + w(ii);
        end
        
        cost = cost/sum_w;
        dir = dir/sum_w;
      end
      
    end
    
    function g = norm(obj,x, u)
      % m3t.geometry.rm.Manifold.norm
      %
      %   g = obj.NORM(x, u)
      %       Returns Riemannian norm of a tangent vector u in tangent space at
      %       point x. Argument u must be a scalar of m3t.geometry.rm.Tangent
      %       type.
      g = sqrt(obj.dot(x, u, u));
    end
    
    
  end
  
end

