classdef BundlePoint < m3t.geometry.rm.AbstractPoint
  %BUNDLEPOINT Summary of this class goes here
  %   Detailed explanation goes here
  
  properties (Access = private)
    tangent_bundle_;
    base_point_;
    tangent_;
  end
  
  methods

    function obj = BundlePoint(manifold, base_point, tangent)

      assert(isa(manifold, 'm3t.geometry.rm.TangentBundle'));
      assert(isa(base_point, 'm3t.geometry.rm.AbstractPoint') ...
        && manifold.getBaseManifold().isequal(base_point.getManifold()));
      assert(isa(tangent, 'm3t.geometry.rm.Tangent') ...
        && manifold.getBaseManifold().isequal(tangent.getManifold()));

      obj.tangent_bundle_ = manifold.copy();
      obj.base_point_ = base_point.copy();
      obj.tangent_ = manifold.getBaseManifold().convertTangentToPointChart(...
        base_point, tangent);

    end
    
    function d = dim(obj)
      d = obj.tangent_bundle_.dim();
    end

    function x = euclideanValue(obj)
      x = obj.tangent_bundle_.pointToEuclidean(obj);
    end

    function x = getBasePoint(obj)
      x = obj.base_point_.copy();
    end

    function c = getChart(obj)
      c = obj.base_point_.getChart();
    end

    function m = getManifold(obj)
      m = obj.tangent_bundle_;
    end

    function v = getTangent(obj)
      v = obj.tangent_.copy();
    end
    
    function x = getValue(obj)
      x = [obj.base_point_.getValue(); obj.tangent_.getValue()];
    end

    function r = isequal(obj, other)
      if isa(other, class(obj))
        if obj.tangent_bundle_.isequal(other.getManifold())
          r = norm(obj.euclideanValue() - other.euclideanValue()) < 1e-5;
          return;
        end
      end
      r = false;
    end
    
  end
end

