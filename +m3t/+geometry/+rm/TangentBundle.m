% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%

classdef TangentBundle < m3t.geometry.rm.Manifold
  
  properties (Access = private)
    base_;
    log_optim_settings_;
  end
  
  methods
    
    function obj = TangentBundle(manifold)
      assert(isa(manifold, 'm3t.geometry.rm.Manifold'));
      obj.base_ = manifold.copy();
      
      % set default optimisation settings for log mapping
      obj.setLogOptimisationSettings();
    end
    
    function V = basis(obj, x)
      dim = obj.base_.dim();
      B = obj.base_.basis(x.getBasePoint());

      V = m3t.geometry.rm.Tangent.zeros(obj, x.getChart(), 2 * dim);
      for i = 1:dim
        V(i) = obj.horizontalLift(x, B(i));
        V(i + dim) = obj.verticalLift(x, B(i));
      end

    end
    
    function c = christoffelSymbols(obj, x)
      % TODO
      c = [];
    end
    
    function y = convertPointToChart(obj, x, chart)

      assert(isa(x, 'm3t.geometry.rm.BundlePoint')...
        && obj.isequal(x.getManifold()));

      p = x.getBasePoint();
      v = x.getTangent();

      p_new = obj.base_.convertPointToChart(p, chart);
      v_new = obj.base_.convertTangentToPointChart(p_new, v);

      y = m3t.geometry.rm.BundlePoint(obj, p_new, v_new);
    end
    
    function v = convertTangentToPointChart(obj,x,u)

      xchart = x.getChart();
      uchart = u.getChart();

      if xchart ~= uchart
        J = obj.getChartTransitionJacobian(x, u.getChart(), x.getChart());
        v = m3t.geometry.rm.Tangent(obj, xchart, J * u.getValue());
      else
        v = u.copy();
      end
    end

    function x = createPoint(obj, varargin)
      
      if nargin == 2
        value = varargin{1};

        assert(isa(value, 'double') ...
          && iscolumn(value) && numel(value) == obj.euclideanDim());

        dim = numel(value) / 2;

        p = obj.base_.createPoint(value(1:dim));
        v = obj.base_.createTangent(p, value(dim+1:end));

        x = m3t.geometry.rm.BundlePoint(obj, p, v); 
      elseif nargin == 3
        p = varargin{1};
        v = varargin{2};

        assert(isa(p, 'm3t.geometry.rm.AbstractPoint') ...
          && obj.base_.isequal(p.getManifold()));
        assert(isa(v, 'm3t.geometry.rm.Tangent') ...
          && obj.base_.isequal(v.getManifold()));

        v = obj.base_.convertTangentToPointChart(p, v);

        x = m3t.geometry.rm.BundlePoint(obj, p, v);
      else
        error(['Invalid number of arguments.', newline])
      end

    end
    
    function v = createTangent(obj, x, u)
      
      if isa(x, 'double')

        dim = obj.euclideanDim();

        assert(iscolumn(x) && numel(x) == dim);
        assert(isa(u, 'double') && iscolumn(u) && numel(u) == dim);

        p = obj.createPoint(x);
        chart = p.getChart(x);

        J = obj.getChartTransitionJacobian(x, int32(0), chart);

        v = m3t.geometry.rm.Tangent(obj, chart, J*u);
        
      elseif isa(x, 'm3t.geometry.rm.BundlePoint')

        assert(obj.isequal(x.getManifold()));
        assert(isa(u, 'double') && iscolumn(u) && numel(u) == obj.dim());

        v = m3t.geometry.rm.Tangent(obj, x.getChart(), u);

      else
        error(['Invalid argument type.', newline]);
      end

    end
    
    function [hor, ver] = decomposeTangent(obj, x, u)

      assert(isa(x, 'm3t.geometry.rm.BundlePoint') ...
        && obj.isequal(x.getManifold()));
      assert(isa(u, 'm3t.geometry.rm.Tangent') ...
        && obj.isequal(u.getManifold()));

      dim = obj.base_.dim();
      chart = x.getChart();

      u = obj.convertTangentToPointChart(x, u);

      p = x.getBasePoint();
      v = x.getTangent();
      vval = v.getValue();

      gamma = obj.base_.christoffelSymbols(p);

      uval = u.getValue();
      hor = m3t.geometry.rm.Tangent(obj.base_, chart,  uval(1:dim));

      ver = uval(dim+1:end);
      for i = 1:dim
        for j = 1:dim
          for k = 1:dim
            ver(i) = ver(i) + gamma(i, j, k) * uval(j) * vval(k);
          end
        end
      end

      ver = m3t.geometry.rm.Tangent(obj.base_, chart, ver);
    end

    function d = dim(obj)
      d = obj.base_.dim() * 2;
    end
    
    function d = distance(obj, x, y)
      assert(isa(x, 'm3t.geometry.rm.BundlePoint') && ...
        obj.isequal(x.getManifold()));
      assert(isa(y, 'm3t.geometry.rm.BundlePoint') && ...
        obj.isequal(y.getManifold()));
      
      d = obj.norm(x, obj.log(x, y));
    end
    
    function d = dot(obj, x, u, v)

      assert(isa(x, 'm3t.geometry.rm.BundlePoint') ...
        && obj.isequal(x.getManifold()));
      assert(isa(u, 'm3t.geometry.rm.Tangent') && ...
        obj.isequal(u.getManifold()));
      assert(isa(v, 'm3t.geometry.rm.Tangent') && ...
        obj.isequal(v.getManifold()));

      p = x.getBasePoint();
      
      [u_h, u_v] = obj.decomposeTangent(x, u);
      [v_h, v_v] = obj.decomposeTangent(x, v);
            
      d = obj.base_.dot(p, u_h, v_h) + obj.base_.dot(p, u_v, v_v);
      
    end
    
    function d = euclideanDim(obj)
      d = 2 * obj.base_.euclideanDim();
    end
    
    function y = exp(obj, x, v)

      assert(isa(x, 'm3t.geometry.rm.BundlePoint') ...
        && obj.isequal(x.getManifold()));
      assert(isa(v, 'm3t.geometry.rm.Tangent') && ...
        obj.isequal(v.getManifold()));
      
      manifold = obj.base_;
      
      p = x.getBasePoint();
      u = x.getTangent();
      
      [nu, mu] = obj.decomposeTangent(x, v);
      norm_v = sqrt(manifold.dot(p, nu, nu) + manifold.dot(p, mu, mu));
      
      if manifold.norm(p, mu) <= 1e-5 || manifold.norm(p, nu) <= 1e-5
        p_old = p;
        p = manifold.exp(p_old, nu);
        u = manifold.parallelTransport(u + mu, p_old, nu);
      else

        num_points = min(ceil(norm_v / 0.01), 500);
        step = 1/num_points;

        for i = 1:num_points

          r = manifold.riemannCurvature(p, u, mu, nu);

          delta_p = step * nu;

          p_new = manifold.exp(p, delta_p);
          u = manifold.parallelTransport(u + step * mu, p, delta_p);
          nu = manifold.parallelTransport(nu - step * r, p, delta_p);
          mu = manifold.parallelTransport(mu, p, delta_p);
          p = p_new;
          
          u = manifold.convertTangentToPointChart(p, u);
          nu = manifold.convertTangentToPointChart(p, nu);
          mu = manifold.convertTangentToPointChart(p, mu);
          
        end

      end
      
      y = obj.createPoint(p, u);
      
    end
    
    function base = getBaseManifold(obj)
      base = obj.base_.copy();
    end
    
    function c = getChartList(obj)
      c = obj.base_.getChartList();
    end

    function J = getChartTransitionJacobian(obj, x, chart1, chart2)
      p = x.getBasePoint();
      v = x.getTangent();
      J1 = obj.base_.getChartTransitionJacobian(p, chart1, chart2);
      J2 = obj.base_.getChartTransitionJacobian(p, chart1, chart2, v);
      J = [J1  zeros(size(J1));
           J2 J1];
    end

    function optimset = getLogOptimisationSettings(obj)
      optimset = obj.log_optim_settings_;
    end
    
    function v = horizontalLift(obj, x, u)
      assert(isa(x, 'm3t.geometry.rm.BundlePoint') ...
        && obj.isequal(x.getManifold()));
      assert(isa(u, 'm3t.geometry.rm.Tangent') ...
        && obj.base_.isequal(u.getManifold()));

      dim = obj.base_.dim();

      chart = x.getChart();
      wval = x.getTangent().getValue();

      u = obj.base_.convertTangentToPointChart(x.getBasePoint(), u);
      uval = u.getValue();

      gamma = obj.base_.christoffelSymbols(x.getBasePoint());
      temp = zeros(dim, 1);
      for i = 1:dim
        for j = 1:dim
          for k = 1:dim
            temp(i) = temp(i) + gamma(i, j, k) * uval(j) * wval(k);
          end
        end
      end

      vval = [uval; - temp];

      v = m3t.geometry.rm.Tangent(obj, chart, vval);
    end
    
    function r = isequal(obj, other)
      if isa(other, 'm3t.geometry.rm.TangentBundle')
        r = obj.base_.isequal(other.getBaseManifold());
      else
        r = false;
      end
    end

    function v = log(obj, x, y)
      
      assert(isa(x, 'm3t.geometry.rm.BundlePoint') && ...
        obj.isequal(x.getManifold()));
      assert(isa(y, 'm3t.geometry.rm.BundlePoint') && ...
        obj.isequal(y.getManifold()));
      
      % load optimisation parameters:
      L = obj.log_optim_settings_.numPoints + 2;
      abs_tol = obj.log_optim_settings_.absTol;
      rel_tol = obj.log_optim_settings_.relTol;
      grad_tol = obj.log_optim_settings_.gradTol;
      max_iter = obj.log_optim_settings_.maxIter;
      step = obj.log_optim_settings_.initStep;
      min_step = obj.log_optim_settings_.minStep;
      c = obj.log_optim_settings_.armijoCtrlParam;
      a = obj.log_optim_settings_.armijoStepShrink;
      b = obj.log_optim_settings_.armijoStepExpand;
      warnings = obj.log_optim_settings_.warnings;
      display = obj.log_optim_settings_.display;
      conjugate = obj.log_optim_settings_.useConjugate;
      dim = (L - 2) * obj.dim();
      
      base = obj.base_;

      iter = 1;
      termination_cause = 'Unknown.';

      %y = obj.convertPointToChart(y, x.getChart());
      
      [p, u] = createMidPoints(x, y, L);
      [v, w] = calculateTangents(p, u, L);
      cost = evaluateCost(p, v, w, L);
      init_cost = cost;
      
      steepest_direction = true;
      grad_norm_sq = 0;

      % steepest descent direction
      dp = m3t.geometry.rm.Tangent.zeros(base, p(1).getChart(), L);
      du = m3t.geometry.rm.Tangent.zeros(base, p(1).getChart(), L);

      % conjugate direction
      cdp = m3t.geometry.rm.Tangent.zeros(base, p(1).getChart(), L);
      cdu = m3t.geometry.rm.Tangent.zeros(base, p(1).getChart(), L);

      if isequal(display,'on')
        num_digits = floor(log10(max_iter+1)) + 1;
        fprintf('\n');
      end
      

      while true
        
        if ~conjugate || mod(iter - 1, dim) == 0
          steepest_direction = true;
        end
        
        [dv, dw] = calculateDerivatives(p, v, w, L);
        [grad_p, grad_u] = calculateGradient(p, u, v, w, dv, dw, L);
        
        % steepest descent direction
        for i = 2:L
          dp(i) = -grad_p(i);
          du(i) = -grad_u(i);
        end
        
        if steepest_direction
          % in first iter use the steepest descend direction
          for i = 2:L
            cdp(i) = dp(i);
            cdu(i) = du(i);
          end
          for i = 2:L
            grad_norm_sq = grad_norm_sq + base.dot(p(i), dp(i), dp(i)) ...
              + base.dot(p(i), du(i), du(i));
          end
        else
          % Fletcher-Reeves
          beta = 1 / grad_norm_sq;
          grad_norm_sq = 0;
          for i = 2:L
            grad_norm_sq = grad_norm_sq + base.dot(p(i), dp(i), dp(i)) ...
              + base.dot(p(i), du(i), du(i));
          end
          beta = grad_norm_sq * beta;
          
          % update conjugate direction
          for i = 2:L
            cdp(i) = base.convertTangentToPointChart(p(i), cdp(i));
            cdu(i) = base.convertTangentToPointChart(p(i), cdu(i));
            cdp(i) = dp(i) + beta * cdp(i);
            cdu(i) = du(i) + beta * cdu(i);
          end
        end
        
        if grad_norm_sq < grad_tol
          termination_cause = 'Gradient tolerance reached.';
          break;
        end
        
        % normalize the conjugate direction
        m = 0;
        for i = 2:L
          m = m + base.dot(p(i), cdp(i), cdp(i));
          m = m + base.dot(p(i), cdu(i), cdu(i));
        end
        for i = 2:L
          cdp(i) = cdp(i) / sqrt(m);
          cdu(i) = cdu(i) / sqrt(m);
        end
        
        % slope along the conjugate direction
        m = 0;
        for i = 2:L
          m = m + base.dot(p(i), grad_p(i), cdp(i));
          m = m + base.dot(p(i), grad_u(i), cdu(i));
        end
        
        [p, u, v, w, new_cost, cdp, cdu] = ...
          lineSearch(p, u, cdp, cdu, cost, L, m);
        
        if isequal(display,'on')
          fprintf(['\tIter: %' num2str(num_digits) 'd | Cost: %.4e | Cost diff: %.4e | ' ...
            'Step size: %.5e | Conjugate : %d | Gradient norm: %.4e |\n'],...
            iter, cost, abs(cost - new_cost), step, ~steepest_direction, ...
            sqrt(grad_norm_sq));
        end
        
        if iter == max_iter
          if warnings
            warning(['Max iteration number exceeded. Cost value: %d', ...
              newline],new_cost);
          end
          cost = new_cost;
          termination_cause = 'Maximum number of iterations reached.';
          break;
        end
        
        if (abs(cost - new_cost) < new_cost * rel_tol) || new_cost < abs_tol
          if steepest_direction
            termination_cause = 'Cost tolerance.';
            cost = new_cost;
            break;
          else%if step < min_step
            % no progress -> reset to steepest direction
            steepest_direction = true;
          end
        else
          cost = new_cost;
          if steepest_direction
            steepest_direction = false;
          end
        end
        
        iter = iter + 1;
        
      end
      
      if isequal(display,'on') || isequal(display,'final')
        fprintf(['\n\tOptimisation terminated by: %s ' ...
          '\n\tIterations: %d.\n\tInitial cost: %f.\n\tFinal cost: %f.\n'], ...
          termination_cause, iter, init_cost, cost);
      end
      
      v = obj.horizontalLift(x, v(1)) + obj.verticalLift(x, w(1));
      
      % helper functions
      
      function cost = evaluateCost(p, v, w, L)
        
        cost = 0;
        
        for ii = 1:L
          cost = cost + base.dot(p(ii), v(ii), v(ii)) ...
            + base.dot(p(ii), w(ii), w(ii));
        end
        
        cost = cost / 2;
      end
      
      function [dv, dw] = calculateDerivatives(p, v, w, L)
        
        dv = m3t.geometry.rm.Tangent.zeros(base, p(1).getChart(), L);
        dw = m3t.geometry.rm.Tangent.zeros(base, p(1).getChart(), L);
        
        for ii = 2:L
          dv(ii) = (base.parallelTransport(v(ii + 1), p(ii + 1), p(ii)) ...
            - base.parallelTransport(v(ii - 1), p(ii - 1), p(ii))) * L * 0.5;
          dw(ii) = (base.parallelTransport(w(ii + 1), p(ii + 1), p(ii)) ...
            - base.parallelTransport(w(ii - 1), p(ii - 1), p(ii))) * L * 0.5;
        end
        
      end
      
      function [v, w] = calculateTangents(p, u, L)
        
        v = m3t.geometry.rm.Tangent.zeros(base, p(1).getChart(), L + 1);
        w = m3t.geometry.rm.Tangent.zeros(base, p(1).getChart(), L + 1);
        
        v(1) = base.log(p(1), p(2)) * L;
        w(1) = (base.parallelTransport(u(2), p(2), p(1)) - u(1)) * L;
        for ii = 2:L
          v(ii) = (base.log(p(ii), p(ii + 1)) - base.log(p(ii), p(ii - 1)))...
            * L * 0.5;
          w(ii) = (base.parallelTransport(u(ii + 1), p(ii + 1), p(ii)) ...
            - base.parallelTransport(u(ii - 1), p(ii - 1), p(ii))) * L * 0.5;
        end
        v(L + 1) = -base.log(p(L + 1), p(L)) * L;
        w(L + 1) = (u(L + 1) - base.parallelTransport(u(L), p(L), p(L + 1))) * L;
      end
      
      function [grad_p, grad_u] = calculateGradient(p, u, v, w, dv, dw, L)

        grad_p = m3t.geometry.rm.Tangent.zeros(base, p(1).getChart(), L);
        grad_u = m3t.geometry.rm.Tangent.zeros(base, p(1).getChart(), L);

        for ii = 2:L
          grad_p(ii) = -(dv(ii) ...
            + base.riemannCurvature(p(ii), u(ii), w(ii), v(ii)));
          grad_u(ii) = -dw(ii);
        end
      end
      
      function [p, u] = createMidPoints(x, y, L)

        x_base = x.getBasePoint();
        y_base = y.getBasePoint();

        e = 1 / L;
        u = base.log(x_base, y_base);

        p = m3t.geometry.rm.Point(base, x.getChart(), zeros(base.dim(), L + 1));
        p(1) = x_base;
        p(L + 1) = y_base;
        for ii = 2:L
          p(ii) = base.exp(p(1), (ii - 1) * e * u);
        end

        u = m3t.geometry.rm.Tangent.zeros(base, x.getChart(), L + 1);
        u(1) = x.getTangent();
        u(L + 1) = y.getTangent();
        for ii = 2:L
          u(ii) = (1 - e * (ii - 1)) * base.parallelTransport(u(1), p(1), p(ii)) ...
            + e * (ii - 1)*base.parallelTransport(u(L + 1), p(L + 1), p(ii));
        end
      end

      function [new_p, new_u, v, w, new_cost, dp, du] = ...
          lineSearch(p, u, dp, du, cost, L, m)

        new_p = m3t.geometry.rm.Point(...
          base, p(1).getChart(), zeros(base.dim, L + 1));
        new_u = m3t.geometry.rm.Tangent.zeros(base, p(1).getChart(), L + 1);

        new_p(1) = p(1);
        new_u(1) = u(1);
        new_p(L+1) = p(L+1);
        new_u(L+1) = u(L+1);
        step = step*b;
        
        while true
          
          for ii = 2:L
            new_p(ii) = base.exp(p(ii), step * dp(ii));
            new_u(ii) = base.parallelTransport(...
              u(ii) + step * du(ii), p(ii), new_p(ii));
          end
          
          [v, w] = calculateTangents(new_p, new_u, L);
          new_cost = evaluateCost(new_p, v, w, L);
          
          if (cost - new_cost > -step * c * m) || step < min_step
            dp(ii) = base.parallelTransport(dp(ii), p(ii), new_p(ii));
            du(ii) = base.parallelTransport(du(ii), p(ii), new_p(ii));
            break;
          else
            step = step * a;
          end
        end
      end
      
    end

    function mu = mean(obj, p_array, w_array)
      num_p = numel(p_array);
      
      if num_p == 1
        mu = p_array;
        return;
      end
      
      % Decompose points to base points and tangents on the base manifold.
      p(num_p) = m3t.geometry.rm.Point(...
        obj.base_, p_array(1).getChart(), zeros(obj.base_.dim(), num_p));
      u(num_p) = m3t.geometry.rm.Tangent.zeros(...
        obj.base_, p_array(1).getChart(), num_p);
      for i = 1:num_p
        p(i) = p_array(i).getBasePoint();
        u(i) = p_array(i).getTangent();
      end
      
      % Get the initial guess for the mean using the mean of the base
      % points on the base manifold. Bring all tangets to that point and
      % calculate their mean.
      
      p0 = obj.base_.mean(p, w_array);
      u0 = m3t.geometry.rm.Tangent.zeros(obj.base_, p0.getChart(), 1);
      
      sum_w = 0;
      for i = 1:num_p
        u0 = u0 + w_array(i) * obj.base_.parallelTransport(u(i), p(i), p0);
        sum_w = sum_w + w_array(i);
      end
      u0 = u0 / sum_w;
      
      mu = m3t.geometry.rm.BundlePoint(obj, p0, u0);
      
      mu = obj.karcherMean(p_array, w_array, mu);
    end

    function g = metricTensor(obj, x)
      assert(isa(x,'m3t.geometry.rm.BundlePoint') ...
        && obj.isequal(x.getManifold()));

      d = obj.dim();
      p = x.getBasePoint();
      v = x.getTangent();
      vval = v.getValue();

      g_base = obj.base_.metricTensor(p);
      christoffel = obj.base_.christoffelSymbols(p);

      g = zeros(d);
      g(1:d/2, 1:d/2) = g_base;
      g(d/2+1:end, d/2+1:end) = g_base;

      for i = 1:d/2
        for j = 1:d/2
          for a = 1:d/2
            for b = 1:d/2
              for m = 1:d/2
                for n = 1:d/2
                  g(i, j) = g(i, j) + g_base(a, b) ...
                    *christoffel(a, m, i) * christoffel(b, n, j) ...
                    * vval(m) * vval(n);
                end
              end
              g(i, d/2 + j) = g(i, d/2 + j) ...
                + g_base(j, a) * christoffel(a, b, i) * vval(b);
            end
          end
        end
      end
      g(d/2+1:end, 1:d/2) = g(1:d/2, d/2+1:end)';
      g = (g + g') / 2;
    end

    function v = parallelTransport(obj, u, varargin)
      assert(isa(u, 'm3t.geometry.rm.Tangent') ...
        && obj.isequal(u.getManifold()));

      if nargin == 4
        x = varargin{1};
        y = varargin{2};

        assert(isa(x, 'm3t.geometry.rm.BundlePoint') ...
          && obj.isequal(x.getManifold()));
        assert((isa(y, 'm3t.geometry.rm.BundlePoint') ...
          || isa(y, 'm3t.geometry.rm.Tangent')) ...
          && obj.isequal(y.getManifold()));

        if isa(y, 'm3t.geometry.rm.BundlePoint')
          y = obj.log(x, y);
        end
      elseif nargin > 4
        x = varargin{:};
        assert(isa(x, 'm3t.geometry.rm.BundlePoint') ...
          && obj.isequal(x.getManifold()));
        v = u;
        for i = 1:(numel(x) - 1)
          v = obj.parallelTransport(v, x(i), x(i+1));
        end
        return;
      else
        error(['Illegal number of arguments.', newline]);
      end
      
      % calculate v using Euler integration
      num_points = min(ceil(obj.norm(x, y) / 0.01), 500);
      step = 1 / num_points;
      
      manifold = obj.base_;
      
      y = obj.convertTangentToPointChart(x, y);
      u = obj.convertTangentToPointChart(x, u);

      [nu, mu] = obj.decomposeTangent(x, y);
      [xi, eta] = obj.decomposeTangent(x, u);
      
      p = x.getBasePoint();
      u = x.getTangent();
      
      for i = 1:num_points
        p_old = p;
        
        r1 = manifold.riemannCurvature(p_old, u, mu, nu);
        r2 = manifold.riemannCurvature(p_old, u, eta, nu);
        r3 = manifold.riemannCurvature(p_old, u, mu, xi);
        r4 = manifold.riemannCurvature(p_old, nu, xi, u);
        
        dp = step * nu;
        
        p = manifold.exp(p_old, dp);
        u = manifold.parallelTransport(u + step * mu, p_old, dp);
        nu = manifold.parallelTransport(nu - step * r1, p_old, dp);
        mu = manifold.parallelTransport(mu, p_old, dp);
        xi = manifold.parallelTransport(xi - step / 2 * (r2 + r3), p_old, dp);
        eta = manifold.parallelTransport(eta + step * r4, p_old, dp);


        u = manifold.convertTangentToPointChart(p, u);
        nu = manifold.convertTangentToPointChart(p, nu);
        mu = manifold.convertTangentToPointChart(p, mu);
        xi = manifold.convertTangentToPointChart(p, xi);
        eta = manifold.convertTangentToPointChart(p, eta);
      end
      
      x = m3t.geometry.rm.BundlePoint(obj, p, u);
      v = obj.horizontalLift(x, xi) + obj.verticalLift(x, eta);
    end

    function y = pointToEuclidean(obj, x)
      assert(isa(x, 'm3t.geometry.rm.BundlePoint') ...
        && obj.isequal(x.getManifold()));

      p = x.getBasePoint();
      u = x.getTangent();

      y = [p.euclideanValue(); u.euclideanValue(p)];
    end

    function mu = riemannCurvature(obj, x, u, v, w)
      % TO DO
      mu = []; 
    end
    
    function R = riemannCurvatureTensor(obj, x)
      % TO DOOO
      R = [];
    end

    function setLogOptimisationSettings(obj,varargin)
      parser = inputParser();
      
      valid_tol = @(x) isscalar(x) && isreal(x) && isnumeric(x) && x > 0;
      valid_int = @(x) isscalar(x) && isnumeric(x) && x > 0 && x == floor(x);
      valid_real = @(x) isscalar(x) && isnumeric(x) && isreal(x) ...
        && x > 0 && x < 1;
      valid_init_step = @(x) isscalar(x) && isreal(x) && isnumeric(x) && x > 0;
      valid_logical = @(x) isscalar(x) && islogical(x);
      valid_display = @(x) ischar(x) && any(strcmp(x,{'on','off','final'}));
      valid_expand = @(x) isscalar(x) && isnumeric(x) && isreal(x) && x > 1;
      
      parser.addParameter('maxIter', 900, valid_int);
      parser.addParameter('absTol', 1e-5, valid_tol);
      parser.addParameter('relTol', 1e-5, valid_tol);
      parser.addParameter('gradTol', 1e-5, valid_tol);
      parser.addParameter('initStep',1, valid_init_step);
      parser.addParameter('minStep', 1e-7, valid_tol);
      parser.addParameter('numPoints', 10, valid_int);
      parser.addParameter('warnings', true, valid_logical);
      parser.addParameter('display', 'off', valid_display);
      parser.addParameter('armijoCtrlParam', 0.5, valid_real);
      parser.addParameter('stepShrinkParam', 0.5, valid_real);
      parser.addParameter('stepExpandParam', 10, valid_expand);
      parser.addParameter('useConjugate',true,valid_logical);
      
      parser.parse(varargin{:});
      res = parser.Results;
      
      obj.log_optim_settings_ = struct(...
        'maxIter', res.maxIter, ...
        'absTol', res.absTol, ...
        'relTol', res.relTol, ...
        'gradTol', res.gradTol, ...
        'initStep', res.initStep, ...
        'minStep', res.minStep, ...
        'numPoints', res.numPoints, ...
        'warnings', res.warnings, ...
        'display', res.display, ...
        'armijoCtrlParam', res.armijoCtrlParam, ...
        'armijoStepShrink', res.stepShrinkParam, ...
        'armijoStepExpand', res.stepExpandParam, ...
        'useConjugate', res.useConjugate);
    end

    function v = tangentToEuclidean(obj, x, u)
      assert(isa(x, 'm3t.geometry.rm.BundlePoint') ...
        && obj.isequal(x.getManifold()));
      assert(isa(u, 'm3t.geometry.rm.Tangent') ...
        && obj.isequal(u.getManifold()));

      J = obj.getChartTransitionJacobian(x, x.getChart(), int32(0));

      v = J * obj.convertTangentToPointChart(x, u).getValue();
    end

    function v = verticalLift(obj, x, u)
      assert(isa(x, 'm3t.geometry.rm.BundlePoint') ...
        && obj.isequal(x.getManifold()));
      assert(isa(u, 'm3t.geometry.rm.Tangent') ...
        && obj.base_.isequal(u.getManifold()));

      chart = x.getChart();

      u = obj.base_.convertTangentToPointChart(x.getBasePoint(), u);
      vval = [zeros(obj.base_.dim(), 1); u.getValue()];

      v = m3t.geometry.rm.Tangent(obj, chart, vval);
    end

  end
  
  methods (Access = protected)
    
    function obj_copy = copyElement(obj)
      obj_copy = obj.copyElement@m3t.geometry.rm.Manifold();
      obj_copy.base_ = obj.base_.copy();
    end
    
  end
  
end

