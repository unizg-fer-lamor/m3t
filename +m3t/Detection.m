% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------

classdef Detection < matlab.mixin.Heterogeneous ...
    & matlab.mixin.Copyable
  
  properties (SetAccess = private)
    timestamp_;
    value_;
  end
  
  methods
    
    function obj = Detection(value, timestamp)
      obj.value_ = value;
      obj.setTimestamp(timestamp);
    end
    
    function t = getTimestamp(obj)
      t = obj.timestamp_;
    end
    
    function v = getValue(obj)
      v = obj.value_;
    end
    
    function setTimestamp(obj,timestamp)
      assert(isa(timestamp,'double') && isscalar(timestamp), ...
        ['Timestamp must be a double scalar.', newline]);
      obj.timestamp_ = timestamp;
    end
    
  end
  
end

