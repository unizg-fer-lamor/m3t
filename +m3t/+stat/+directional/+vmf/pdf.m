% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
%
%   m3t.stat.directional.vmf.pdf(x,mu,kappa)
%
%     Calculates the probability density function (pdf) of a von Mises-Fisher
%     distribution with mean mu and concentration kappa at point x. Arguments x
%     and mu must be column vectors of same dimension and kappa must be positive
%     scalar.
%
% ------------------------------------------------------------------------------

function value = pdf(x, mu, kappa)
  dim = numel(x);
  x = x/norm(x);
  mu = mu/norm(mu);
  value = kappa^(0.5 * dim - 1) / ((2 * pi)^(0.5 * dim) * ...
    besseli(0.5 * dim - 1, kappa, 1)) * exp(kappa * (mu' * x - 1));
end

