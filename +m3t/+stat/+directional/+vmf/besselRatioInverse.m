% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
%
%   m3t.stat.directional.vmf.besselRatioInverse(d,x)
%     Calculates the inverse of a function:
%
%                    I_{d/2}(x)
%       A_{d}(x) = --------------,
%                   I_{d/2-1}(x)
%
%     where I_{n}(x) is the n-th order modified Bessel function of the first
%     kind. Function currently supports only integer values of d such that
%     0 < d < 256.
%
%     See also m3t.stat.directional.vmf.besselRatio, besseli
%
% ------------------------------------------------------------------------------

function result = besselRatioInverse(d, x)
  assert(x >= 0 && x <= 1);
  assert(isinteger(d) && isscalar(d) && d > 0 && d <= 256);
  d = double(d);
  persistent lut;
  if isempty(lut)
    % load lookup table
    load('+m3t\+stat\+directional\+vmf\inv_bessel_ratio_lut.mat','lut');
  end
  result = ppval(lut(d), x);
end

