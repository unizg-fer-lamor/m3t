% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
%
%   m3t.stat.directional.vmf.quantiles
%
%     Calculates the quantiles of the von Mises-Fisher distribution.
%
%     q = m3t.geometry.directional.vmf.quantiles(p, k, d) returns the quantiles 
%     for the value p of d-dimensional vMF distribution with concentration k.
%
% ------------------------------------------------------------------------------

function delta = quantiles(percent, kappa, dim)
  persistent lut; 
  if isempty(lut)
    load('+m3t\+stat\+directional\+vmf\quantiles_lut.mat','lut');
  end
  if kappa <= 1000
    delta = interp2(...
      lut.alpha,lut.kappa,lut.values(:,:,dim-1)',percent,kappa,'spline');
  else
    c = interp1(lut.alpha,lut.c(:,dim-1),percent,'makima');
    delta = c*kappa^(-1/2);
  end
end

