% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
%
%   m3t.stat.directional.vmf.confidenceTest(x,mu,kappa,percent)
%
%     Interval test for unit vector x given the von Mises-Fisher distribution
%     defined by mean mu and concentration kappa. Arguments x and mu must be
%     column vectors of same dimension, kappa must be a positive scalar and
%     percent must be scalar from interval [0,1] that defines the width of the
%     confidence interval.
%
% ------------------------------------------------------------------------------

function result = confidenceTest(x, mu, kappa, percent)
  x = x/norm(x);
  mu = mu/norm(mu);
  dim = numel(x);
  delta = m3t.stat.directional.vmf.quantiles(percent, kappa, dim);
  result = x'*mu >= cos(delta);
end

