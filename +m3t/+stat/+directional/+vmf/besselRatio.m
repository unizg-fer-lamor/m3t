% ------------------------------------------------------------------------------
%
%   This file is part of m3t.
%
%   Copyright (C) 2021 Borna Bicanic, University of Zagreb Faculty of Electrical
%   Engineering and Computing.
%
%   m3t is free software: you can redistribute it and/or modify it under the
%   terms of the GNU General Public License as published by the Free Software
%   Foundation, either version 3 of the License, or (at your option) any later
%   version.
%
%   This program is distributed in the hope that it will be useful, but WITHOUT
%   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
%   more details.
%
%   You should have received a copy of the GNU General Public License along with
%   this program. If not see http://www.gnu.org/licenses/.
%
% ------------------------------------------------------------------------------
%
%   m3t.stat.directional.vmf.besselRatio(d,x)
%     Calculates the ratio:
%
%                    I_{d/2}(x)
%       A_{d}(x) = --------------,
%                   I_{d/2-1}(x)
%
%     where I_{n}(x) is the n-th order modified Bessel function of the first
%     kind.
%
%     See also m3t.stat.directional.vmf.besselRatioInverse, besseli
%
% ------------------------------------------------------------------------------

function result = besselRatio(d, x)
  d = double(d);
  result = besseli(d/2, x, 1)/besseli(d/2 - 1, x, 1);
end

